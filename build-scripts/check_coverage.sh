#!/bin/sh

coverage run --source=resumeai setup.py test -q
cov=`$1coverage report -m | grep TOTAL | sed 's/TOTAL\s*\w*\s*\w*\s*//g' | cut -d'%' -f1`

echo '#######'
echo 'coverage was:' $cov '%'
echo '#######'

if [ $cov -eq 100 ]
then
    exit 0
else
    coverage report -m
    exit 1
fi
