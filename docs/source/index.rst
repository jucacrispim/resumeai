.. ResumeAi documentation master file, created by
   sphinx-quickstart on Wed Oct 29 15:24:16 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bem-vindo à documentação do ResumeAí!
=====================================

ResumeAí é um sumariador - por extração - de texto que também faz
a extração de entidades nomeadas. O ResumeAi em si é uma biblioteca
(pacote resumeai), mas este projeto conta também com uma aplicação
web que utiliza a biblioteca.


Usando a aplicação web
++++++++++++++++++++++

Esta aplicação web utiliza a biblioteca de sumariação - resumeai - e faz
o resumo dos conteúdos publicados pelos sites da Editora Globo.

O servidor (já em produção) para acessar a aplicação web é:
``http://54.174.11.63``.


Listando conteúdos resumidos
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Usando a api rest você pode listar os últimos conteúdos resumidos. A url
para lista de conteúdos é: ``http://54.174.11.63/json/lista/ultimas``.
Nesta url se pode paginar os resultados passando-se o parâmetro ``pagina``,
assim: ``http://54.174.11.63/json/lista/ultimas?pagina=2``.

Pode-se listar somente os conteúdos com determinada entidade nomeada usando-se
uma url como ``http://54.174.11.63/json/lista/<ENTIDADE>``, algo assim:
``http://54.174.11.63/json/lista/TV Globo`` (com espaço mesmo).

Pode-se também listar somente os conteúdos de determinado site, usando-se a
seguinte url ``http://54.174.11.63/json/site/<NOME DO SITE>``, algo assim:
``http://54.174.11.63/json/site/Época``.

Nas listagens por site e entidades nomeadas também se pode usar o parâmetro
``pagina`` para paginação.


Obtendo os assuntos quentes (trend topics)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Você pode obter os assuntos mais comentados nos sites através da seguinte
url: ``http://54.174.11.63/json/trendtopics``. Esta url retorna os
assuntos mais comentados pelos sites da Editora Globo num intervalo
de tempo determinado pelo algorítmo do webapp (mesma coisa de usar o
parâmetro ``smart``).

Pode-se alterar o número de assuntos, a quantidade de horas a serem
consideradas e escolher também um site específico usando os parâmetros
``quantidade``, ``horas`` e ``site`` respectivamente. Algo assim:
``http://54.174.11.63/json/trendtopics?horas=3&quantidade=5&site=Crescer``.

Pode-se também usar o parâmetro ``smart``, que recebe 1 ou 0. Esta opção
faz com que o servidor tente escolher o melhor intervalo de horas para
retornar os trend topics. Assim:
``http://54.174.11.63/json/trendtopics?smart=1``.





Contents:

.. toctree::
   :maxdepth: 2



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
