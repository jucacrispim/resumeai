# -*- coding: utf-8 -*-


from fabric.api import run, env, put
from fabric.context_managers import cd, prefix


INSTALL_COMMAND = 'sudo aptitude install -y '
PIP_INSTALL_COMMAND = 'pip install --extra-index-url=http://pypi.jaobidev.com.br/+simple/ '
WEBAPP_SERVER_DEPS = [
    'uwsgi', 'nginx', 'python-virtualenv', 'git-core', 'python3-dev']
CELERY_SERVER_DEPS = ['python-virtualenv', 'git-core',
                      'python3-dev', 'supervisord', 'mongodb', 'supervisor']
APTITUDE_UPDATE = 'sudo aptitude update'
ENVIRON = env.roles[0].split('-')[1]
USER_HOME = '/home/sillyme'
SRC_DIR = USER_HOME + '/src'
RESUMEAI_SRC = SRC_DIR + '/resumeai'
VENV_DIR = USER_HOME + '/venv'
RESUMEAI_VENV = VENV_DIR + '/resumeai'
ACTIVATE_ENV_COMMAND = 'source %s/bin/activate' % RESUMEAI_VENV
RESUMEAI_REPO = 'git@bitbucket.org:jucacrispim/resumeai.git'

UWSGI_COMMAND = '%s/bin/uwsgi  -s /tmp/uwsgi.sock ' % RESUMEAI_VENV
UWSGI_COMMAND += '--daemonize=uwsgi.log '
UWSGI_COMMAND += '--module webapp.views --callable toscoapp --http 5000 '
UWSGI_COMMAND += '--pidfile uwsgi.pid '

SUPERVISORD_CONFFILE = RESUMEAI_SRC + \
    '/infra/%s/conffiles/supervisor/celerybeat.conf' % ENVIRON
SUPERVISORD_COMMAND = 'sudo supervisord -c %s' % SUPERVISORD_CONFFILE

env.roledefs = {'webapp-dev': ['ubuntu@54.84.248.76'],
                'celery-dev': ['ubuntu@54.84.248.76'],
                'docs-dev': ['ubuntu@54.84.248.76'],
                'webapp-prod': ['ubuntu@54.174.11.63'],
                'celery-prod': ['ubuntu@54.173.198.105']}

env.key_filename = '~/.ssh/recomendacoes.pem'


def install_server_deps_webapp():
    run(APTITUDE_UPDATE)
    cmd = INSTALL_COMMAND + ' '.join(WEBAPP_SERVER_DEPS)
    run(cmd)


def install_server_deps_celery():
    run(APTITUDE_UPDATE)
    cmd = INSTALL_COMMAND + ' '.join(CELERY_SERVER_DEPS)
    run(cmd)


def clone_or_update_project(named_tree):
    mkdircmd = 'mkdir -p %s ' % SRC_DIR
    try:
        run(mkdircmd)
    except:
        pass

    clone_cmd = 'git clone %s' % RESUMEAI_REPO
    with cd(SRC_DIR):
        try:
            run(clone_cmd)
        except:
            pass

        with cd(RESUMEAI_SRC):
            update_cmd = 'git pull --all'
            run(update_cmd)

            checkout_cmd = 'git checkout %s' % named_tree
            run(checkout_cmd)


def install_requirements():
    with prefix(ACTIVATE_ENV_COMMAND):
        with cd(RESUMEAI_SRC):
            cmd = PIP_INSTALL_COMMAND + ' -r requirements.txt'
            run(cmd)


def create_env(pyversion='python3'):
    mkdircmd = 'mkdir -p %s' % VENV_DIR
    try:
        run(mkdircmd)
    except:
        pass

    cmd = 'virtualenv -p /usr/bin/%s %s' % (pyversion, RESUMEAI_VENV)
    run(cmd)


def deploy_webapp(full=False, named_tree='master'):
    if full:
        install_server_deps_webapp()
        create_env()
    clone_or_update_project(named_tree)
    install_requirements()
    copy_conffiles_webapp()
    cadastra_sinonimos()
    start_uwsgi()
    restart_nginx()


def deploy_celery(full=False, named_tree='master'):
    if full:
        install_server_deps_celery()
        create_env()

    clone_or_update_project(named_tree)
    install_requirements()
    copy_conffiles_celery()
    cadastra_sinonimos()
    restart_celery()


def deploy_docs(full=False, named_tree='master'):
    if full:
        install_server_deps_celery()
        create_env()

    clone_or_update_project(named_tree)
    install_requirements()
    make_docs()
    copy_conffiles_nginx()
    restart_nginx()


def make_docs():
    with cd(RESUMEAI_SRC + '/docs'):
        with prefix(ACTIVATE_ENV_COMMAND):
            run('make html')
            run('sudo mkdir -p /var/www/html')
            run('sudo rm -rf /var/www/html/docs')
            run('sudo cp -r build/html /var/www/html/docs')


def restart_celery():
    cmd = 'sudo killall supervisord'
    try:
        run(cmd)
    except:
        pass

    cmd = "ps aux | grep celery | cut -d ' ' -f4  | xargs kill -9"
    try:
        run(cmd)
    except:
        pass

    cmd = 'sudo killall celery && sleep 3'
    while True:
        try:
            run(cmd)
        except:
            break

    with prefix(ACTIVATE_ENV_COMMAND):
        with cd(RESUMEAI_SRC):
            try:
                run('sudo chmod 777 celerybeat-scheduler')
            except:
                pass

            try:
                run('rm atualizar.lock')
            except:
                pass

            run(SUPERVISORD_COMMAND)

    # cmd = 'python webapp/celeryconfig.py'
    # with prefix(ACTIVATE_ENV_COMMAND):
    #     with cd(RESUMEAI_SRC):
    #         run(cmd, pty=False)


def cadastra_sinonimos():
    with cd(RESUMEAI_SRC):
        with prefix(ACTIVATE_ENV_COMMAND):
            run('export PYTHONPATH="." && python webapp/utils.py --cadsin')


def start_uwsgi():
    with cd(RESUMEAI_SRC):
        kill_cmd = 'sudo kill -9 `cat uwsgi.pid`'
        try:
            run(kill_cmd)
        except:
            pass

        with prefix(ACTIVATE_ENV_COMMAND):
            run(UWSGI_COMMAND)


def copy_conffiles_celery():
    put('infra/%s/conffiles/webapp_config.py' % ENVIRON,
        '%s/webapp/config.py' % RESUMEAI_SRC)


def copy_conffiles_webapp():
    copy_conffiles_nginx()

    put('infra/%s/conffiles/webapp_config.py' % ENVIRON,
        '%s/webapp/config.py' % RESUMEAI_SRC)


def copy_conffiles_nginx():
    put('infra/%s/conffiles/nginx/sites-enabled/resumeai' % ENVIRON,
        '/etc/nginx/sites-enabled/', use_sudo=True)


def restart_nginx():
    cmd = 'sudo service nginx restart'
    run(cmd)
