Pesquisa sumarização 01
=======================

Classificação multiplos sumários
++++++++++++++++++++++++++++++++

Aqui está a sequência em que as estratégias foram apresentadas em
cada texto da pesquisa:


Texto 1: A estratégia de Obama contra o Estado Islâmico em 5 pontos
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* lazy
* eager
* eagerNoSimilarity
* semantria


Texto 2: Aécio critica ataques pessoais de Dilma e Marina e diz que não vai entrar no 'vale tudo'
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* semantria
* eager
* lazy
* eagerNoSimilarity


Texto 3: Dilma diz que Mantega não fica como ministro em um segundo mandato
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Neste aqui, a última frase do lazy foi retirara para não ficar igual ao
eagerNoSimilarity

* eagerNoSimilarity
* lazy
* semantria
* eager

Texto 4: AVALIAÇÃO: VW FOX RENOVADO CHEGA AO MERCADO POR R$ 35.900
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* semantria
* lazy
* eagerNoSimilarity
* eager

Texto 5: NYFW: aprenda a fazer o look girly da maquiagem da grife Giulietta
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* eager
* eagerNoSimilarity
* semantria
* lazy

Texto 6: VENDAS DO VAREJO RECUAM 1,1% EM JULHO
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* lazy
* semantria
* eager
* eagerNoSimilarity

Texto 7: Autonomia do BC é para acabar com inteferência política, diz Marina
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Aqui eager perdeu o segundo parágrafo para não ficar igual ao eagerNoSimilarity

* lazy
* eager
* eagerNoSimilarity
* semantria


Classificação como bom ou ruim
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Modelo 1: eager
Modelo 2: semantria
