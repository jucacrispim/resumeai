Primeira parte: Classificação - ordenação - de múltiplos resumos.
=================================================================

Nesta primeira parte serão apresentados alguns textos e é preciso
que vocês dêem uma ordenação para os resumos de acordo com a qualidade.
O primeiro é o melhor, o segundo o segundo melhor e assim por diante.


Texto 1: A estratégia de Obama contra o Estado Islâmico em 5 pontos
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Tamanho original: 4935

Em pronunciamento na noite de quarta-feira (10) o presidente dos Estados Unidos, Barack Obama, anunciou que o país vai se opor aos terroristas do grupo Estado Islâmico através de ofensivas aéreas. Sua condução não exige autorização do Congresso. As ações, segundo o presidente, se estenderão para a Síria pela primeira vez. Obama comparou o plano àquele que o governo já pratica em países como o Iêmen e Somália, em que drones e missões de tropas especiais são usados para combater militantes da Al Qaeda e grupos afiliados.  Apesar de elogiada pelo presidente, a estratégia adotada nesses países já foi apontada como ineficaz por críticos, além de provocar mortes de grande número de civis.

O pronunciamento de Obama ocorreu às vésperas do 13º aniversário dos atentados às Torres Gêmeas, no dia 11 de setembro de 2001. Segundo Obama, as ofensivas aéreas são necessárias para evitar que o Estado Islâmico se torne uma ameaça real para os Estados Unidos. Por ora, seus militantes foram os responsáveis pela decapitação de dois jornalistas americanos. Como as investidas são classificadas como medidas de contra-terrorismo, elas não precisam passar pelo Congresso americano para ser aprovadas. “Não hesitarei em agir contra o Estado Islâmico na Síria, tampouco no Iraque”, afirmou Obama.

Apesar de não precisar de autorização do Congresso para lançar essas ofensivas, a administração Obama espera encontrar apoio para autorizar o envio de munição e e organizar treinamento aos rebeldes sírios que se opõem ao regime de Bashar Al-Assad. O pronunciamento do presidente ocorre cerca de um ano depois de Obama ter afirmado que não usaria bombardeios aéreos contra Assad.

No pronunciamento de 15 minutos, Obama ressaltou que as operações militares na Síria e no Iraque, para deter o Estado Islâmico, serão diferentes das guerras anteriores travadas pelo país na região: “Eu trabalhei quatro anos e meio para acabar com as guerras, não para começá-las”. A nova estratégia não envolvera tropas americanas combatendo em solo:“Não seremos arrastados para mais uma guerra no Iraque”.

Obama disse que a estratégia americana está amparada em quatro pilares: ataques aéreos, apoio a aliados locais, esforços de contraterrorismo para prevenir ataques e minar o Estado Islâmico, e assistência humanitária contínua a civis. Seus planos podem ser organizados em 5 principais pontos:

1 – Ataques aéreos
Eles já ocorrem no norte do Iraque. Com seu auxílio, o exército iraquiano conseguiu conter o avanço do EI na região em meados de agosto. Obama se ampara no aparente sucesso já experimentado pela estratégia para aumentar a frequência dos ataques.  Como a ideia deu certo no norte do Iraque, é possível que funcione também no resto do país e na Síria. Por ora, os ataques americanos são limitados: são direcionados a ações do Estado Islâmico que possam trazer riscos humanitários óbvios ou ameaçar militares americanos atuando na região. Essas limitações deixam de existir.

A maior diferença entre a estratégia adotada até aqui e o plano anunciado na noite de quarta está na expansão desses ataques para a Síria. A estratégia foi desenhada para tirar proveito do maior ponto fraco do Estado Islâmico - suas rotas de abastecimento. A intenção dos americanos é interromper a movimentação dos terroristas entre o Iraque e a Síria, onde eles obtêm munição, combustível e suprimentos.

2 – Evitar a ação de tropas americanas em solo
Um dos pontos mais destacados no pronunciamento de Obama. Segundo ele, não serão enviados soldados americanos para o campo de batalha.

3 – Conquistar o apoio de atores regionais, como a Arábia Saudita, e aliados europeus.
Obama quer buscar o apoio de governos sunitas na região e de aliados europeus. Na quarta-feira, a administração Obama entrou em contato com monarquias sunitas, em busca de seu auxílio para armar, treinar e abastecer rebeldes sírios contra o Estado Islâmico.
Obama também tem planos para deter o financiamento internacional para o EI. Para isso, pretende contar com o apoio dos países europeus. Outra preocupação é que os cerca de 1500 europeus que viajaram para lutar com o Estado Islâmico retornem aos seus países de origem para organizar atentados. Os EUA vão cooperar com esses países para deter possíveis atentados

4 – Treinar e armar as tropas iraquianas e curdas
Cerca de 470 soldados americanos serão enviados para a região para treinar o exército iraquiano e as tropas curdas. Espera-se que, com mais treinamento e equipamento melhor, esses aliados serão capazes de deter o avanço do Estado Islâmico e retomar os territórios já conquistados.

5 – Treinar e armar rebeldes sírios
     Para que os ataques aéreos surtam resultado, o governo espera contar com aliados em solo que retomem o controle de territórios ocupados pelo Estado Islâmico na Síria. Obama já destinou US$500 milhões para armar os rebelde sírios. Espera aprovação do Congresso para destinar uma verba maior para este fim.



Resumo 1: A estratégia de Obama contra o Estado Islâmico em 5 pontos
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Tamanho do resumo: 537

Em pronunciamento na noite de quarta-feira o presidente dos Estados Unidos, Barack Obama, anunciou que o país vai se opor aos terroristas do grupo Estado Islâmico através de ofensivas aéreas... “Não hesitarei em agir contra o Estado Islâmico na Síria, tampouco no Iraque”, afirmou Obama... Apesar de não precisar de autorização do Congresso para lançar essas ofensivas, a administração Obama espera encontrar apoio para autorizar o envio de munição e e organizar treinamento aos rebeldes sírios que se opõem ao regime de Bashar Al-Assad.


Resumo 2: A estratégia de Obama contra o Estado Islâmico em 5 pontos
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Tamanho do resumo: 572

Em pronunciamento na noite de quarta-feira o presidente dos Estados Unidos, Barack Obama, anunciou que o país vai se opor aos terroristas do grupo Estado Islâmico através de ofensivas aéreas... “Não hesitarei em agir contra o Estado Islâmico na Síria, tampouco no Iraque”, afirmou Obama... No pronunciamento de 15 minutos, Obama ressaltou que as operações militares na Síria e no Iraque, para deter o Estado Islâmico, serão diferentes das guerras anteriores travadas pelo país na região: “Eu trabalhei quatro anos e meio para acabar com as guerras, não para começá-las”...


Resumo 3: A estratégia de Obama contra o Estado Islâmico em 5 pontos
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Tamanho do resumo: 645

Em pronunciamento na noite de quarta-feira o presidente dos Estados Unidos, Barack Obama, anunciou que o país vai se opor aos terroristas do grupo Estado Islâmico através de ofensivas aéreas... “Não hesitarei em agir contra o Estado Islâmico na Síria, tampouco no Iraque”, afirmou Obama... Obama disse que a estratégia americana está amparada em quatro pilares: ataques aéreos, apoio a aliados locais, esforços de contraterrorismo para prevenir ataques e minar o Estado Islâmico, e assistência humanitária contínua a civis... A estratégia foi desenhada para tirar proveito do maior ponto fraco do Estado Islâmico - suas rotas de abastecimento...


Resumo 4: A estratégia de Obama contra o Estado Islâmico em 5 pontos
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Tamanho do resumo: 421

Sua condução não exige autorização do Congresso... Obama comparou o plano àquele que o governo já pratica em países como o Iêmen e Somália, em que drones e missões de tropas especiais são usados para combater militantes da Al Qaeda e grupos afiliados... Apesar de elogiada pelo presidente, a estratégia adotada nesses países já foi apontada como ineficaz por críticos, além de provocar mortes de grande número de civis...


Agora, ordene os resumos (ex: 3, 1, 2, 4)

Resumos ordenados:


Aécio critica ataques pessoais de Dilma e Marina e diz que não vai entrar no 'vale tudo'
========================================================================================

Tamanho original: 2767

Montes Claros - O candidato do PSDB à Presidência, Aécio Neves, criticou nesta quinta-feira, 11, os ataques feitos pela campanha da presidente Dilma Rousseff contra Marina Silva (PSB). A ex-ministra está à frente do tucano nas pesquisas de intenção de voto, mas Aécio afirmou que não entrará "no vale tudo para ganhar eleição". Durante sua agenda em Montes Claros (MG), o tucano manteve a estratégia de associar Marina ao PT, lembrando o fato de a candidata já ter sido filiada ao partido e ministra do governo Lula.
RelacionadasMinistro de Minas e Energia causa 'vergonha alheia', diz MarinaTSE mantém propaganda do PT sobre autonomia do BC"Não faço a ela (Marina) qualquer acusação do ponto de vista pessoal. Acho absolutamente inaceitável o tipo de acusação que ela recebe hoje da presidente Dilma. Não entro nesse campo, entro no campo político", declarou, referindo-se a programas da campanha do PT como o que comparou a adversária do PSB aos ex-presidentes Jânio Quadros e Fernando Collor.
Aécio fez questão de pontuar o que considera incoerências do discurso adotado por Marina na atual campanha eleitoral em relação a sua postura diante de uma série de temas. "É preciso que saibamos, por exemplo, que a Marina candidata é a que abraça o agronegócio ou a aquela que propunha a proibição do cultivo de transgênicos no País. É a Marina que hoje defende a política econômica do PSDB, até indo além do que imaginamos adequado como a autonomia do Banco Central, ou é a Marina que no PT lá atrás combateu o Plano Real e votou contra a Lei de Responsabilidade Fiscal", disse.
O senador também voltou a associar Marina ao PT, em uma tentativa de se posicionar como "a mudança verdadeira em relação a tudo isso que está aí". "Vejo muitas semelhanças no discurso da candidata Marina, que respeito pessoalmente, com o discurso da candidata Dilma quatro anos atrás. Até porque conviveram muito tempo juntas no próprio PT", salientou.
Arrecadação. O fato de aparecer em terceiro lugar nas pesquisas de intenção de voto, segundo Aécio, não representou queda na arrecadação de sua campanha. Ele disse que, caso isso tenha ocorrido, não foi "avisado". O candidato destacou que sua campanha "não é rica", mas "vai bem" e será feita "dentro do planejado". E, apesar de ter arrecadado mais que o dobro da candidatura de Marina Silva, afirmou que "as doações estão indo de forma muito vigorosa para as outras candidaturas".
De acordo com a segunda parcial da prestação de contas à Justiça Eleitoral, a campanha tucana arrecadou R$ 42 milhões, contra R$ 19 milhões arrecadados pelo PSB. Mesmo somados, os valores não chegam à metade dos R$ 123 milhões arrecadados até o momento pela campanha pela reeleição da presidente Dilma Rousseff.


Resumo 1: Aécio critica ataques pessoais de Dilma e Marina e diz que não vai entrar no 'vale tudo'
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Tamanho do resumo: 579

Montes Claros - O candidato do PSDB à Presidência, Aécio Neves, criticou nesta quinta-feira, 11, os ataques feitos pela campanha da presidente Dilma Rousseff contra Marina Silva (PSB)... A ex-ministra está à frente do tucano nas pesquisas de intenção de voto, mas Aécio afirmou que não entrará "no vale tudo para ganhar eleição"... É a Marina que hoje defende a política econômica do PSDB, até indo além do que imaginamos adequado como a autonomia do Banco Central, ou é a Marina que no PT lá atrás combateu o Plano Real e votou contra a Lei de Responsabilidade Fiscal", disse...


Resumo 2: Aécio critica ataques pessoais de Dilma e Marina e diz que não vai entrar no 'vale tudo'
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Tamanho do resumo: 494

Montes Claros - O candidato do PSDB à Presidência, Aécio Neves, criticou nesta quinta-feira, 11, os ataques feitos pela campanha da presidente Dilma Rousseff contra Marina Silva... Aécio fez questão de pontuar o que considera incoerências do discurso adotado por Marina na atual campanha eleitoral em relação a sua postura diante de uma série de temas... "Vejo muitas semelhanças no discurso da candidata Marina, que respeito pessoalmente, com o discurso da candidata Dilma quatro anos atrás...

Resumo 3: Aécio critica ataques pessoais de Dilma e Marina e diz que não vai entrar no 'vale tudo'
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Tamanho do resumo: 613

Montes Claros - O candidato do PSDB à Presidência, Aécio Neves, criticou nesta quinta-feira, 11, os ataques feitos pela campanha da presidente Dilma Rousseff contra Marina Silva... RelacionadasMinistro de Minas e Energia causa 'vergonha alheia', diz MarinaTSE mantém propaganda do PT sobre autonomia do BC"Não faço a ela qualquer acusação do ponto de vista pessoal... É a Marina que hoje defende a política econômica do PSDB, até indo além do que imaginamos adequado como a autonomia do Banco Central, ou é a Marina que no PT lá atrás combateu o Plano Real e votou contra a Lei de Responsabilidade Fiscal", disse.


Resumo 4: Aécio critica ataques pessoais de Dilma e Marina e diz que não vai entrar no 'vale tudo'
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Tamanho do resumo: 541

Montes Claros - O candidato do PSDB à Presidência, Aécio Neves, criticou nesta quinta-feira, 11, os ataques feitos pela campanha da presidente Dilma Rousseff contra Marina Silva... RelacionadasMinistro de Minas e Energia causa 'vergonha alheia', diz MarinaTSE mantém propaganda do PT sobre autonomia do BC"Não faço a ela qualquer acusação do ponto de vista pessoal... Aécio fez questão de pontuar o que considera incoerências do discurso adotado por Marina na atual campanha eleitoral em relação a sua postura diante de uma série de temas...


Resumos ordenados:


Texto 3: Dilma diz que Mantega não fica como ministro em um segundo mandato
===========================================================================

Tamanho original: 3515


A presidente Dilma Rousseff afirmou nesta segunda-feira, durante entrevista ao jornal "O Estado de S. Paulo" transmitida pela internet, que o ministro Guido Mantega, da Fazenda, não permanecerá no governo na hipótese de ela vir a ser reeleita.
"O Guido já me comunicou que ele não tem como ficar no governo em um segundo mandato por razões eminentemente pessoais que peço que vocês respeitem", declarou.
Na semana passada, a presidente já havia afirmado que fará mudanças em sua equipe de governo se vencer as eleições de outubro. Na ocasião, ela disse, sem detalhar quais pastas seriam alvo de alterações, que "governo novo, equipe nova".

Questionada se, com a substituição de Mantega, alterará a política econômica na hipótese de reeleição, Dilma voltou a mencionar, sem citar nomes, o episódio da eleição municipal de 1985, em São Paulo, e não respondeu. Na ocasião, o ex-presidente Fernando Henrique Cardoso (PSDB), à época candidato do PMDB à Prefeitura de São Paulo, posou para a imprensa sentado na cadeira do prefeito um dia antes da eleição. FHC perdeu para o ex-presidente Jânio Quadros.
Confrontada com a informação de que o adversário Aécio Neves (PSDB) já anunciou o nome do ministro da Fazenda caso seja eleito (Armínio Fraga), Dilma não quis falar sobre o eventual novo ocupante da pasta.
"Não vou nunca dizer quem vai ser ministro no meu segundo mandato, se porventura eu tiver. Quem indica ministro antes da hora, eu acho que dá azar. Da ultima vez que fizeram isso – você lembra – dá azar", declarou.
Dilma afirmou que poderá haver mudanças na economia porque o país "se preparou para essas mudanças". "Temos condições de diminuir alguns incentivos e outros não. Vamos continuar a garantir emprego e valorização do salário mínimo", disse.
Delação premiada
A presidente afirmou que recorrerá até ao Supremo Tribunal Federal (STF) para saber se há integrantes do governo envolvidos com as denúncias resultantes de delação premiada do ex-diretor da Petrobras Paulo Roberto Costa, preso no Paraná.
Costa vem dando depoimentos diários a policiais federais e procuradores da República em troca da possibilidade de redução da pena. Nos depoimentos, Costa delatou dezenas de senadores e deputados federais de três partidos (PT, PMDB e PP), governadores e um ministro como supostos beneficiários de um esquema de pagamento de propinas com dinheiro de contratos da Petrobras.
Dilma afirmou que, após a divulgação das denúncias pela revista "Veja", determinou ao ministro da Justiça, José Eduardo Cardozo, que fizesse um ofício à Polícia Federal pedindo acesso ao conteúdo dos depoimentos para saber se há algum funcionário ou agente do governo envolvido.
"Quando a PF respondeu que não pode, fiz ofício ao procurador-geral da República que me informe, para proceder às providências cabíveis [...] Se o Ministério Público não for capaz de responder, vou pedir ao Supremo Tribunal Federal", declarou.
Segundo a presidente. ela necessita das informações oficiais porque não sabe se as informações divulgadas pela imprensa são verdadeiras. "Primeiro é preciso saber se isso é verdade. Então, eu quero essas informações. [...] Quem vai falar isso para mim é quem investiga [...] e não diz-que-diz-que. Quero a informação a mais aprofundada possível", afirmou.
A candidata disse que tem a "competência" para dar informações sobre o assunto é Polícia Federal, o Ministério Público ou o Supremo Tribunal Federal. "Baseado na informação da imprensa, eu não posso condenar ou perdoar ninguém", disse.


Resumo 1: Dilma diz que Mantega não fica como ministro em um segundo mandato
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
eagerNoSimilarity
Tamanho do resumo: 536

A presidente Dilma Rousseff afirmou nesta segunda-feira, durante entrevista ao jornal "O Estado de S. Paulo" transmitida pela internet, que o ministro Guido Mantega, da Fazenda, não permanecerá no governo na hipótese de ela vir a ser reeleita... "O Guido já me comunicou que ele não tem como ficar no governo em um segundo mandato por razões eminentemente pessoais que peço que vocês respeitem", declarou... Na semana passada, a presidente já havia afirmado que fará mudanças em sua equipe de governo se vencer as eleições de outubro...


Resumo 2: Dilma diz que Mantega não fica como ministro em um segundo mandato
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
lazyNamedEntityBalanced
Tamanho do resumo: 563

A presidente Dilma Rousseff afirmou nesta segunda-feira, durante entrevista ao jornal "O Estado de S. Paulo" transmitida pela internet, que o ministro Guido Mantega, da Fazenda, não permanecerá no governo na hipótese de ela vir a ser reeleita... .. Na semana passada, a presidente já havia afirmado que fará mudanças em sua equipe de governo se vencer as eleições de outubro... Na ocasião, o ex-presidente Fernando Henrique Cardoso, à época candidato do PMDB à Prefeitura de São Paulo, posou para a imprensa sentado na cadeira do prefeito um dia antes da eleição.


Resumo 3: Dilma diz que Mantega não fica como ministro em um segundo mandato
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
semantria
Tamanho do resumo: 566

A presidente Dilma Rousseff afirmou nesta segunda-feira, durante entrevista ao jornal "O Estado de S. Paulo" transmitida pela internet, que o ministro Guido Mantega, da Fazenda, não permanecerá no governo na hipótese de ela vir a ser reeleita... "Temos condições de diminuir alguns incentivos e outros não... Delação premiada A presidente afirmou que recorrerá até ao Supremo Tribunal Federal (STF) para saber se há integrantes do governo envolvidos com as denúncias resultantes de delação premiada do ex-diretor da Petrobras Paulo Roberto Costa, preso no Paraná...


Resumo 4: Dilma diz que Mantega não fica como ministro em um segundo mandato
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
eager
Tamanho do resumo: 594

A presidente Dilma Rousseff afirmou nesta segunda-feira, durante entrevista ao jornal "O Estado de S. Paulo" transmitida pela internet, que o ministro Guido Mantega, da Fazenda, não permanecerá no governo na hipótese de ela vir a ser reeleita... Na semana passada, a presidente já havia afirmado que fará mudanças em sua equipe de governo se vencer as eleições de outubro... Questionada se, com a substituição de Mantega, alterará a política econômica na hipótese de reeleição, Dilma voltou a mencionar, sem citar nomes, o episódio da eleição municipal de 1985, em São Paulo, e não respondeu...


Resumos ordenados:
