# -*- coding: utf-8 -*-

import os


BASE_DIR = os.path.dirname(__file__)
DATA_DIR = os.path.join(BASE_DIR, 'data')


from resumeai.summarization import Summarizer


def summarize_text(text):  # pragma: no cover
    strategy = Summarizer(text)
    summary = strategy.summarize()
    return summary
