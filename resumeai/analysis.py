# -*- coding: utf-8 -*-

import math
from resumeai.decomposition import (DecomposedText, DecomposedParagraph,
                                    DecomposedSentence)
from resumeai.extraction import NamedEntityExtractor
from resumeai import hacks
from resumeai import magicnumbers


class BaseAnalysedText(object):
    def _update_entities_dict(self, entities_dict, new_dict):
        entities = entities_dict.copy()
        for e, v in new_dict.items():
            if e in entities.keys():
                keys = list(entities.keys())
                eindex = keys.index(e)
                oldent = keys[eindex]
                if len(oldent.entity) <= len(e.entity):
                    # oldent.entity = e.entity
                    # oldent.variations = e.variations
                    pass

                entities[e] = entities_dict[e] + v
            else:
                entities[e] = v

        return entities

    @property
    def ordered_entities(self):
        entities = [(v, k) for k, v in self.entities.items()]
        entities = [e[1] for e in sorted(entities, reverse=True)]
        return entities


class AnalysedText(BaseAnalysedText, DecomposedText):
    """
    Classe reprenstando um texto já decomposto e analisado, pontuando-se
    sentenças por similaridade de termos e presença de entidades nomeadas.
    """
    def __init__(self, raw_text, language, paragraph_delimiter='\n',
                 sentence_min_size=magicnumbers.SENTENCE_MIN_SIZE):
        self.sentence_min_size = sentence_min_size
        super(AnalysedText, self).__init__(raw_text, language,
                                           paragraph_delimiter)
        self._entities = None
        self._sentences = None

    @property
    def entities(self):
        """
        Retorna todas as entidades nomeadas encontradas no texto.
        """
        if self._entities is not None:
            return self._entities

        entities = {}
        for para in self.paragraphs:
            entities = self._update_entities_dict(entities, para.entities)

        self._entities = entities
        return self._entities

    def _get_paragraphs(self):

        return [
            AnalysedParagraph(
                self, p, self.stopwords, self.sentence_min_size) for p in
            self.raw_text.split(self.paragraph_delimiter) if p]

    @property
    def sentences(self):
        if self._sentences is not None:
            return self._sentences

        sentences = []
        for p in self.paragraphs:
            sentences += p.sentences

        self._sentences = sentences
        return sentences


class AnalysedParagraph(BaseAnalysedText, DecomposedParagraph):
    """
    Classe representando um parágrado de um texto já decomposto e
    analisado. Parágrafo recebe pontuação por posição e as pontuações
    por entidade e similaridade são médias aritiméticas das pontuações
    das sentenças.
    """
    def __init__(self, text, raw_text, stopwords, sentence_min_size):
        self.sentence_min_size = sentence_min_size

        super(AnalysedParagraph, self).__init__(raw_text, stopwords)
        self.text = text
        self._entities = None
        self._similarity_score = None
        self._entities_score = None

    @property
    def entities(self):
        """
        Retorna todas as entidades nomeadas encontradas neste parágrafo
        """
        entities = {}
        if self._entities is not None:  # pragma: no cover
            return self._entities

        for sent in self.sentences:
            sent_entities = sent.entities
            entities = self._update_entities_dict(entities, sent_entities)

        self._entities = entities
        return self._entities

    @property
    def similarity_score(self):
        """
        Retorna a pontuação por similaridade de termos.
        """
        if self._similarity_score is not None:  # pragma: no cover
            return self._similarity_score

        score = 0.0
        if not self.sentences:  # pragma: no cover
            return score

        for sentence in self.sentences:
            score += sentence.similarity_score

        score /= len(self.sentences)
        self._similarity_score = score
        return self._similarity_score

    @property
    def entities_score(self):
        """
        Retorna a pontuação por presença de entidades nomeadas.
        """
        if self._entities_score is not None:  # pragma: no cover
            return self._entities_score

        score = 0.0
        if not self.sentences:  # pragma: no cover
            return score

        for sentence in self.sentences:
            score += sentence.entities_score

        score /= len(self.sentences)
        self._entities_score = score
        return self._entities_score

    @property
    def score(self):
        """
        Retorna a pontuação geral do parágrafo, que é a soma
        da pontuação por posição com a pontuação por entidades e
        com a pontuação por similaridade
        """
        score = (self.entities_score +
                 self.similarity_score)
        return score

    def _get_sentences(self):
        return [
            AnalysedSentence(
                self, s, self.stopwords, self.sentence_min_size) for s in
            self.sent_tokenize(self.raw_text)]


class AnalysedSentence(DecomposedSentence):
    """
    Classe representando uma sentença (frase) que já foi decomposta
    e analisada, tendo sido pontuada de acordo com a similaridade de
    termos e a presença de entidades nomeadas.
    """
    def __init__(self, paragraph, raw_text, stopwords, sentence_min_size):
        self.sentence_min_size = sentence_min_size
        raw_text = hacks.remove_numbers(raw_text)

        super(AnalysedSentence, self).__init__(raw_text, stopwords)
        self.paragraph = paragraph
        self.entity_extractor = NamedEntityExtractor(self.stopwords)
        self.entities = self.entity_extractor.extract_sentence_entities(
            self.words)

        self._similarity_score_dict = {}
        self._similarity_score = None
        self._entities_score = None

    @property
    def score(self):  # pragma: no cover
        """
        Retorna a pontuação geral da sentença, que é a soma da
        pontuação de similaridade com a pontuação de presença de
        entidades nomeadas.
        """
        return self.similarity_score + self.entities_score

    # @property
    # def text_rank(self):
    #     rank = 0.0
    #     for sentence in self.paragraph.sentences:
    #         pass

    @property
    def similarity_score(self):
        """
        Retorna a pontuação por similaridade de termos.
        """
        if self._similarity_score is not None:  # pragma: no cover
            return self._similarity_score

        final_score = 0.0
        if len(self) <= self.sentence_min_size:
            self._entities_score = final_score
            return self._entities_score

        for sentence in self.paragraph.text.sentences:
            words = sentence.important_words
            if sentence == self or len(words) == 0:
                self._similarity_score_dict[sentence] = final_score
                continue

            intersection = len(self.important_words.intersection(words))
            score = intersection / (math.log(len(self.important_words)) +
                                    math.log(len(words)))
            final_score += score
            self._similarity_score_dict[sentence] = score

        self._similarity_score = final_score
        return self._similarity_score

    @property
    def entities_score(self):
        """
        Retorna a pontuação por presença de entidades nomeadas.
        """
        if self._entities_score is not None:  # pragma: no cover
            return self._entities_score

        score = 0.0
        if len(self) <= self.sentence_min_size:
            self._entities_score = score
            return self._entities_score

        for i, entity in enumerate(self.paragraph.text.ordered_entities):
            if entity.entity in self.raw_text \
               or [v for v in entity.variations if v in self.raw_text]:
                try:
                    extra = magicnumbers.ENTITIES[i]
                except IndexError:
                    extra = magicnumbers.ENTITIES_DEFAULT
                score += extra

        score /= (len(self.important_words) /
                  magicnumbers.ENTITIES_DIVISION)

        self._entities_score = score
        return self._entities_score

    def __hash__(self):
        return hash(self.raw_text)
