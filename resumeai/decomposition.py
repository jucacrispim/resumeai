# -*- coding: utf-8 -*-

import nltk
from resumeai import hacks

STOPWORDS = nltk.corpus.stopwords.words('portuguese')


class BaseDecomposedText(object):
    def __init__(self, raw_text):
        self.raw_text = raw_text.strip()

    def __len__(self):
        return len(self.raw_text)

    def __eq__(self, other):
        if hasattr(other, 'raw_text'):
            return self.raw_text == other.raw_text
        return self.raw_text == other

    def __gt__(self, other):
        if hasattr(other, 'raw_text'):
            return self.raw_text > other.raw_text
        return self.raw_text > other

    def __lt__(self, other):
        if hasattr(other, 'raw_text'):
            return self.raw_text < other.raw_text
        return self.raw_text < other


class DecomposedText(BaseDecomposedText):
    """
    Classe representando um texto que foi decomposto
    em pagrágrafos e sentenças (frases).
    """
    def __init__(self, raw_text, language, paragraph_delimiter='\n'):
        super(DecomposedText, self).__init__(raw_text)
        self.stopwords = STOPWORDS

        self.paragraph_delimiter = paragraph_delimiter
        self.paragraphs = self._get_paragraphs()

    def _get_paragraphs(self):
        return [DecomposedParagraph(p, self.stopwords) for p in
                self.raw_text.split(self.paragraph_delimiter) if p]


class DecomposedParagraph(BaseDecomposedText):
    """
    Classe representando um parágrafo de um texto decomposto.
    O parágrafo é decomposto em sentenças.
    """
    def __init__(self, raw_text, stopwords):
        super(DecomposedParagraph, self).__init__(raw_text)
        self.stopwords = stopwords
        self.sentence_tokenizer = nltk.data.load(
            'tokenizers/punkt/portuguese.pickle')
        self.sentences = self._get_sentences()

    def _get_sentences(self):
        return [DecomposedSentence(s, self.stopwords) for s in
                self.sent_tokenize(self.raw_text)]

    def sent_tokenize(self, raw_text):
        """
        Tokeniza sentenças preservando as citações como uma sentença única.
        """
        sentences = self.sentence_tokenizer.tokenize(raw_text)
        sentences = hacks.handle_quotations(raw_text, sentences)
        return sentences


class DecomposedSentence(BaseDecomposedText):
    """
    Classe representando uma sentença decomposta em palavras.
    """
    def __init__(self, raw_text, stopwords):
        super(DecomposedSentence, self).__init__(raw_text)
        self.stopwords = stopwords
        self.stemmer = nltk.stem.RSLPStemmer()
        self.words = nltk.word_tokenize(raw_text)
        self._important_words = None

    @property
    def important_words(self):
        """
        Set das palavras importantes desta sentença.
        Palavras importantes são palavras que não constam
        em self.stopwords. Tdoas as palavras importantes são
        ``stemmerzadas``, isto é, têm seu sufixo retirando.
        """
        if self._important_words is not None:  # pragma: no cover
            return self._important_words

        trash = ['.', '!', ',', '?', ':', ';', '(', ')', '[', ']', '-']
        words = set([self.stemmer.stem(p) for p in self.words
                     if p.lower() not in self.stopwords
                     and p not in trash
                     and not p.isnumeric()])
        self._important_words = words
        return self._important_words
