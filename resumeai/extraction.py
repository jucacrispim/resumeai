# -*- coding: utf-8 -*-

import os
import nltk
import pickle
import resumeai


_notentities_file = os.path.join(resumeai.DATA_DIR, 'notentities.pickle')
_tagger_file = os.path.join(resumeai.DATA_DIR, 'mac_morpho_aubt.pickle')

with open(_notentities_file, 'rb') as f:
    NOTENTITIES = pickle.loads(f.read())

with open(_tagger_file, 'rb') as f:
    MAC_MORPHO_TAGGER = pickle.loads(f.read())


class NamedEntityExtractor(object):
    """
    Extrator de entidades nomeadas presentes em textos.
    Usa extrai entidades baseado no pos_tagger default do nltk
    e também usando um pos_tagger treinado com o corpus mac_morpho.
    No fim, faz a junção das entidades encontradas pelos dois.
    """
    def __init__(self, stopwords):
        self.stopwords = stopwords
        self.notentities = NOTENTITIES
        self.mac_morpho_tagger = MAC_MORPHO_TAGGER

    def extract_entities(self, text):
        """
        Extrai as entidades de um texto inteiro, já decomposto.
        """
        scored_entities = {}
        for para in text.paragraphs:
            for sent in para.sentences:
                scored = self.extract_sentence_entities(sent.words)
                for e, v in scored.items():
                    if e in scored_entities.keys():
                        scored_entities[e] = scored_entities[e] + v
                    else:
                        scored_entities[e] = v

        return scored_entities

    def extract_sentence_entities(self, words):
        """
        Extrai as entidades nomeadas de uma sentença já decomposta.
        Recebe como parâmetro uma lista com as palavras da sentença
        (sentence.words).
        """
        base_entities = self._extract_entities_default(words)
        mac_morpho_entities = self._extract_entities_trained_mac_morpho(words)
        entities = self._merge_entities(base_entities, mac_morpho_entities)
        return entities

    def _extract_entities_default(self, words):
        # extrai entidades usando o pos tagger padrão
        # do nltk.
        tagged = nltk.pos_tag(words)
        chunked = nltk.chunk.ne_chunk(tagged, binary=True)
        scored_entities = self._extract_entities_from_chunked(chunked, words)
        return scored_entities

    def _extract_entities_trained_mac_morpho(self, words):
        # extrai entidades usando o pos tagger treinado
        # com o corpus mac_morpho
        tagged = self._pos_tag_mac_morpho(words)
        chunked = self._ne_chunk_mac_morpho(tagged)
        scored_entities = self._extract_entities_from_chunked(
            chunked, words)
        scored_entities = self._correct_entities_mac_morpho(scored_entities)
        return scored_entities

    def _correct_entities_mac_morpho(self, scored_entities):
        # Corrige entidades que são errôneamente consideradas
        # ou que tem palavras erradas como parte da entidade.
        corrected_entities = {}
        for k, v in scored_entities.items():
            entity = self._correct_entity(k)
            if entity:
                corrected_entities[entity] = v

        return corrected_entities

    def _correct_entity(self, entity):
        # desconsidera palavras que são adicionadas no final
        # de entidades como se fossem parte do nome, mas não são
        # como 'Aécio Neves esteve' vira 'Aécio Neves'
        while (entity.last_word in self.notentities
               or entity.last_word in self.stopwords
               or entity.last_word.islower()):
            entity = entity.strip(entity.last_word).strip(' ')

        # O mac_morpho pega umas coisas bizzarras.
        # tipo, coisas com uma letra só, palavras comuns
        # mas em caixa alta. Aqui se desconsidera isso.
        if len(entity.entity) > 1 and \
           entity.entity.lower() not in self.notentities and \
           not(entity.isupper() and len(entity.split()) > 1):

            return entity

    def _pos_tag_mac_morpho(self, words):
        # part of speech tagger treinado com o corpus mac_morpho
        # tagger = nltk.data.load('taggers/mac_morpho_aubt.pickle')
        tagger = self.mac_morpho_tagger
        tagged = tagger.tag(words)
        return tagged

    def _ne_chunk_mac_morpho(self, tagged):
        # Agrupa entidades nomeadas a partir do texto pos tagged
        # com mac_morpho
        chunker = nltk.chunk.RegexpParser("""NE: {<NPROP>+<ADJ+?NPROP+>?}""")
        chunked = chunker.parse(tagged)
        return chunked

    def _merge_entities(self, base_entities, trained_entities):
        # junta as entidades encontradas por dois
        # taggers - e chunkers -  distintos
        final_entities = {}
        for k, v in base_entities.items():
            if k in trained_entities.keys():
                for nk in trained_entities.keys():
                    if k == nk:
                        if len(k.entity) > len(nk.entity):
                            nk = k
                        break

                if not (nk.isupper() and len(nk.split()) > 1):
                    final_entities[nk] = v
            else:
                if not (k.isupper() and len(k.split()) > 1):
                    final_entities[k] = v

        for k, v in trained_entities.items():
            if (k not in final_entities.keys()):
                final_entities[k] = v

        return final_entities

    def _extract_entities_from_chunked(self, chunked, words):
        # extrai entidades nomeadas "NE" de textos já agrupados
        # e classifica de acordo com a quantidade de aparições.
        scored_entities = {}
        for c in chunked:
            entities = self._extract_entities_from_chunked_text(c)
            entities = [self._correct_entity(e) for e in entities]
            if not entities:
                continue

            for entity in entities:
                # entity = self._correct_entity(entity)
                if not entity or not self._is_valid_entity(entity):
                    continue

                if entity in scored_entities.keys():
                    entity = entities[entities.index(entity)]

                # tentando separar a primeira palavra da frase, que geralmente
                # é reconhecida erroneamente como uma NE
                if words[0] == entity.entity \
                   or (words[0] == entity.first_word and
                       entity.first_word.lower() in self.notentities):
                    score = 0
                else:
                    score = 1

                entity_score = scored_entities.get(entity, 0)
                entity_score += score
                scored_entities[entity] = entity_score

        entities = dict([(e, v) for e, v in scored_entities.items()
                         if v > 0])
        return entities

    def _is_valid_entity(self, entity):
        # às vezes o tagger mac_morpho marca coisas como
        # 'João distraiu-se' como se fosse uma NE
        if entity.entity.lower() in self.stopwords \
           or entity.entity.lower() in self.notentities \
           or not entity.entity[0].isupper() \
           or entity.entity.endswith('-se'):

            return False
        return True

    def _extract_entities_from_chunked_text(self, chunked):
        entities = []
        # aqui pega algumas coisas que as NEs do nltk deixa passar.
        # Depois serão filtradas na regrinha do >= 0.
        if not hasattr(chunked, 'label'):
            return entities

        # procurando só por entidades nomeadas (named entities - NE)
        if chunked.label() == 'NE':
            ent_list = [c[0] for c in chunked if c[0] != '.']
            while ent_list and ent_list[0].islower():
                ent_list.pop(0)

            if not ent_list:
                return entities

            ent = ' '.join(ent_list)
            entities.append(NamedEntity(ent))
        else:
            for c in chunked:
                entities += self._extract_entities_from_chunked_text(c)
        return entities


class NamedEntity(object):
    def __init__(self, entity):
        self.entity = entity
        self.variations = self._make_variations(entity)

    def _make_variations(self, entity_str):
        splited = entity_str.split()
        variations = splited.copy()
        if len(splited) >= 3:
            v = ' '.join(entity_str.split(' ', 2)[:2])
            if v not in variations:
                variations.append(v)

            v = ' '.join(entity_str.rsplit(' ', 2)[-2:])
            if v not in variations:
                variations.append(v)
        return list(variations)

    @property
    def last_word(self):
        return self.entity.rsplit(' ', 1)[-1]

    @property
    def first_word(self):
        return self.entity.split(' ')[0]

    def strip(self, chars=''):
        stripped = self.entity.strip(chars)
        return self.__class__(stripped)

    def __eq__(self, other):
        try:
            strtype = basestring
        except NameError:
            strtype = str

        isstr = isinstance(other, strtype)
        if not isstr and not hasattr(other, 'entity'):
            return False

        if isstr:
            if other == self.entity or other in self.variations:
                return True

        else:
            if other.entity == self.entity \
               or other.entity in self.variations \
               or self.entity in other.variations:
                return True

        return False

    def __lt__(self, other):
        try:
            strtype = basestring
        except NameError:
            strtype = str

        isstr = isinstance(other, strtype)
        if not isstr and not hasattr(other, 'entity'):
            return NotImplemented

        if isstr:
            return self.entity < other
        return self.entity < other.entity

    def __gt__(self, other):
        try:
            strtype = basestring
        except NameError:
            strtype = str

        isstr = isinstance(other, strtype)
        if not isstr and not hasattr(other, 'entity'):
            return NotImplemented

        if isstr:
            return self.entity > other
        return self.entity > other.entity

    def __hash__(self):
        # ???
        # :'(
        # mas funciona!
        # Isso aqui tá muito errado. Mas é o seguinte:
        # Pra fazer um `coisa in lista` eu preciso de __hash__
        # mas esse cara não dá pra fazer um hash decente, porque
        # se fizer um hash de entity ou variations ou tudo junto
        # não vai achar só a string com uma coisa. Então o hash
        # bizarro é só pra o in lista, mas não use esse cara num set()
        # não vai funcionar.
        return hash((1))

    def __repr__(self):  # pragma: no cover
        return repr(self.entity)

    def lower(self):
        lower = self.entity.lower()
        return self.__class__(lower)

    def isupper(self):
        for v in self.variations:
            if not v.isupper():
                return False
        return True

    def split(self, chars=' '):
        return self.entity.split(chars)
