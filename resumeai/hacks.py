# -*- coding: utf-8 -*-

import re


def is_interview(analised_text):
    """
    Classifica um texto como  uma entervista ou não.
    Quando é uma entrevista, precisamos de um outro
    método para sumarização porque num resumo de
    3 ou 4 parágrafos não podemos deixar que entrem
    perguntas que ficarão sem respostas.
    """
    # Aqui tento achar padrão do tipo NOME-DA-REVISTA - Aqui a pergunta...
    # É assim que eu identifico uma entrevista.
    interview_probability_score = 0
    interview_signs = ['-', chr(8211)]
    for i, paragraph in enumerate(analised_text.paragraphs):
        if len(paragraph.sentences) < 1:  # pragma: no cover
            continue

        s = paragraph.sentences[0]

        if len(s.words) < 2:  # pragma: no cover
            continue

        if s.words[1] in interview_signs:
            interview_probability_score += 1

    text_score = (len(analised_text.paragraphs) / 10.0) * 8.0
    if interview_probability_score >= text_score:
        return True
    else:
        return False


def remove_numbers(raw_text):
    """
    aqui remove numeração no começo das frases, coisa que há
    em frases que fazem parte de uma lista."""

    pat = re.compile('(^\d+.*?)\w')
    found = re.findall(pat, raw_text) or []
    for f in found:
        raw_text = raw_text.replace(f, '')
    return raw_text


def handle_quotations(raw_text, sentences):
    """
    Trata citações como uma só sentença, evitando a perda de sentido
    nas declarações de alguém.
    """
    pat = re.compile('["|“].*?["|”].*?[\.*|\?*|!*]')
    quotations = re.findall(pat, raw_text)
    new_sentences = sentences.copy()
    for sentence in sentences:
        for quotation in quotations:
            if sentence in quotation:
                try:
                    i = new_sentences.index(sentence)
                except ValueError:  # pragma: no cover
                    continue

                if quotation not in new_sentences:
                    new_sentences[i] = quotation
                else:
                    new_sentences.pop(i)
    return new_sentences
