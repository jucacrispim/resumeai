# -*- coding: utf-8 -*-

# estes números são para balancear as equações de pontuação

# Estes aqui são pras entidades. A mais importante paga o primeiro,
# a segunda mais importante o segundo número e assim por diante.
# Em caso de index error, pega o default.
ENTITIES = [0.0]
ENTITIES_DEFAULT = 0.0
# Aqui faz uma divisão do valor que divide o valor das entidades.
# Quanto maior o número maior o peso que entidades terão nas sentenças.
ENTITIES_DIVISION = 4

# A aqui a quantidade de sentenças que usaremos
# para fazer resumo
SENTENCES_QUANTITY = 5

# esse número aqui é para excluir sentenças menores que isso
SENTENCE_MIN_SIZE = 70
# Aqui é pra calibrar o tamanho do resumo
SUMMARY_MIN_SIZE = 450
SUMMARY_MAX_SIZE = 650

# Parâmetro do TextRank - que vem do PageRank. Pode ser qualquer
# valor entre 0 e 1. O valor usado aqui é o mesmo usado por
# Mihalcea (2004) e Brin & Page (1998).
DAMPING_FACTOR = 0.85
