# -*- coding: utf-8 -*-

import re
from resumeai import analysis, magicnumbers, hacks


class SummarizedText(object):
    """
    Classe representando um texto já sumarizado.
    Para criação do sumário, primeiro são descartados
    os parágrafos com menor pontuação e depois separam-se
    as frases com maior pontuação dos parágrafos restantes.
    """
    def __init__(self, analised_text, sentences_quantity=None,
                 summary_max_size=None, summary_min_size=None):
        self.analised_text = analised_text
        self.sentences_quantity = sentences_quantity
        self.summary_max_size = summary_max_size
        self.summary_min_size = summary_min_size
        self.important_sentences = self._get_important_sentences()
        self.summary = self._create_summary()

    @property
    def entities(self):
        """
        Retorna as entidades presentes no texto original usado para criar
        o resumo.
        """
        entities_dict = self.analised_text.entities
        entities = [(v, k) for k, v in entities_dict.items()]
        entities = [e[1] for e in sorted(entities, reverse=True)]
        return entities

    @property
    def original_text(self):
        """
        Retorna o texto original usado para criar o resumo.
        """
        return self.analised_text.raw_text

    def _get_important_sentences(self):
        # Aqui remove os parágrafos menos pontuados
        # até que só sobre a quantidade de parágrafos
        # pedida

        if hacks.is_interview(self.analised_text):
            # se é entrevista, só consideramos o primeiro parágrafo
            # para criar o resumo, que geralmente é um parágrafo
            # de introdução.
            try:
                text = [s for s in self.analised_text.paragraphs[0].sentences]
            except IndexError:  # pragma: no cover
                text = []
        else:
            text = [s for s in self.analised_text.sentences if s]

        if self.sentences_quantity is None:  # pragma: no cover
            return text

        while len(text) > self.sentences_quantity:
            worst = self._get_worst_sentence(text)
            text.pop(text.index(worst))
        return text

    def _get_worst_sentence(self, text):
        # pega a sentença com a pior nota.
        worst = None
        for paragraph in text:
            if not worst or paragraph.score <= worst.score:
                worst = paragraph

        return worst

    def _create_summary(self):
        # paga a melhor sentença de cada parágrafo para criar o
        # sumário
        if len(self.original_text) <= self.summary_min_size:
            return self.original_text

        summary = [self._remove_parens(s.raw_text)
                   for s in self.important_sentences]
        strsummary = '.. '.join(summary) + '..'

        if not self.summary_max_size:  # pragma: no cover
            return strsummary

        # aqui controla o tamanho do sumário. Enquanto
        # o sumário é maior que o máximo, retira a última
        # frase.
        while len(strsummary) > self.summary_max_size:
            summary.pop()
            strsummary = '.. '.join(summary) + '..'
        return '.. '.join(summary) + '..'

    def _remove_parens(self, sentence):
        # remove coisas entre parênteses nos textos.
        for parens in re.findall(re.compile('\s*?\(.*?\)'), sentence):
            sentence = sentence.replace(parens, '')
        return sentence


class Summarizer(object):
    """
    Uma simples classe que faz um resumo de um texto.
    """
    def __init__(self, raw_text, language='portuguese',
                 sentence_min_size=magicnumbers.SENTENCE_MIN_SIZE,
                 summary_min_size=magicnumbers.SUMMARY_MIN_SIZE,
                 summary_max_size=magicnumbers.SUMMARY_MAX_SIZE,
                 sentences_quantity=magicnumbers.SENTENCES_QUANTITY,
                 paragraph_delimiter='\n'):

        self.raw_text = raw_text
        self.language = language
        self.sentence_min_size = sentence_min_size
        self.summary_min_size = summary_min_size
        self.summary_max_size = summary_max_size
        self.sentences_quantity = sentences_quantity
        self.paragraph_delimiter = paragraph_delimiter

        self.text = analysis.AnalysedText(self.raw_text, self.language)

    def summarize(self):
        summarized = SummarizedText(self.text, self.sentences_quantity,
                                    self.summary_max_size,
                                    self.summary_min_size)
        return summarized
