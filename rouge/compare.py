# -*- coding: utf-8 -*-

import os
import sys
import pyrouge
import summtests


logger = pyrouge.utils.log.get_global_console_logger()

BASE_DIR = os.path.dirname(__file__)
FONTES_DIR = os.path.join(BASE_DIR, 'textos-fonte')
MODELS_DIR = os.path.join(BASE_DIR, 'models')
SYSTEM_DIR = os.path.join(BASE_DIR, 'system')


def run_rouge(output_title, fname=None):

    r = pyrouge.Rouge155()
    r.system_dir = SYSTEM_DIR
    r.model_dir = MODELS_DIR
    r.system_filename_pattern = 'St-(.*).txt'
    r.model_filename_pattern = '(Ext|Sum)-#ID#.txt'
    output = r.convert_and_evaluate()
    output = r.output_to_dict(output)

    keys = ['rouge_1_precision', 'rouge_1_recall', 'rouge_1_f_score',]
    res = {k: v for k, v in output.items() if k in keys}

    output = _format_output(res, output_title)
    print(output)
    if fname:
        if os.path.exists(fname):
            mode = 'a'
        else:
            mode = 'w'

        with open(fname, mode) as fd:
            fd.write(output)


def resumir_textos_fonte(summfunc):
    files = [f for f in os.listdir(FONTES_DIR) if not f.startswith('.')]

    for f in files:
        with open(os.path.join(FONTES_DIR, f), 'r') as fd:
            content = fd.read()

            logger.info('resumindo %s' % f)
        resumo = summfunc(content)
        with open(os.path.join(SYSTEM_DIR, f), 'w') as fd:
            fd.write(resumo)


def _format_output(res, title):
    strout = "\n\n############# RESULTADO #############\n\n"
    strout += '%s\n\n' % title
    strout += 'ROUGE-1:\n\n'
    strout += '  Precisão: %s\n' % res['rouge_1_precision']
    strout += '  Cobertura: %s\n' % res['rouge_1_recall']
    strout += '  F-measure: %s\n' % res['rouge_1_f_score']
    return strout


def _get_cmdline(argv):
    valid = ['--no-summ', '--random', '--simple', '--title',
             '--fname']
    opts = {}
    for v in valid:
        for a in argv:
            if v in a:
                o = a.strip('--')
                if '=' in o:
                    k, val = o.split('=')
                else:
                    k, val = o, True

                opts[k] = val
    return opts


def compare(opts):
    fname = opts.get('fname')
    title = opts.get('title', '')
    if opts.get('no-summ'):
        run_rouge(title, fname)
        return

    if opts.get('random'):
        summfunc = summtests.random_summ

    elif opts.get('simple'):
        summfunc = summtests.simple_summ

    else:
        summfunc = summtests.ok_summ


    resumir_textos_fonte(summfunc)
    run_rouge(title, fname)


if __name__ == '__main__':
    opts = _get_cmdline(sys.argv)
    compare(opts)
