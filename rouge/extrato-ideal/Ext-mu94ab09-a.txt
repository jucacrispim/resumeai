Os casos de abuso e maus-tratos de crianças continuam a chegar com persistência desalentadora -cerca de 3 milhões de casos em 1993, segundo um relatório entregue do Comitê Nacional de Prevenção do Abuso Infantil (NCPCA).
A crise americana de abuso infantil cresce quando as crianças nascem no mesmo ambiente de pobreza, dependência de drogas e abuso em que seus pais foram criados.
Em maio do ano passado, assistentes sociais de Monticello, Kentucky (EUA), descobriram que o garotinho Daniel Reynolds apresentava hematomas.
Em junho os médicos disseram que a perna direita do garoto, que estava quebrada, parecia ter sido torcida até quebrar.
E no início de dezembro, segundo a polícia e os promotores, Daniel morreu de um soco na cabeça.
Algumas dessas mortes poderiam ter sido impedidas.
As meninas da família Schoo, cujos pais as deixaram sozinhas em casa enquanto tiravam férias no México.
Ela cresce quando organizações com financiamento insuficiente enviam assistentes sociais com carga excessiva de trabalho a lares infelizes e pobres, para "salvar famílias".
"E as estatísticas mais recentes sobre abuso de crianças talvez reflitam apenas "50% dos casos que realmente acontecem", diz Philip McClain, dos Centros para o Controle de Enfermidades.
Problemas insolúveisDesde que um estudo feito em 1962 identificou pela primeira vez a "Síndrome da Criança Espancada", o plano de combate ao abuso infantil foi modificado repetidas vezes.
"Nos anos 80 chegaram os advogados e os grupos de defesa, contestando aquela posição".
"É por isso que o público se sente revoltado", diz Joy Byers, do NCPCA.
