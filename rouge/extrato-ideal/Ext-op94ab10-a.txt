A mais recente pesquisa do Datafolha sobre a intenção de voto para a Presidência sugere um quadro radicalmente diferente do que havia no pleito anterior, na mesma altura do ano.
Mais exatamente, entre Luiz Inácio Lula da Silva (do PT) e Fernando Henrique Cardoso (do PSDB).
De fato, nas três situações propostas aos pesquisados, Fernando Henrique assume um nítido segundo lugar, ao contrário do que ocorria em todas as sondagens anteriores.
Fica também evidente que uma eventual candidatura Sarney pelo PMDB continua mais forte do que a de Quércia, tira mais votos de Lula do que de FHC e é a que mais ameaça o cenário suposto de bipolarização.
FHC parece ter-se beneficiado da renúncia de Paulo Maluf à candidatura, embora parte dos votos potenciais do prefeito paulistano tenham claramente migrado igualmente para Lula.
Antes, o segundo lugar era disputado pelo próprio FHC, mas também por Antônio Britto (PMDB), Paulo Maluf (PPR) e José Sarney (PMDB), tendo Leonel Brizola (PDT) bastante próximo.
Em vez de um numeroso leque de candidatos com alguma ou muita chance de passar para o turno final, desta vez o que a pesquisa indica é uma disputa bipolar.
Se de fato for assim, o segundo turno será uma disputa insólita em um país de forte presença conservadora: a direita não terá nenhum nome de seus quadros presente na batalha final.
