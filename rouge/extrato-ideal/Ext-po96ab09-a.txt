BRASÍLIA - A Câmara dos Deputados vai gastar R$ 12,25 milhões em 1996 para conservar e reformar os 420 apartamentos funcionais ocupados pelos deputados federais.
Pagar o auxílio moradia aos 83 deputados que não têm apartamento funcional consome mais R$ 2,98 milhões anuais da Câmara.
O deputado Augusto Carvalho (PPS-DF) fez o levantamento dos gastos da Câmara com imóveis no Siafi, o sistema informatizado que controla todos os gastos públicos da União.
Neste caso, o parlamentar pode usar os R$ 3 mil por mês sem pagar qualquer imposto.
Optou então por entregar o valor recebido a uma creche de Brasília.
A conta envolve desde reformas em apartamentos até a compra de eletrodomésticos e o pagamento de empresas que fazem vigilância e limpeza nos prédios.
