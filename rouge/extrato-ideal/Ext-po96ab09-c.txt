Dois meses depois de amargar a reprovação de 86% dos cariocas - que o consideraram omisso quando da enchente responsável pela morte de 67 pessoas -, o governador Marcello Alencar conseguiu recuperar, em parte, a credibilidade junto ao eleitor.
A pesquisa JB-Vox Populi feita nos dias 30 e 31 de março com 697 moradores da capital apontou um índice de 54% de avaliação positiva do governo do Rio de Janeiro.
Sem dúvida, é uma recuperação, mas a administração do governador teve muito mais conceito péssimo (18%) do que ótimo (4%).
O diretor do Vox Populi, Marcos Coimbra, chama a atenção para o fato de os eleitores da capital serem mais normalmente mais exigentes e críticos com os governantes do que os do interior.
O eleitor da capital foi decisivo para eleger Marcello Alencar no segundo turno.
Logo no início do ano passado, conseguiu do presidente Fernando Henrique Cardoso a promessa de investimentos no Rio, para obras como ampliação do Porto de Sepetiba e conclusão do metrô.
Além disso, conseguiu trazer para o Rio a primeira fábrica de caminhões da Volkswagen, o que lhe garantiu grande prestígio com a população.
A onda de seqüestros, a partir de junho de 1995, levou os cariocas a cobrar uma polícia mais eficiente e menos corrupta.
Em uma comparação com os demais governadores tucanos, ficou bem atrás (54%) de Tasso Jereissati (76%) e Eduardo Azeredo (70%), mas saiu-se melhor do que Mário Covas, de São Paulo, que teve 47% de avaliação positiva.
Teve a aprovação de pouco mais da metade dos entrevistados.
