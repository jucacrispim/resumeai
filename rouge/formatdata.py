# -*- coding: utf-8 -*-

import os
import sys
import logging
import pyrouge
from resumeai import summarize_text

logger = pyrouge.utils.log.get_global_console_logger()

BASE_DIR = os.path.dirname(__file__)
FONTES_DIR = os.path.join(BASE_DIR, 'textos-fonte')
MODELS_DIR = os.path.join(BASE_DIR, 'models')
SYSTEM_DIR = os.path.join(BASE_DIR, 'system')

def convert2utf():
    files = [f for f in os.listdir(FONTES_DIR) if not f.startswith('.')]
    for f in files:
        fname = os.path.join(FONTES_DIR, f)
        os.system('iconv -f ISO-8859-1 -t UTF-8 %s > %s.new' %(fname, fname))
        os.system('mv %s.new %s' %(fname, fname))

    files = [f for f in os.listdir(MODELS_DIR) if not f.startswith('.')]
    for f in files:
        fname = os.path.join(MODELS_DIR, f)
        os.system('iconv -f ISO-8859-1 -t UTF-8 %s > %s.new' %(fname, fname))
        os.system('mv %s.new %s' %(fname, fname))

def resumir_fontes():
    files = [f for f in os.listdir(FONTES_DIR) if not f.startswith('.')]

    for f in files:
        with open(os.path.join(FONTES_DIR, f), 'r') as fd:
            content = fd.read()

            logger.info('resumindo %s' % f)
        resumo = summarize_text(content)
        with open(os.path.join(SYSTEM_DIR, f), 'w') as fd:
            fd.write(resumo.summary)


def run_rouge(full=False):
    r = pyrouge.Rouge155()
    r.system_dir = SYSTEM_DIR
    r.model_dir = MODELS_DIR
    r.system_filename_pattern = 'St-(.*).txt'
    r.model_filename_pattern = '(Ext|Sum)-#ID#.txt'
    output = r.convert_and_evaluate()
    output = r.output_to_dict(output)

    print('\n\n######## RESULTADOS #########\n')
    p = output['rouge_1_precision']
    r = output['rouge_1_recall']
    f_score = output['rouge_1_f_score']
    total1 = sum([ p + r + f_score ]) / 3
    print('ROUGE-1: %s' % total1)
    if full:
        msg = '  Precision: %s\n' % p
        msg += '  Recall: %s\n' % r
        msg += '  F-Score: %s\n' % f_score
        print(msg)

    p = output['rouge_2_precision']
    r = output['rouge_2_recall']
    f_score = output['rouge_2_f_score']
    total2 = sum([ p + r + f_score ]) / 3

    print('ROUGE-2: %s' % total2)
    if full:
        msg = '  Precision: %s\n' % p
        msg += '  Recall: %s\n' % r
        msg += '  F-Score: %s\n' % f_score
        print(msg)


if __name__ == "__main__":
    for a in sys.argv:
        if a == '--resumir-textos':
            resumir_fontes()
            sys.exit(0)
        if a == '--convert':
            convert2utf()
            sys.exit(0)
        if a == '--rouge':
            if not '--no-sum' in sys.argv:
                resumir_fontes()

            full = '--full' in sys.argv
            run_rouge(full=full)
            sys.exit(0)

    print('sei lá')
