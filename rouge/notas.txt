# Os que usam entities aqui usam a pontuação [0.3, 0.2, 0.1] e d=0.0


############# RESULTADO #############

Baseline random

ROUGE-1:

  Precisão: 0.54628
  Cobertura: 0.32316
  F-measure: 0.40009


############# RESULTADO #############

Algorítimo simples: sem stopwords, com stemmer, trash words (incluindo isnumeric), sentence_min_size, entities e handle quotations

ROUGE-1:

  Precisão: 0.51746
  Cobertura: 0.42969
  F-measure: 0.46372


############# RESULTADO #############

TextRank: sem stopwords, com stemmer, trash words (incluindo isnumeric) sentence_min_size, entities e handle quotations

ROUGE-1:

  Precisão: 0.5158
  Cobertura: 0.42902
  F-measure: 0.46256


############# RESULTADO #############

TextRank: puro

ROUGE-1:

  Precisão: 0.5048
  Cobertura: 0.44399
  F-measure: 0.46715


############# RESULTADO #############

TextRank: Com handle quotations

ROUGE-1:

  Precisão: 0.51649
  Cobertura: 0.42791
  F-measure: 0.46217


############# RESULTADO #############

TextRank: Com handle quotations e stemmer

ROUGE-1:

  Precisão: 0.51596
  Cobertura: 0.43059
  F-measure: 0.46385


############# RESULTADO #############

TextRank: Com handle quotations, stemmer e trash words (com isnumeric())

ROUGE-1:

  Precisão: 0.51465
  Cobertura: 0.42854
  F-measure: 0.46185


############# RESULTADO #############

TextRank: Com stemmer

ROUGE-1:

  Precisão: 0.50479
  Cobertura: 0.44368
  F-measure: 0.46721


############# RESULTADO #############

Algorítimo simples: Com stemmer

ROUGE-1:

  Precisão: 0.50511
  Cobertura: 0.44429
  F-measure: 0.46765


############# RESULTADO #############

Algorítimo simples: Com stemmer e entities

ROUGE-1:

  Precisão: 0.50461
  Cobertura: 0.44377
  F-measure: 0.46712


############# RESULTADO #############

Algorítimo simples: Com stemmer e sentence_min_size

ROUGE-1:

  Precisão: 0.50404
  Cobertura: 0.44621
  F-measure: 0.46827


############# RESULTADO #############

Algorítimo simples: Com stemmer, sentence_min_size e entities

ROUGE-1:

  Precisão: 0.50421
  Cobertura: 0.44614
  F-measure: 0.4683


############# RESULTADO #############

TextRank: Com stemmer, sentence_min_size e entities

ROUGE-1:

  Precisão: 0.50407
  Cobertura: 0.44618
  F-measure: 0.46827


############# RESULTADO #############

TextRank: Com stemmer e sentence_min_size

ROUGE-1:

  Precisão: 0.50372
  Cobertura: 0.44623
  F-measure: 0.4681


############# RESULTADO #############

TextRank: Com stemmer, sentence_min_size, handle quotations e entities

ROUGE-1:

  Precisão: 0.5153
  Cobertura: 0.43252
  F-measure: 0.46461


############# RESULTADO #############

Algorítimo simples: Com stemmer, sentence_min_size, handle quotations e entities

ROUGE-1:

  Precisão: 0.51594
  Cobertura: 0.43224
  F-measure: 0.46477


############# RESULTADO #############

Algorítimo simples: Com stemmer, sentence_min_size, entities e novo tamanho de sumario (min 3 sents e maximo 10 sents)

ROUGE-1:

  Precisão: 0.51249
  Cobertura: 0.43887
  F-measure: 0.46833
