<html>
<head>
<title>dummy title</title>
</head>
<body bgcolor="white">
<a name="1">[1]</a> <a href="#1" id=1><html></a>
<a name="2">[2]</a> <a href="#2" id=2><head></a>
<a name="3">[3]</a> <a href="#3" id=3><title>dummy title</title></a>
<a name="4">[4]</a> <a href="#4" id=4></head></a>
<a name="5">[5]</a> <a href="#5" id=5><body bgcolor="white"></a>
<a name="6">[6]</a> <a href="#6" id=6><a name="1">[1]</a> <a href="#1" id=1><html></a></a>
<a name="7">[7]</a> <a href="#7" id=7><a name="2">[2]</a> <a href="#2" id=2><head></a></a>
<a name="8">[8]</a> <a href="#8" id=8><a name="3">[3]</a> <a href="#3" id=3><title>dummy title</title></a></a>
<a name="9">[9]</a> <a href="#9" id=9><a name="4">[4]</a> <a href="#4" id=4></head></a></a>
<a name="10">[10]</a> <a href="#10" id=10><a name="5">[5]</a> <a href="#5" id=5><body bgcolor="white"></a></a>
<a name="11">[11]</a> <a href="#11" id=11><a name="6">[6]</a> <a href="#6" id=6><a name="1">[1]</a> <a href="#1" id=1>Ontem, os dois homens demonstraram que política é um jogo de verdades relativas - ou que um segundo turno de eleições zera acusações e insultos trocados no primeiro.</a></a></a>
<a name="12">[12]</a> <a href="#12" id=12><a name="7">[7]</a> <a href="#7" id=7><a name="2">[2]</a> <a href="#2" id=2>MOSCOU - O presidente Boris Yeltsin e o general Alexander Lebed selaram ontem uma aliança com o objetivo de dar a Yeltsin a vitória no segundo turno das eleições presidenciais russas, no início de julho.</a></a></a>
<a name="13">[13]</a> <a href="#13" id=13><a name="8">[8]</a> <a href="#8" id=8><a name="3">[3]</a> <a href="#3" id=3>Lebed vai tentar transferir para Yeltsin os 11 milhões de votos que lhe deram o terceiro lugar no primeiro turno, ajudando-o a derrotar o comunista Guenadi Ziuganov.</a></a></a>
<a name="14">[14]</a> <a href="#14" id=14><a name="9">[9]</a> <a href="#9" id=9><a name="4">[4]</a> <a href="#4" id=4>O economista Grigori Iavlinski, quarto colocado no primeiro turno, declarou, em entrevista transmitida pela televisão, ser contra o voto em Ziuganov.</a></a></a>
<a name="15">[15]</a> <a href="#15" id=15><a name="10">[10]</a> <a href="#10" id=10><a name="5">[5]</a> <a href="#5" id=5>Em troca, em plena campanha eleitoral, foi nomeado assessor de segurança nacional e secretário do Conselho de Segurança, que assessora a presidência em questões de defesa.</a></a></a>
<a name="16">[16]</a> <a href="#16" id=16><a name="11">[11]</a> <a href="#11" id=11><a name="6">[6]</a> <a href="#6" id=6>Mas não o indicou como vice-presidente, cargo que o colocaria direto na linha do poder caso o presidente não consiga concluir o mandato.</a></a></a>
<a name="17">[17]</a> <a href="#17" id=17><a name="12">[12]</a> <a href="#12" id=12><a name="7">[7]</a> <a href="#7" id=7>A grande incógnita da aliança de ocasião entre Yeltsin e Lebed é saber se o presidente está disposto a dar poder suficiente ao general para satisfazer suas ambições políticas, que não são pequenas.</a></a></a>
<a name="18">[18]</a> <a href="#18" id=18><a name="13">[13]</a> <a href="#13" id=13><a name="8">[8]</a> <a href="#8" id=8>Caíram igualmente em desgraça, Yeltsin quando deixou o Politburo para seguir carreira política, em 1987, e Lebed em 1995, quando o então ministro da Defesa, Pavel Grachev, o destituiu do comando do 14° Exército.</a></a></a>
<a name="19">[19]</a> <a href="#19" id=19><a name="14">[14]</a> <a href="#14" id=14><a name="9">[9]</a> <a href="#9" id=9>"Não haverá sublevações", disse Lebed em entrevista à imprensa.</a></a></a>
<a name="20">[20]</a> <a href="#20" id=20><a name="15">[15]</a> <a href="#15" id=15><a name="10">[10]</a> <a href="#10" id=10>Os dois homens não se assemelham apenas na aparência, um tanto ríspida e grosseira.</a></a></a>
<a name="21">[21]</a> <a href="#21" id=21><a name="16">[16]</a> <a href="#16" id=16><a name="11">[11]</a> <a href="#11" id=11>Ambos são produtos das hierarquias soviéticas - Yeltsin, do Partido Comunista; Lebed, da elite de páraquedistas do Exército.</a></a></a>
<a name="22">[22]</a> <a href="#22" id=22><a name="17">[17]</a> <a href="#17" id=17><a name="12">[12]</a> <a href="#12" id=12>Yeltsin, 65 anos, já foi hospitalizado algumas vezes, com problemas cardíacos.</a></a></a>
<a name="23">[23]</a> <a href="#23" id=23><a name="18">[18]</a> <a href="#18" id=18><a name="13">[13]</a> <a href="#13" id=13>Mas a escolha se mostrou desastrosa - Rutskoi foi excluído das decisões de governo pelos assessores liberais do Kremlin, e se vingou denunciando as reformas econômicas do presidente.</a></a></a>
<a name="24">[24]</a> <a href="#24" id=24><a name="19">[19]</a> <a href="#19" id=19><a name="14">[14]</a> <a href="#14" id=14>Em 1993 liderou uma frustrada tentativa de golpe em Moscou e acabou na cadeia, de onde saiu mais tarde.</a></a></a>
<a name="25">[25]</a> <a href="#25" id=25><a name="20">[20]</a> <a href="#20" id=20><a name="15">[15]</a> <a href="#15" id=15></a></a></a>
<a name="26">[26]</a> <a href="#26" id=26><a name="21">[21]</a> <a href="#21" id=21></body></a></a>
<a name="27">[27]</a> <a href="#27" id=27><a name="22">[22]</a> <a href="#22" id=22></html></a></a>
<a name="28">[28]</a> <a href="#28" id=28></body></a>
<a name="29">[29]</a> <a href="#29" id=29></html></a>
</body>
</html>