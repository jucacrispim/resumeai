<html>
<head>
<title>dummy title</title>
</head>
<body bgcolor="white">
<a name="1">[1]</a> <a href="#1" id=1><html></a>
<a name="2">[2]</a> <a href="#2" id=2><head></a>
<a name="3">[3]</a> <a href="#3" id=3><title>dummy title</title></a>
<a name="4">[4]</a> <a href="#4" id=4></head></a>
<a name="5">[5]</a> <a href="#5" id=5><body bgcolor="white"></a>
<a name="6">[6]</a> <a href="#6" id=6><a name="1">[1]</a> <a href="#1" id=1><html></a></a>
<a name="7">[7]</a> <a href="#7" id=7><a name="2">[2]</a> <a href="#2" id=2><head></a></a>
<a name="8">[8]</a> <a href="#8" id=8><a name="3">[3]</a> <a href="#3" id=3><title>dummy title</title></a></a>
<a name="9">[9]</a> <a href="#9" id=9><a name="4">[4]</a> <a href="#4" id=4></head></a></a>
<a name="10">[10]</a> <a href="#10" id=10><a name="5">[5]</a> <a href="#5" id=5><body bgcolor="white"></a></a>
<a name="11">[11]</a> <a href="#11" id=11><a name="6">[6]</a> <a href="#6" id=6><a name="1">[1]</a> <a href="#1" id=1>Anita Malfatti , com sua exposição individual  de pintura em 1917, dá os primeiros sinais da Semana de Arte Moderna de 1922. </a></a></a>
<a name="12">[12]</a> <a href="#12" id=12><a name="7">[7]</a> <a href="#7" id=7><a name="2">[2]</a> <a href="#2" id=2></a></a></a>
<a name="13">[13]</a> <a href="#13" id=13><a name="8">[8]</a> <a href="#8" id=8><a name="3">[3]</a> <a href="#3" id=3>A polêmica em torno do acontecimento ficou marcada na crítica de Monteiro Lobato no artigo Paranóia ou Mistificação, em que ele condena as influências do cubismo e futurismo , tendências em voga na Europa. </a></a></a>
<a name="14">[14]</a> <a href="#14" id=14><a name="9">[9]</a> <a href="#9" id=9><a name="4">[4]</a> <a href="#4" id=4></a></a></a>
<a name="15">[15]</a> <a href="#15" id=15><a name="10">[10]</a> <a href="#10" id=10><a name="5">[5]</a> <a href="#5" id=5>Sob influência inicial do expressionismo alemão, Malfatti se opõe à tendência neoclássica da arte brasileira do início do século XX. Procura inspiração , também, no primitivismo do mundo rural brasileiro. </a></a></a>
<a name="16">[16]</a> <a href="#16" id=16><a name="11">[11]</a> <a href="#11" id=11><a name="6">[6]</a> <a href="#6" id=6></a></a></a>
<a name="17">[17]</a> <a href="#17" id=17><a name="12">[12]</a> <a href="#12" id=12><a name="7">[7]</a> <a href="#7" id=7>Segundo Annateresa Fabris, curadora do segundo módulo da Bienal Brasil Século 20, a modernidade adotada por São Paulo não foi a de Anita, mas a de Victor Brecheret,  que interligava referências do passado com a tradição da escultura. O senso comum dos paulistanos não se chocava com a  estética de Brecheret, menos destrutiva  e mais voltada para  temas  nacionais. </a></a></a>
<a name="18">[18]</a> <a href="#18" id=18><a name="13">[13]</a> <a href="#13" id=13><a name="8">[8]</a> <a href="#8" id=8></a></a></a>
<a name="19">[19]</a> <a href="#19" id=19><a name="14">[14]</a> <a href="#14" id=14><a name="9">[9]</a> <a href="#9" id=9>A crítica se mostra empenhada em  valorizar a elaboração de um discurso nacional,  que priorizasse a brasilidade temática mais do que tudo. Mário de Andrade, por exemplo, considera Cândido Portinari o modelo do artista nacional, por esse motivo. Tal prisma impediu a avaliação de características importantes nas obras de outros artistas da época. </a></a></a>
<a name="20">[20]</a> <a href="#20" id=20><a name="15">[15]</a> <a href="#15" id=15><a name="10">[10]</a> <a href="#10" id=10></a></a></a>
<a name="21">[21]</a> <a href="#21" id=21><a name="16">[16]</a> <a href="#16" id=16><a name="11">[11]</a> <a href="#11" id=11>A partir de 1930, segundo Aguilar, curador da Fundação Bienal, a nossa arte entra num figurativismo cada vez mais dogmático. A moda acaba levando alguns artistas a abandonarem outros vôos e a se enquadrarem. </a></a></a>
<a name="22">[22]</a> <a href="#22" id=22><a name="17">[17]</a> <a href="#17" id=17><a name="12">[12]</a> <a href="#12" id=12></a></a></a>
<a name="23">[23]</a> <a href="#23" id=23><a name="18">[18]</a> <a href="#18" id=18><a name="13">[13]</a> <a href="#13" id=13>Outro curador do segundo módulo, Tadeu Chiarelli , se preocupou em mostrar os efeitos redutores  desse enquadramento para a avaliação da obra de certos artistas. </a></a></a>
<a name="24">[24]</a> <a href="#24" id=24><a name="19">[19]</a> <a href="#19" id=19><a name="14">[14]</a> <a href="#14" id=14></a></a></a>
<a name="25">[25]</a> <a href="#25" id=25><a name="20">[20]</a> <a href="#20" id=20><a name="15">[15]</a> <a href="#15" id=15></a></a></a>
<a name="26">[26]</a> <a href="#26" id=26><a name="21">[21]</a> <a href="#21" id=21></body></a></a>
<a name="27">[27]</a> <a href="#27" id=27><a name="22">[22]</a> <a href="#22" id=22></html></a></a>
<a name="28">[28]</a> <a href="#28" id=28></body></a>
<a name="29">[29]</a> <a href="#29" id=29></html></a>
</body>
</html>