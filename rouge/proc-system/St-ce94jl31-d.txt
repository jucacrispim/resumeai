<html>
<head>
<title>dummy title</title>
</head>
<body bgcolor="white">
<a name="1">[1]</a> <a href="#1" id=1><html></a>
<a name="2">[2]</a> <a href="#2" id=2><head></a>
<a name="3">[3]</a> <a href="#3" id=3><title>dummy title</title></a>
<a name="4">[4]</a> <a href="#4" id=4></head></a>
<a name="5">[5]</a> <a href="#5" id=5><body bgcolor="white"></a>
<a name="6">[6]</a> <a href="#6" id=6><a name="1">[1]</a> <a href="#1" id=1><html></a></a>
<a name="7">[7]</a> <a href="#7" id=7><a name="2">[2]</a> <a href="#2" id=2><head></a></a>
<a name="8">[8]</a> <a href="#8" id=8><a name="3">[3]</a> <a href="#3" id=3><title>dummy title</title></a></a>
<a name="9">[9]</a> <a href="#9" id=9><a name="4">[4]</a> <a href="#4" id=4></head></a></a>
<a name="10">[10]</a> <a href="#10" id=10><a name="5">[5]</a> <a href="#5" id=5><body bgcolor="white"></a></a>
<a name="11">[11]</a> <a href="#11" id=11><a name="6">[6]</a> <a href="#6" id=6><a name="1">[1]</a> <a href="#1" id=1>Comparada com a potencialidade econômica do país, o nível da educação básica brasileira está em último lugar no mundo. Para se determinar a posição de cada nação, o Unicef comparou a taxa de evasão escolar com o Produto Interno Bruto per capita soma de todos os bens e serviços produzidos, dividida pela população do país. Com base em dados fornecidos pelo Ministério da Educação, a lista da ONU informa que apenas 39% chegam a este estágio. Eles deveriam ter matriculados 88% das crianças até a 5ª série: sua taxa é de 94%, comparável aos países mais desenvolvidos do planeta. "É um tremendo desperdício de recursos. Tivéssemos uma taxa menor, haveria menos sobrecarga de professores e salas de aula", afirma Hingel, numa opinião compartilhada pela imensa maioria dos educadores brasileiros. Ao se aprofundar a discussão do desperdício da repetência, encontra-se um antigo mito: a criança sai da escola por falta de condições econômicas da família. As mais recentes pesquisas estão demonstrando que a família valoriza a educação, vista como um mecanismo de ascensão social.Mas o aluno não consegue progredir, assimilar conhecimento e, depois de várias tentativas em meio à repetência, engrossa as estatatísticas de evasão escolar. Há uma série de fatores que confluem: os professores recebem baixos salários e são mal-treinados, a metodologia e o currículo são inadaptados, o aluno chega à sala de aula sem nenhuma base educacional da família ou da pré-escola. Devido a mudanças legais, facilitaram-se as aposentadorias de professores, o que fez aumentar a despesa do ministério com inativos. O ministro Hingel chega a prever o que chama de "caos": "Se mantivermos esse ritmo, num prazo de 10 anos todas as verbas do ministério serão destinadas apenas aos aposentados".</a></a></a>
<a name="12">[12]</a> <a href="#12" id=12><a name="7">[7]</a> <a href="#7" id=7></body></a></a>
<a name="13">[13]</a> <a href="#13" id=13><a name="8">[8]</a> <a href="#8" id=8></html></a></a>
<a name="14">[14]</a> <a href="#14" id=14></body></a>
<a name="15">[15]</a> <a href="#15" id=15></html></a>
</body>
</html>