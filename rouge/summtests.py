# -*- coding: utf-8 -*-

import random
from resumeai import summarize_text, hacks
from resumeai.summarization import SummarizedText, Summarizer
# Aqui ficam os sumarizadores baseline


class RandomSummarizedText(SummarizedText):
    """
    Pega sentenças aletórias para criar o resumo
    """
    def _create_summary(self):
        if len(self.original_text) <= self.summary_min_size:
            return self.original_text

        sqtt = int(len(self.analised_text.sentences) * 0.3) or 1
        txtlen = len(self.analised_text.sentences)
        sentences = []
        while len(sentences) < sqtt:
            sentences.append(
                self.analised_text.sentences[random.choice(range(txtlen))])

        sentences = sorted(
            sentences, key=lambda s: self.analised_text.sentences.index(s))

        strsummary = ' '.join([hacks.remove_parens(s.raw_text)
                               for s in sentences])

        return strsummary


class RandomSummarizer(Summarizer):
    def summarize(self):
        summarized = RandomSummarizedText(self.text,
                                          self.summary_max_size,
                                          self.summary_min_size)
        return summarized


def random_summ(txt):
    s = RandomSummarizer(txt)
    return s.summarize().summary

def simple_summ(txt):
    raise NotImplementedError

def ok_summ(txt):
    return summarize_text(txt).summary
