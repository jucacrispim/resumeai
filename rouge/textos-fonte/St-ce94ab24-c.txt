Ainda que comece em 1922, com a Semana de Arte Moderna de SÃÂ£o Paulo, o segundo mÃÂ³dulo da "Bienal Brasil SÃÂ©culo 20" tem origem em 1917, ano em que Anita Malfatti (1896-1964) faz sua primeira exposiÃÂ§ÃÂ£o individual em SÃÂ£o Paulo.

A exposiÃÂ§ÃÂ£o provoca grande rebuliÃÂ§o. O escritor Monteiro Lobato faz uma crÃÂ­tica ÃÂ  exposiÃÂ§ÃÂ£o, chamada "ParanÃÂ³ia ou MistificaÃÂ§ÃÂ£o?", em que condena a adesÃÂ£o de Malfatti ao cubismo, futurismo "e outros ismos" que entÃÂ£o surgiam na Europa.

Com influÃÂªncia inicial do expressionismo alemÃÂ£o, Malfatti vai contra a tendÃÂªncia neoclÃÂ¡ssica da arte brasileira do inÃÂ­cio do sÃÂ©culo. Para o curador da FundaÃÂ§ÃÂ£o Bienal, Nelson Aguilar, Lobato ficou chocado com a economia de traÃÂ§os e a franqueza de cores de sua pintura.

Malfatti tambÃÂ©m volta sua tÃÂ©cnica moderna ao mundo rural brasileiro, usando motivos primitivistas em seu trabalho. A atitude ÃÂ© inÃÂ©dita.

Mas, segundo a curadora do mÃÂ³dulo, Annateresa Fabris, a modernidade que SÃÂ£o Paulo comeÃÂ§aria a adotar nÃÂ£o ÃÂ© a prescrita por Malfatti. Fabris dÃÂ¡ o exemplo de Victor Brecheret (1894-1955), cuja aceitaÃÂ§ÃÂ£o pelo meio intelectual foi muito maior.

Para Annateresa, Brecheret lanÃÂ§ava mÃÂ£o de referÃÂªncias do passado, em diÃÂ¡logo com a tradiÃÂ§ÃÂ£o da escultura, e isso lhe rendeu sucesso entre os crÃÂ­ticos paulistas.

A modernidade que lhes interessava seria, entÃÂ£o, mais de conteÃÂºdo que de forma, mais de ordem que de ÃÂ­mpeto destrutivo -o contrÃÂ¡rio do que pregava a maioria das vanguardas internacionais. "A estÃÂ©tica de Brecheret nÃÂ£o chocava o senso comum da ÃÂ©poca", diz Annateresa.

Outro exemplo desse comportamento ÃÂ© o temor que o escritor MÃÂ¡rio de Andrade manifestou ÃÂ  pintora Tarsila do Amaral (1890- 1973). "Ele temia que a temporada que Tarsila passaria em Paris fosse 'despersonalizar' sua linguagem artÃÂ­stica."

Ali se comeÃÂ§a a formar um "discurso nacional", diz Annateresa. O que crÃÂ­ticos como SÃÂ©rgio Milliet e MÃÂ¡rio admiravam em Tarsila era sua "brasilidade".

Para Annateresa, esse discurso impediu a avaliaÃÂ§ÃÂ£o de caracterÃÂ­sticas importantes nas obras de Anita, Tarsila, Di Cavalcanti (1897-1976), Vicente do RÃÂªgo Monteiro (1899-1970), Lasar Segall (1891-1957) e Oswaldo Goeldi (1895-1961).

Ao mesmo tempo, consagrou pintores apenas por mÃÂ©rito temÃÂ¡tico. O maior exemplo, segundo Fabris, ÃÂ© CÃÂ¢ndido Portinari (1903-1962). MÃÂ¡rio de Andrade o converte em protÃÂ³tipo do artista nacional.

JÃÂ¡ ÃÂ© a dÃÂ©cada de 30. Desse perÃÂ­odo em diante, julga Aguilar, a arte moderna brasileira "entra num figurativismo cada vez mais dogmÃÂ¡tico". Segall, por exemplo, depois da ousadia de "Paisagem Brasileira" (1925), volta ÃÂ s cores sombrias e formas conservadoras de seu expressionismo.

A consequÃÂªncia disso ÃÂ© exemplificada pela trajetÃÂ³ria da pintora portuguesa Vieira da Silva (1908-1991) na dÃÂ©cada de 40. Radicada em Paris desde os anos 20, ela vem para o Brasil em 1940 e, para ter repercussÃÂ£o aqui, praticamente abandona seu abstracionismo (leia definiÃÂ§ÃÂ£o abaixo).

O outro curador deste segundo mÃÂ³dulo, Tadeu Chiarelli, se dedicou a pensar nos artistas "marginalizados" por aquele discurso nacional -e nÃÂ£o sÃÂ³ por ele. "TambÃÂ©m houve os que foram postos ÃÂ  margem por nÃÂ£o ser de vanguarda", diz Chiarelli.

Ele encaixa nessa categoria, entre outros, Anita Malfatti, porque voltou ÃÂ  ordem, a repensar a tradiÃÂ§ÃÂ£o; FlÃÂ¡vio de Carvalho (1899-1973) e Ismael Nery (1900- 1934), cujas obras sÃÂ£o ousadas e conturbadas; e Maria Martins (1900-1978), por sua investigaÃÂ§ÃÂ£o do irracionalismo.

"Vamos mostrar uma visÃÂ£o nÃÂ£o-institucionalizada do modernismo", diz. "ÃÂ preciso rever as obras dos artistas independentemente dos grupos a que pertenceram."
