O capitalismo tem proporcionado oportunidades para pessoas que desejam entrar, de alguma forma, no mundo dos negÃÂ³cios. Cada vez mais jovens, executivos, profissionais liberais e trabalhadores se transformam em empreendedores movidos por seus sonhos e pelas oportunidades oferecidas pelo mercado.

Quanto mais aberto o mercado, mais oportunidades para que pessoas se arrisquem no mundo dos negÃÂ³cios. O livre mercado ÃÂ© a fonte natural de novas oportunidades para empreendedores. E, ao contrÃÂ¡rio do que alguns apregoam, cada vez mais existirÃÂ£o novas e notÃÂ¡veis alternativas de empreendimento, em funÃÂ§ÃÂ£o da entrada de novos atores no palco dos negÃÂ³cios.

Como o mercado possui suas prÃÂ³prias leis e se move com grande velocidade ÃÂ© preciso estar alerta. O mercado ÃÂ© bondoso e recompensa os empreendedores que, atravÃÂ©s de sua inteligÃÂªncia e trabalho, demonstrem "competÃÂªncia" para competir.

Por outro lado, o mercado ÃÂ© enÃÂ©rgico e nÃÂ£o hesita em penalizar aqueles que se estabeleceram, mas nÃÂ£o adquiriram ou trouxeram novas formas para se autoproteger da concorrÃÂªncia. Estes, que sÃÂ£o a maioria, acabaram rotulados pelo fracasso.

Com a velocidade do mercado, inÃÂºmeros produtos, serviÃÂ§os e negÃÂ³cios simplesmente desapareceram pela incapacidade de percepÃÂ§ÃÂ£o ou adaptaÃÂ§ÃÂ£o ÃÂ s novas expectativas de consumo.

Por outro lado, surgem novas e fantÃÂ¡sticas alternativas para produtos, serviÃÂ§os ou negÃÂ³cios que pela mudanÃÂ§a de hÃÂ¡bitos de nossa sociedade, proporcionam um terreno fÃÂ©rtil para novos empreendimentos.

NÃÂ£o basta apenas sonhar para que um novo empreendedor obtenha sucesso. SÃÂ£o inÃÂºmeras as variÃÂ¡veis para se garantir o sucesso de um empreendimento: capital, a nova idÃÂ©ia, o ponto, a experiÃÂªncia anterior, o nicho, o grau de inovaÃÂ§ÃÂ£o, o nÃÂ­vel de diferenciaÃÂ§ÃÂ£o, a forma de atendimento, o horÃÂ¡rio de funcionamento, o "timing" para entrada no negÃÂ³cio, o volume de trabalho despendido, a equipe e muito mais.

Sugiro a anÃÂ¡lise de um sÃÂ³ item. Em jÃÂ¡ havendo o sonho, a experiÃÂªncia ÃÂ© a principal peÃÂ§a para o acerto ou, pelo menos, para minimizar o insucesso. Muitas vezes, pela precariedade do sonho, que beira mais ÃÂ  fantasia, e pela ansiedade do novo empreendedor -somado ÃÂ  autoconfianÃÂ§a-, arrisca-se um novo negÃÂ³cio, sem qualquer experiÃÂªncia anterior. Movido, geralmente, pela errÃÂ´nea anÃÂ¡lise da "fruta do vizinho".

Se fosse possÃÂ­vel, o sonho teria que acontecer com os pÃÂ©s no chÃÂ£o. A partir daÃÂ­, entra-se no processo de dominar a ansiedade, que, na maioria das vezes, determina o insucesso do empreendimento pela incapacidade de uma anÃÂ¡lise mais detida sobre o assunto.

A autoconfianÃÂ§a, que ÃÂ© um fator altamente positivo, aliada ÃÂ  ansiedade pode provocar um desastre. Pode-se evitar tudo isso com a experiÃÂªncia, preferivelmente, na atividade a ser exercida. ExperiÃÂªncia ÃÂ© fundamental para minimizar desacertos.

SÃÂ³ existe uma coisa pior do que a falta de experiÃÂªncia: um empreendedor desprovido de conhecimento anterior bÃÂ¡sico para o novo negÃÂ³cio, mas com capital. InexperiÃÂªncia, somada a capital, nÃÂ£o ÃÂ© garantia de ÃÂªxito.

Se nÃÂ£o for possÃÂ­vel contar com a experiÃÂªncia, o convÃÂ­vio indireto com a nova atividade e um conhecimento mÃÂ­nimo do mundo dos negÃÂ³cios poderÃÂ£o proporcionar bons passos rumo ao sucesso.

O que deve ser lembrado ÃÂ© que cada caso ÃÂ© um caso e nÃÂ£o existe receita de sucesso. O que existe, sim, sÃÂ£o fatores que podem evitar o fracasso. 
