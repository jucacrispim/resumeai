Mulheres com maior poder de decisÃÂ£o se constituem no ponto chave para a resoluÃÂ§ÃÂ£o de problemas populacionais do mundo, afirma relatÃÂ³rio da ONU (OrganizaÃÂ§ÃÂ£o das NaÃÂ§ÃÂµes Unidas).

O relatÃÂ³rio de 66 pÃÂ¡ginas, intitulado "Estado da PopulaÃÂ§ÃÂ£o Mundial 1994", a ser lanÃÂ§ado amanhÃÂ£, informa tambÃÂ©m que cerca de 1,1 bilhÃÂ£o de pessoas no planeta tÃÂªm renda de apenas US$ 1 por dia.

O relatÃÂ³rio serÃÂ¡ discutido durante a ConferÃÂªncia Internacional sobre PopulaÃÂ§ÃÂ£o e Desenvolvimento, a ser realizada no Cairo, capital do Egito, em setembro.

Segundo o documento, que traz grÃÂ¡ficos e tabelas, entre 14 milhÃÂµes e 18 milhÃÂµes de adultos no mundo estÃÂ£o infectados com o vÃÂ­rus da Aids -quase o dobro de pessoas infectadas em 1990.

Segundo a ONU, no fim do sÃÂ©culo 20 estarÃÂ£o morrendo a cada ano, em decorrÃÂªncia da Aids, 1,8 milhÃÂµes de pessoas -nÃÂºmero igual ao total de mortos durante a primeira dÃÂ©cada da doenÃÂ§a.

LanÃÂ§ado anualmente pelo Fundo de PopulaÃÂ§ÃÂ£o da ONU, o documento aponta o maior poder de decisÃÂ£o das mulheres como fundamental para se chegar a taxas moderadas de crescimento populacional.

Para o relatÃÂ³rio, a diminuiÃÂ§ÃÂ£o deste crescimento implica menor pressÃÂ£o sobre o meio ambiente e outras ÃÂ¡reas problemÃÂ¡ticas.

Segundo a ONU, igualdades de direitos, para homens e mulheres, no acesso ÃÂ  saÃÂºde, planejamento familiar e educaÃÂ§ÃÂ£o sÃÂ£o importantes para a proteÃÂ§ÃÂ£o ambiental e o desenvolvimento econÃÂ´mico.

Por maior poder ÃÂ s mulheres, a ONU entende a igualdade de oportunidades de escolher quando casar, ter filhos, arrumar empregos e ter amplo acesso aos vÃÂ¡rios nÃÂ­veis de educaÃÂ§ÃÂ£o.

Um dos pontos recorrentes do relatÃÂ³rio da ONU ÃÂ© a necessidade de se ter taxas moderadas de crescimento populacional no mundo.

Uma menor fertilidade pode trazer impactos considerÃÂ¡veis na qualidade de vida, como por exemplo provendo mais educaÃÂ§ÃÂ£o, atendimento de saÃÂºde e oportunidades de empregos.

A populaÃÂ§ÃÂ£o mundial estÃÂ¡ em torno de 5,66 bilhÃÂµes de pessoas. Mesmo com o registrado decrÃÂ©scimo das taxas de fertilidade, ainda nascem por ano 94 milhÃÂµes de pessoas.

Segundo projeÃÂ§ÃÂµes da ONU, a populaÃÂ§ÃÂ£o mundial deverÃÂ¡ chegar a 8,5 bilhÃÂµes em 2025 e a 10 bilhÃÂµes, em 2050. Isto se as taxas de fertilidade continuarem declinando.

A ColÃÂ´mbia ÃÂ© citada como um dos exemplos de histÃÂ³rias de sucesso na diminuiÃÂ§ÃÂ£o da taxa de crescimento populacional. No inÃÂ­cio dos anos 60, sua taxa era de 3% ao ano. Hoje, estÃÂ¡ em 1,9%.

Em ZimbÃÂ¡bue, outro paÃÂ­s com experiÃÂªncia positiva no controle do crescimento populacional, o nÃÂºmero mÃÂ©dio de crianÃÂ§as por mulher, na dÃÂ©cada de 60, estava prÃÂ³ximo de oito. Caiu para sete em 1981 e atualmente ÃÂ© de 5,4.

Na TailÃÂ¢ndia, outro exemplo citado pelo relatÃÂ³rio, o nÃÂºmero mÃÂ©dio de crianÃÂ§as por mulher, caiu de seis, na dÃÂ©cada de 60, para 3,7 em 1980, chegando a 2,1 em 1991. As mulheres tailandesas estÃÂ£o entre as mais economicamente ativas da ÃÂsia.

A taxa de mulheres adultas alfabetizadas ÃÂ© quase tÃÂ£o alta quanto o ÃÂ­ndice registrado entre os homens -cerca de 96%. 

