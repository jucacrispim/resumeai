FranÃÂ§ois Mitterrand quer ser lembrado como o grande construtor da Europa. Alguns erros polÃÂ­ticos e revelaÃÂ§ÃÂµes sobre seu passado ameaÃÂ§am abalar essa imagem.

Sofrendo de cÃÂ¢ncer na prÃÂ³stata, Mitterrand vive dias difÃÂ­ceis no fim de seu mandato e de sua vida.

Sua ligaÃÂ§ÃÂ£o com a extrema direita na juventude, revelada este ano -em parte, por vontade do prÃÂ³prio presidente, que quer acertar contas com seu passado-, chocou os franceses.

Mitterrand definiu suas posiÃÂ§ÃÂµes do passado como erros da juventude. O fato ÃÂ© que, apÃÂ³s a guerra, aos poucos ele se impÃÂ´s como lÃÂ­der da esquerda e maior adversÃÂ¡rio do general Charles de Gaulle.

FaÃÂ§anha 

Em 1965, aos 49 anos, ele alcanÃÂ§ou a faÃÂ§anha de levar De Gaulle ao segundo turno da eleiÃÂ§ÃÂ£o presidencial.

Quatro anos depois, os socialistas preferiram escolher Gaston Defferre como candidato e naufragaram, com apenas 5% dos votos.

Mitterrand retomou as rÃÂ©deas do partido em 1971, no congresso de Epinay. No ano seguinte, assinou com o Partido Comunista o programa comum da esquerda.

A alianÃÂ§a durou cinco anos e sÃÂ³ beneficiou os socialistas, que roubaram boa parte do eleitorado cativo dos comunistas.

Foi com a ajuda desses votos que, finalmente, Mitterrand alcanÃÂ§ou seu objetivo, derrotando por pouco ValÃÂ©ry Giscard d'Estaing na eleiÃÂ§ÃÂ£o presidencial de 1981.

Em 1988, foi reeleito facilmente. ApÃÂ³s dois anos de coabitaÃÂ§ÃÂ£o com Jacques Chirac, um premiÃÂª de direita, Mitterrand bateu o prÃÂ³prio Chirac no segundo turno.

O balanÃÂ§o de seus dois mandatos ÃÂ© polÃÂªmico. Para uns, foi um perÃÂ­odo de paz em que a FranÃÂ§a enriqueceu; para outros, a maioria dos compromissos de campanha foi esquecida. Mesmo ÃÂ  esquerda, muitos o vÃÂªem como um homem obcecado pelo poder e impiedoso.

VacilaÃÂ§ÃÂµes 

Na polÃÂ­tica estrangeira, algumas vacilaÃÂ§ÃÂµes marcaram os ÃÂºltimos anos do seu governo.

O presidente nÃÂ£o percebeu a tempo a queda do comunismo: nÃÂ£o previu a queda do Muro de Berlim em 1989, e chegou a flertar com os golpistas de Moscou em 1991.

Apesar de criticado por seus adversÃÂ¡rios, devido ÃÂ s contradiÃÂ§ÃÂµes que marcaram sua carreira, Mitterrand se manteve coerente em pelo menos um ponto: a defesa da UniÃÂ£o EuropÃÂ©ia.

JÃÂ¡ em 1951, durante um congresso socialista, o futuro presidente dizia que  nada ÃÂ© possÃÂ­vel, muito menos a paz, se a FranÃÂ§a nÃÂ£o for o agente da Europa.

Treze anos depois, Mitterrand escreveu:  Creio que a Europa corresponde ÃÂ  vontade da histÃÂ³ria. Em 1973, ameaÃÂ§ou renunciar ÃÂ  lideranÃÂ§a do partido, dividido entre prÃÂ³ e antieuropeus.

Em 1992, o presidente reviveu seus grandes momentos de campanhas do passado ao se engajar na luta pela aprovaÃÂ§ÃÂ£o em plebiscito do tratado de Maastricht, que prevÃÂª a moeda ÃÂºnica na Europa.

Mitterrand aceitou participar de um debate na televisÃÂ£o contra Philippe SÃÂ©guin, deputado conservador que se opunha ao tratado.

Apesar da diferenÃÂ§a de idade (75 anos contra 49, ÃÂ  ÃÂ©poca), Mitterrand se mostrou jovial e foi considerado vencedor. O tratado foi aprovado por pequena margem.

Mas hÃÂ¡ setores que se opÃÂµem ÃÂ  uniÃÂ£o sem fronteiras. A FranÃÂ§a foi a principal responsÃÂ¡vel pelo adiamento da livre circulaÃÂ§ÃÂ£o de pessoas no interior da Comunidade, prevista para janeiro de 95.

O motivo alegado foram dificuldades para implantar o banco de dados de todas as polÃÂ­cias europÃÂ©ias, em Estrasburgo. Os franceses receiam o trÃÂ¡fico de drogas e a imigraÃÂ§ÃÂ£o clandestina.

Por fim, os agricultores sÃÂ£o a classe social que mais se opÃÂµe ÃÂ  UE. Eles se queixam do fim de vÃÂ¡rios subsÃÂ­dios, extintos por Bruxelas. ÃÂ comum encontrar espantalhos com cartazes de protesto nas estradas do interior francÃÂªs.

