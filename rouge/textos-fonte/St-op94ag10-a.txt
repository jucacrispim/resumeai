Um dos aspectos positivos da atual polÃÂ­tica cambial reside no fato de ela nÃÂ£o pretender envolver-se na defesa de uma taxa de cÃÂ¢mbio real potencialmente insustentÃÂ¡vel a longo prazo.

O ministro da Fazenda tem dito, com frequÃÂªncia, que o compromisso com o "cÃÂ¢mbio estÃÂ¡vel" vai atÃÂ© dezembro. NÃÂ£o hÃÂ¡ qualquer obrigaÃÂ§ÃÂ£o de intervenÃÂ§ÃÂ£o no mercado de cÃÂ¢mbio, a nÃÂ£o ser que a cotaÃÂ§ÃÂ£o do dÃÂ³lar supere um real.

Essas informaÃÂ§ÃÂµes sÃÂ£o importantes porque o nosso comÃÂ©rcio exterior estÃÂ¡ longe de apresentar resultados satisfatÃÂ³rios. Entre 1988 e 1993, com um Produto Interno Bruto revelando um crescimento mÃÂ©dio da ordem de 0,8% e uma ampla capacidade ociosa no setor industrial, nossas exportaÃÂ§ÃÂµes cresceram a uma taxa de apenas 2,8% ao ano, enquanto as exportaÃÂ§ÃÂµes mundiais cresciam a 6,2% ao ano. Nossa participaÃÂ§ÃÂ£o nas exportaÃÂ§ÃÂµes mundiais caÃÂ­ram de 1,2% para 1% no perÃÂ­odo.

Por outro lado, as importaÃÂ§ÃÂµes, graÃÂ§as ÃÂ  desburocratizaÃÂ§ÃÂ£o, ÃÂ  eliminaÃÂ§ÃÂ£o de barreiras nÃÂ£o-tarifÃÂ¡rias e ÃÂ  dramÃÂ¡tica reduÃÂ§ÃÂ£o tarifÃÂ¡ria, cresceram ÃÂ  taxa mÃÂ©dia anual de 12%. Nossa participaÃÂ§ÃÂ£o nas importaÃÂ§ÃÂµes mundiais passaram de 0,6% para 0,8%, forÃÂ§ando um amplo aumento da produtividade e da qualidade da produÃÂ§ÃÂ£o nacional para enfrentar a competiÃÂ§ÃÂ£o externa.

O saldo exagerado da balanÃÂ§a comercial foi reduzido de 19,2 bilhÃÂµes de dÃÂ³lares, em 1988, para 13,1 bilhÃÂµes, em 1993. A situaÃÂ§ÃÂ£o do balanÃÂ§o de transaÃÂ§ÃÂµes correntes nÃÂ£o ÃÂ©, entretanto, tÃÂ£o brilhante como se quer fazer crer.

Nos ÃÂºltimos seis anos (1988-1993) tivemos um saldo positivo em transaÃÂ§ÃÂµes correntes da ordem de 6,7 bilhÃÂµes de dÃÂ³lares, praticamente construÃÂ­do pelas remessas unilaterais de brasileiros que estÃÂ£o trabalhando no exterior e que atingiram a 6,4 bilhÃÂµes de dÃÂ³lares no perÃÂ­odo. Sem essas remessas, o Brasil teria registrado equilÃÂ­brio nas transaÃÂ§ÃÂµes correntes ao longo dos ÃÂºltimos seis anos.

A fantÃÂ¡stica acumulaÃÂ§ÃÂ£o de reservas internacionais (no conceito de caixa) nos ÃÂºltimos 29 meses (janeiro de 1992 a maio de 1994) da ordem de 30 bilhÃÂµes de dÃÂ³lares deveu-se praticamente ao movimento de capitais estimulado pela maior taxa de juro real de que se tem notÃÂ­cia no mundo civilizado.

Ela foi financiada por emissÃÂµes de papel-moeda e pelo crescimento da dÃÂ­vida interna, num movimento de auto-reforÃÂ§o: juro real estratosfÃÂ©rico atrai capital externo, o que aumenta a dÃÂ­vida interna e exige juro real ainda mais alto para induzir os agentes a retÃÂª-la em suas mÃÂ£os. Tudo isso ÃÂ  custa da manutenÃÂ§ÃÂ£o de ampla capacidade ociosa e desemprego para milhÃÂµes de brasileiros...

O que acontecerÃÂ¡ quando os juros tiverem que diminuir? Provavelmente teremos no mercado de bens e serviÃÂ§os um aumento do nÃÂ­vel de atividade (o que ÃÂ© desejÃÂ¡vel), que retirarÃÂ¡ um dos estÃÂ­mulos ÃÂ s exportaÃÂ§ÃÂµes e elevarÃÂ¡ o nÃÂ­vel das importaÃÂ§ÃÂµes (o que tambÃÂ©m ÃÂ© saudÃÂ¡vel). E no mercado financeiro uma reduÃÂ§ÃÂ£o do fluxo de entrada.

SÃÂ³ entÃÂ£o saberemos se manipular a taxa de cÃÂ¢mbio nominal pelo diferencial de juros interno e externo e esperar que a taxa de cÃÂ¢mbio real se ajuste pela reduÃÂ§ÃÂ£o dos preÃÂ§os internos foi uma aposta sustentÃÂ¡vel. Mas nÃÂ£o serÃÂ¡ a hipÃÂ³tese implÃÂ­cita nesse modelo (flexibilidade dos preÃÂ§os para baixo) um pouco extravagante?
