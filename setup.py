# -*- coding: utf-8 -*-

import os
from setuptools import setup, find_packages
from setuptools.command.install import install
import zipfile


VERSION = '0.7.3.6'
DESCRIPTION = """
Sumarizador simples
"""
LONG_DESCRIPTION = DESCRIPTION

class ResumeAiInstall(install):

    def run(self, *args, **kwargs):
        install.run(self, *args, **kwargs)
        self.download_data()

    def download_data(self):
        import nltk
        downloader = nltk.downloader.Downloader()
        corpora_dir = os.path.join(downloader.download_dir, 'corpora')
        stopwords_dir = os.path.join(corpora_dir, 'stopwords')
        tokenizers_dir = os.path.join(downloader.download_dir, 'tokenizers')
        punkt_dir = os.path.join(tokenizers_dir, 'punkt')

        if not os.path.exists(stopwords_dir):
            downloader.download('stopwords')

        if not os.path.exists(punkt_dir):
            downloader.download('punkt')


setup(name='resumeai',
      version=VERSION,
      author='Juca Crispim',
      author_email='juca@poraodojuca.net',
      description=DESCRIPTION,
      long_description=LONG_DESCRIPTION,
      url='https://gitorious.org/pyrocumulus',
      # packages=find_packages(exclude=['tests', 'tests.*', 'settings',
      #                                 'settings.*']),
      packages=['resumeai'],
      install_requires=['nltk>=3.0.0', 'numpy>=1.9.0'],
      include_package_data=True,

      classifiers=[
          'Development Status :: 3 - Alpha',
          'Intended Audience :: Developers',
          'License :: OSI Approved :: GNU General Public License (GPL)',
          'Operating System :: OS Independent',
          'Programming Language :: Python :: 3',
          'Programming Language :: Python :: 3.2',
          'Programming Language :: Python :: 3.3',
          'Programming Language :: Python :: 3.4',
          'Topic :: Software Development :: Libraries :: Python Modules',
      ],
      test_suite='tests',
      cmdclass={'install': ResumeAiInstall}
  )
