# -*- coding: utf-8 -*-

import os
import sys
import pickle
from unicodedata import normalize
from test_data.txts import txts
from test_data.txts_semantria import txts as semantria_txts
import resumeai
from resumeai import summarize_text
import webapp
#from resumeai.summarization import strategies


def printinfo(title, content, summary, entities, sname=''):
    print(title)
    print('=' * len(title), '\n')
    print('Estratégia: %s' % sname)

    print("Tamanho do texto original: ", len(content))
    print("Tamanho do resumo: ", len(summary))
    print('enitdades: %s' % entities)
    print('\n')
    print(summary)
    print('\n\n')

def get_summary(content, sname, sindex):
    if sname == 'semantria':
        try:
            summary = semantria_txts[sindex]
        except IndexError:
            summary = ''
        entities = ''
    else:
        summary, entities =  summarize_text(content, sname)

    return summary, entities


def add2notentities(word):
    notentities_file = os.path.join(resumeai.DATA_DIR, 'notentities.pickle')
    with open(notentities_file, 'rb') as f:
        notentities = pickle.loads(f.read())
    print('len original: %s' % len(notentities))
    notentities.add(word.lower())
    with open(notentities_file, 'wb') as f:
        f.write(pickle.dumps(notentities))
    print('len final: %s' % len(notentities))


def add2nottrends(entity):
    nottrends_file = os.path.join(webapp.DATA_DIR, 'nottrends.pickle')
    with open(nottrends_file, 'rb') as f:
        nottrends = pickle.loads(f.read())

    print('len original: %s' % len(nottrends))
    nottrends.add(entity)

    with open(nottrends_file, 'wb') as f:
        f.write(pickle.dumps(nottrends))

    print('len final: %s' % len(nottrends))


def rmnotentities(word):
    notentities_file = os.path.join(resumeai.DATA_DIR, 'notentities.pickle')
    with open(notentities_file, 'rb') as f:
        notentities = pickle.loads(f.read())

    print('len original: %s' % len(notentities))
    notentities.discard(word)
    print('len final: %s' % len(notentities))

def innotentities(word):
    notentities_file = os.path.join(resumeai.DATA_DIR, 'notentities.pickle')
    with open(notentities_file, 'rb') as f:
        notentities = pickle.loads(f.read())

    return word in notentities


def write2file(title, otext, *summaries, entities=[], basedir='.'):
    txt = '<html>\n<head>resumeai</head>\n<body>'
    txt += "<h2>%s</h2><br/>" % (title)
    txt += 'Tamanho original: %s<br/><br><pre>' % len(otext)
    txt += '%s</pre><br/><br/>' % otext
    for summary in summaries:
        txt += "<h3>%s</h3><br/>" % (title)
        txt += 'Tamanho do resumo: %s<br/><br/>' % len(summary)
        txt += '<pre>%s</pre><br/><br/>' % summary
    txt += '</body></html>'
    fname = title.lower().replace(' ', '-').replace('!', '').replace('.', '')
    fname = fname.replace('?' ,'').replace('%', '')
    fname += '.html'
    fname = fname.strip()
    fname = normalize("NFKD", fname).encode('ascii', 'ignore')

    with open(fname, 'w') as f:
        f.write(txt)
        f.close()


if __name__ == '__main__':

    sname = 'eager'
    txt = 'all'
    compare = []
    wait = False
    w2f = False
    for a in sys.argv:
        if '--strategy' in a:
            sname = a.split('=')[1]
        elif '--text' in a:
            txt = a.split('=')[1]
        elif '--compare' in a:
            compare = a.split('=')[1].split(',')
        elif '--wait' in a:
            wait = True
        elif '--w2f' in a:
            w2f = True

    if not compare:
        compare = [sname]
    print('Total de textos: %s' % len(txts))
    if txt == 'all':
        for i, t in enumerate(txts):
            content = t['corpo']
            summaries = []
            for sname in compare:
                summary, entities = get_summary(content, sname, i)
                if w2f:
                    summaries.append(summary)
                else:
                    printinfo(t['titulo'], content, summary, entities, sname=sname)

            if wait:
                print('comandos:\n1) add2notentities')
                cmd = input('esperando... ')
                if cmd == '1':
                    words = input('add2notentities: ')
                    words = words.split(',')
                    for w in words:
                        add2notentities(w.strip())
            if w2f:
                write2file(t['titulo'], content, *summaries)
    else:
        t = txts[int(txt)]
        summaries = []
        for sn in compare:
            content = t['corpo']
            summary, entities = get_summary(content, sn, int(txt))
            if w2f:
                summaries.append(summary)
            else:
                printinfo(t['titulo'], content, summary, entities, sname=sname)

            if w2f:
                write2file(t['titulo'], content, *summaries)
