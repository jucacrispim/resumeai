# -*- coding: utf-8 -*-

txts = [
    # txt1
    {'titulo': """Um país de analfabetos científicos""",
     'corpo':"""
A maioria da população brasileira não domina a linguagem científica necessária para lidar com situações cotidianas, tais como ler resultados de exames de sangue, calcular se o tanque tem gasolina suficiente para uma viagem, relacionar e entender o impacto de ações no meio ambiente ou entender a cobrança da conta de luz.
Essa é a conclusão da primeira pesquisa nacional que mede o índice de letramento científico (ILC) do brasileiro, feita pelo Instituto Abramundo, em parceria com o Instituto Paulo Montenegro, do Grupo IBOPE, e a ONG Ação Educativa.
Quase 65% da população metropolitana entre 14 e 50 anos, com mais de quatro anos de estudos, têm um ILC, no máximo, rudimentar. Pouco menos de um terço (31%) consegue entender textos com um grau um pouco maior de dificuldade, como interpretar a tabela de nutrientes em rótulos de produtos e especificações técnicas de produtos eletroeletrônicos. A maioria absoluta, 79%, além de não conseguir entender os termos científicos que lê, é incapaz de aplicar isso em situações cotidianas, como ler um manual de instrução para usar um aparelho doméstico.
O Brasil que não sabe ler e fazer ciência
A pesquisa do ILC convidou os entrevistados a resolverem situações do cotidiano. Ele avalia o domínio da linguagem científica, como o conhecimento científico é colocado em prática no cotidiano e como tais conhecimentos pautam a visão de mundo dessas pessoas.

"Nós já esperávamos um resultado ruim, mas o que veio foi péssimo”, afirma Ricardo Uzal, presidente do Abramundo. “Nós sabemos o quanto a ausência do domínio científico impede o exercício da cidadania. Quem tem esse domínio se coloca de forma diferente diante de problemas do dia a dia, ele sabe questionar, propor soluções, testar alternativas”. Uzal diz ainda que a pesquisa mostra que faltam políticas públicas adequadas para melhorar o ensino de ciências nas escolas. Os resultados da pesquisa da Abramundo evidenciam ainda a falta de habilidade matemática aplicada no dia a dia. “A matemática serve como base para todas as outras ciências”, afirma Uzal.
Entre os que fazem ou fizeram curso superior, apenas 11% podem ser considerados proficientes. Há uma parcela significativa, de 37%, que não passa do nível rudimentar. Entre os que estudaram até o ensino médio, a situação é ainda mais crítica: apenas 1% é proficiente e mais da metade (52%), tem domínio rudimentar.
Na mais prestigiada avaliação internacional de alunos, o PISA, feito pela Organização para a Cooperação e Desenvolvimento Econômico (OCDE), ciências é a disciplina em que os alunos brasileiros estão mais defasados.

Na última prova, de 2013, o Brasil ficou 59º lugar, entre 65 países. Não houve avanço em relação ao desempenho de 2009. Para os organizadores da pesquisa do ILC, o resultado mostra a urgência de se criar políticas públicas de educação, para melhorar a eficiência do ensino da disciplina no ensino fundamental e médio. Em uma outra prova, o Pisa avaliou a capacidade dos alunos de resolver problemas de lógica. O Brasil se deu mal novamente, ficou em 38º lugar, entre 44 países.

Educação sem ciência
Mesmo entre os que fazem ou fizeram um curso superior, o domínio da linguagem e de conceitos científicos, bem como as habilidades de aplicar isso no dia a dia, é baixo.

Mesmo entre as pessoas mais proficientes, são poucos os que se sentem à vontade na hora de realizar pequenas tarefas ou ações que envolvem conhecimento científico. Combater um pequeno incêndio seguindo as instruções escritas nos equipamentos contra fogo, por exemplo. 27%   da turma mais proficiente, a do Nível 4, declarou que teria dificuldade ou não seriam capazes de fazer isso. A mesma proporção apareceu entre os de Nível 3. Ler manuais para instalar aparelhos domésticos é outro desafio para 18% dos mais proficientes, 20% dos de nível básico e 27% para os de letramento rudimentar.
Pequenos desafios cotidianos
Mesmo entre os mais proficientes, algumas atividades que envolvem conhecer e aplicar conceitos científicos são complicadas. (Proporção de pessoas que declararam que teriam dificuldades ou não seriam capazes de fazer determinadas atividades, por nível de proficiência)
 	 Nível 1	 Nível 2	 Nível 3	 Nível 4
Compreender as contraindicações de um remédio, lendo a bula	 37%	 26%	 16%	 11%
Conferir a conta de luz	 40%	 28%	 18%	 8%
Ler manuais de instalação de aparelhos domésticos	 39%	 27%	 20%	 18%
Combater um pequeno incêndio seguindo instruções escritas no equipamento contra fogo	 45%	 40%	 27%	 27%
Consultar dados sobre saúde e remédios na internet	 58%	 41%	 24%	 16%
Leia abaixo uma questão para cada um dos níveis e confira em qual deles você se enquadraria (as respostas estão mais abaixo):
Nível 1
Melco Aspirina 500
Indicações: Dor de cabeça, dores musculares, dor reumática, , dor de dente, dor de ouvido. Alivia os sintomas da gripe comum.
Dose oral: 1 a 2 comprimidos de 6 em 6 horas, de preferência após as refeições, durante 7 dias no máximo.
Precauções: Não use para gastrite ou úlcera p´ptica. Não use se estiver tomando medicamentos anticoagulantes, ou se tiver sangramentos frequentes.
Ingredientes: Cada comprimedo contém: 500 mg de ácido acetilsalicílico. Excipiente: C.B.P 1 comprimido.
Questão: Por quanto tempo, no máximo, você pode tomar esse remédio?

Nível 2
Leia o texto abaixo:
CHUVA, MENOR ATRITO DOS PNEUS
Ao dirigir na chuva, tenha em mente: os freios param as rodas, mas são os pneus que param o carro. A mesma advertência vale para o caso de dirigir na lama, sobre a areia, com óleo na pista ou em outras circunstâncias que alterem as condições de atrito.
Pneus desgastados, sem estrias, na chuva, aumentam a probabilidade  de perda de aderência e consequente controle do veículo, pois a água não escoará e o pneu deslizará sobre ela.
Questão: O que faz com que o pneu com estrias aumente a segurança quando a pista está molhada?

Nível 3
Os gráficos a seguir mostram a evolução de populações de bactérias ao longo do tempo em duas pessoas infectadas com a mesma bactéria. Nos dois casos, os doentes tomaram antibióticos.
Nivel 3 ILC (Foto: Reprodução)
Questão: Formule hipóteses sobre o que pode ter ocorrido para justificar a diferença nos gráficos dos dois casos.
Nível 4
A Organização das Nações Unidas (ONU) produz estudos que permitem fazer projeções sobre a concentração de dióxido de carbono na atmosfera e o aumento da temperatura global. É com base nesses estudos que foi produzido o gráfico a seguir:
Nível ILC (Foto: Reprodução)

Questão: Por que o gráfico apresenta dois traçados, um para o "cenário otimista" e outro para o "cenário pessimista".





Exemplos de respostas corretas
Nível 1
Por, no máximo, 7 dias.
Nível 2
O pneu com estrias facilita o escoamento da água, diminuindo a perda de atrito, a perda de aderência.
Nível 3
O segundo paciente (caso B) pode ter interrompido o tratamento, quando os sintomas diminuíram, fazendo voltar assim a infecção. Ou as bactérias desenvolveram resistência/mutação/evolução. Ou o remédio não matou todas as bactérias. Não tomou o remédio conforme indicava a bula ou o médico.
Nível 4
Deve mencionar termos que tenham o sentido de “depende”, “probabilidade” ou “possibilidade” / São duas possibilidades diferentes, conforme o comportamento humano e da atmosfera / Depende das emissões de carbono. / Não se sabe exatamente o acontecerá, os gráficos lidam com probabilidades/projeções/estimativa.
"""},
    # txt2
    {'titulo': 'A internet afeta as relações entre jovens',
     'corpo': """
Uma nova pesquisa da Universidade da Califórnia, em Los Angeles, mostra que a nova geração superconectada de crianças pode ter perdido uma habilidade importante: a identificação das emoções transmitidas pelo olhar e pela expressão facial.

O estudo comparou crianças de 11 e 12 anos que passam a semana num internato (sem acesso diário à internet) a crianças que passam, em média, cinco horas por dia conectadas em seus celulares. Resultado: os internos conseguem identificar emoções em testes com fotos e trechos de filme de maneira muito mais precisa que aqueles que vivem on-line.

A troca constante de mensagens em que sentimentos são descritos por meio de letras ou palavras (por exemplo, “kkk” para rir) pode fazer esses jovens perder a capacidade de ler emoções básicas, obtidas de forma não verbal, por meio do olhar.

No Brasil, uma pesquisa feita pelo Portal Educacional avaliou o comportamento na internet de mais de 4 mil jovens de 13 a 17 anos, de escolas particulares de 14 Estados brasileiros. A conclusão é que esses jovens estão cada vez mais conectados e que o uso da internet afeta a forma como eles se relacionam. Quase 95% deles acessam a internet todos os dias (ou quase todos os dias), 83% dizem acessar a internet sempre ou quase sempre de um tablet ou de um celular, e mais de 90% começaram a usar redes sociais com 12 anos ou menos.

De acordo com a pesquisa, 22% dos jovens já ficaram com alguém que conheceram pela internet. Os que já namoraram são 11%, e 5% já fizeram sexo. Mais de quatro em cada dez jovens disseram que mesmo quem namora pode dar uma paquerada na rede.

Os pais estão longe de entender esse fenômeno. Cerca de 80% dos 300 que participaram da pesquisa dizem ter problemas com os filhos, em função do tempo de conexão. Os filhos afirmam que 80% dos pais não controlam o acesso à internet pelo celular e, dos 20% que controlam, metade só faz isso em função da conta telefônica. Pouco mais de 1% dos pais sabem que seus filhos já ficaram ou namoraram alguém que conheceram pela rede.

Entre os 60 professores que responderam a pesquisa, mais de 73% já detectaram problemas de relacionamento entre os alunos em sala de aula, por causa de conteúdos postados nas redes sociais. Quase 64% percebem casos de bullying pela internet entre os alunos.

Pais e professores dizem que o uso da internet pelos jovens é uma preocupação maior que outras questões clássicas, como drogas ou sexualidade. A pesquisa americana e a brasileira mostram que essa geração tem padrões de uso da rede que marcam uma forma nova e particular de interpretar emoções, comportamentos e até relacionamentos. Por isso, essas são questões que deveriam ser trabalhadas tanto na sala de aula quanto em casa.
        """},
    # txt3
    {'titulo': """TESTAMOS O AUDI RS Q3""",
             'corpo': """
Após pegar um trânsito intenso, deixei escapar um sorriso de canto de boca quando vi que uma avenida completamente livre nos aguardava. Me ajeitei melhor no banco, empunhei o volante, sinal verde, pé direito fundo e lá fomos nós. E o sorrisinho virou sorrisão: que carro! Não podia esperar nada menos. Afinal, estava no comando da versão mais potente do Q3, o Audi RS Q3. À venda no Brasil desde junho por R$ 273.600, o modelo é a porta de entrada para a família RS, divisão esportiva da montadora alemã.

O porte de veículo familiar engana, mas se trata de um SUV singular. O Audi é alimentado por um motor 2.5 turbo, a gasolina, capaz de entregar 310 cv, disponíveis entre 5.200 e 6.700 giros. O torque é expressivo: 42,8 kgfm a 1.500 rpm, ou na primeira pisada no acelerador, se preferir. A fabricante informa que o RS Q3 alcança os 100 km/h em 5,2 segundos.

Em nossos testes, ele cravou 4,8 s, fazendo uso do sistema de largada (outra evidência da esportividade do modelo). Um de seus principais concorrentes, o Land Rover Evoque topo de linha pode até ser mais bonito, mas fica para trás quando o assunto é desempenho. No teste de frenagem a 100 km/h, por exemplo, o Audi leva vantagem em dois metros, percorrendo exatos 40 m. A retomada de 40 km/h a 80 km/h é de 2,4 s, 2 s mais rápida que a do oponente inglês.

Ainda que, caso tivesse grana, preferisse investir em um esportivo efetivamente com cara de esportivo, a veRSatilidade do RS Q3 para o dia a dia agrada e é um de seus pontos fortes. Com 2,60 metros de entre-eixos, acomoda bem a família inteira. O porta-malas, no entanto, não é lá grande coisa para um SUV: 356 litros.

Dirigindo com uma pegada mais mansa, o Audi entrega médias de consumo elogiáveis: 8,5 km/l na cidade e 11,4 km/l na estrada. O comportamento é praticamente irretocável, bem como as respostas do câmbio automatizado de sete marchas e dupla embreagem, com opção de trocas sequenciais por aletas atrás do volante de base reta. O sistema Audi Drive Select ainda permite que o condutor acerte a dinâmica de condução de sua preferência, adaptando itens como amortecedores e direção.

Mas no meio dessa pimenta toda, a lista de equipamentos não me agradou tanto. Fiquei surpresa ao notar que o modelo não tinha câmera para auxílio de estacionamento – há apenas sensores dianteiros e traseiros. Senti falta também do monitoramento de ponto cego, assim como outro detector de distância que avisa, com alerta sonoro e visual, quando você se aproxima em velocidade alta do veículo à frente. Em mimos, o Audi é escolado. A central multimídia traz navegador GPS integrado (com áudio em português de Portugal) e comandos de voz. A cereja do bolo é o sistema de som de alta qualidade da marca Bose, que só perde mesmo para o belíssimo e inconfundível ronco do propulsor de cinco cilindros do RS Q3.
        """},
    #txt4
    {'titulo': """AVALIAÇÃO: VW FOX RENOVADO CHEGA AO MERCADO POR R$ 35.900""",
     'corpo': """
Embora seja um animal esperto, a raposa ganha uma aura ainda mais sagaz nos livros de ficção. Seus movimentos deixam caçadores perplexos e seu discurso (afinal, são contos) sempre tentam levar alguém no papo. O Volkswagen Fox fez valer o seu batismo e apelou para mais do que alguns truques para manter a posição de segundo carro mais vendido da marca. Para começar, ganhou uma tapa mais forte do que o último de 2009. Mudam a dianteira e a traseira, inspiradas no queridinho Golf 7. Além disso, adotou o EA211 1.6 16V de 120 cv e os controles eletrônicos de tração e de estabilidade da Saveiro Cross, e conservou uma cartada só para si: o câmbio manual de seis marchas.

Somado a isso, os preços não aumentaram muito. São movimentos que, além de deixá-lo bem na caçada, também darão fôlego até a nova geração em 2016. O novo Fox usará a plataforma MQB do Golf, arquitetura que ainda vai gerar no Brasil um crossover e um sedã, revelados por Autoesporte. Talvez por isso esteja ficando cada vez mais parecido com o médio. Até para substituir o Polo, o que deixou a VW sem um compacto premium.

Olhe bem para os faróis: estão lá dois blocos recortados na parte superior pelo capô. Os contornos cromados se fazem de led. Com altura de pivô de basquete, o Fox nunca foi um exemplo de equilíbrio estético. Agora os para-choques, cava das rodas e lanternas horizontalizadas colaboraram para assentar o desenho no chão. Há outros toques, como o logotipo pivotante da tampa do porta-malas, que se faz de maçaneta. Por dentro, o console tem novas saídas de ar e o quadro de instrumentos nova moldura superior, além de novos revestimentos. Quem quiser os  aventureiro CrossFox, terá que esperar pelo menos até o Salão de São Paulo, enquanto a perua Spacefox de cara nova chega no início de 2015.

É justamente a reforma que une todas as versões. Em outros pontos, a revolução foi apenas pelo topo. Se você quiser o novo 1.6, terá que partir para o Highline de R$ 48.490 – R$ 380 de aumento. O top é a única que oferece opcionalmente os controles eletrônicos de estabilidade e de tração com hill-holder, afora controle de cruzeiro, volante multifuncional do Golf, rodas aro 16 e faróis auxiliares que viram para iluminar esquinas nas conversões.
O câmbio MQ200-6F é baseado no MQ200 de cinco marchas. Em precisão, não deve nada ao MQ250 do Golf, já que ganhou novos sincronizadores. As primeiras cinco marchas têm as mesmas relações. Não é loucura, o diferencial foi encurtado para compensar e a velocidade máxima é em quinta marcha, relegando a sexta a ritmos de cruzeiro.
Volkswagen Fox 2015 (Foto: Fabio Aro)
VOLKSWAGEN FOX 2015 (FOTO: FABIO ARO)
Como funciona na prática? O teste de lançamento foi relegado à pista da Goodyear com velocidade máxima limitada. Pior que isso, no trecho em que os jornalistas estavam liberados para andar mais rápido, apenas o Fox Highline 1.6 I-Motion estava disponível. Deu para ver que o câmbio automatizado de cinco marchas está bem parelhado ao novo motor e que os trancos diminuíram. Durante as fotos, deu para esticar um pouco e ver que, mesmo em sexta, o bom torque do motor mantém o pique sem reduções acima dos 2 mil giros. O giro a 120 km/h em sexta marcha ficou em 2.800 rotações, poderia ser mais baixo.
                                                         LINHA DO TEMPO

CONHEÇA A TRAJETÓRIA DO FOX
Segundo a Volkswagen, o Fox Highline vai aos 100 km/h em 9,8 s e chega aos 189 km/h. Na nossa pista de testes, foram 10,3 s na mesma prova, contra 11,5 s marcados pela versão anterior equipada com motor 1.6 8V de 104 cv. O motor multiválvulas aposta nos comandos variáveis para entregar um bom torque a médias rotações, o que ajuda nas retomadas. Foram 10,1 s na prova de 60 a 100 km/h em quarta marcha, 0,3 s mais rápido que o antigo. Não foi só em números frios que o Fox evoluiu. A direção elétrica (antes só o Bluemotion possuía) manteve a rapidez e calibração do mecanismo hidráulico. Na única reta, cones forçavam chicanes. Ali deu para ver, ainda que de passagem, que a calibração da suspensão continua a fazer o máximo possível para fazer o espigado Fox adernar menos nas mudanças de trajetórias e curvas. O novo conjunto de rodas aro 16 e pneus 195/55 confere uma boa aderência. Mesmo inclinando bem mais que o Gol ou Polo, o Fox é previsível e seguro.
COMPACTO VERSÃO 2.0

O pacote de salvaguardas dinâmicas veio junto com a nova arquitetura eletrônica, que estreou no Gol reestilizado, chamado pela marca de G6. Além do ESP e controle de tração, veio o bloqueio eletrônico do diferencial atua como o da Saveiro Cross a até 80 km/h, que aciona o freio da roda com menor tração e passa a força para outra. Completam as secretárias eletrônicas o hill-holder, além do assistente de frenagem que aumenta a pressão no circuito hidráulico – conjugado a discos dianteiros maiores. São opcionais, contudo. De série, o Fox Highline traz ar, direção elétrica, trio elétrico, banco do motorista ajustável em altura, volante regulável em altura e profundidade, som CD/MP3 com entradas e Bluetooth, rodas de liga aro 15 (pneus 195/55), afora retrovisores elétricos tilt-down (baixa na hora em que a ré é engatada). Entre os opcionais livres estão as rodas aro 16, som com navegador, teto solar e revestimentos de couro. O módulo tecnológico soma controle de cruzeiro, espelho eletrocrômico, sensores de chuva e crepusculares e faróis de neblina com luz de conversão. Os faróis também são incluidos pelo módulo segurança, que acrescenta os controles eletrônicos.
Ao menos foi possível aproveitar a locação para fazer um teste dinâmico sobre uma lâmina de água. O Fox nem deu trabalho para fazer um circuito de cones que incluíam um slalom. Isso com as salvaguardas desligadas. Claro que com os controles de estabilidade e de tração funcionando, a dianteira ficou mais contida, sem espalhar e a sensação de segurança é bem maior, embora os sistemas intervenham com autoridade ao menor sinal de destracionamento. Nada de modo Sport aqui.
Se não era possível andar tanto, o jeito foi testar a cabine. A central GPS sensível ao toque é a mesma do Tiguan e Passat. É funcional e descomplicada. Há outras opções de cores no painel, como o cinza da versão Highline. Além do GPS, só o volante que traz impressão premium. Não é um truque inédito, quando o Passat CC chegou, logo emprestou seu volante funcional a outros VW.
Embora tenha sido o primeiro a lançar essa tendência de teto alto no Brasil, o Fox viu a chegada de concorrentes que espicharam em outras direções, como a distância entre-eixos e a largura.  Mesmo com o capacete obrigatório estilo formiga atômica, minha cabeça ficou a dedos do teto. Verticalizados, os passageiros até viajam com boa impressão de espaço para cabeças, porém as pernas não se sentem tão folgadas como no passado. As estações mudaram. Por isso mesmo, era mais do que hora da raposa mudar a pelagem.
Conheça todos os preços do novo Fox
Fox 1.0 Trendline - R$ 35.900
Fox 1.6 Trendline - R$ 39.800
Fox Bluemotion 1.0 - R$ 37.690
Fox 1.0 Comfortline - R$ 38.190
Fox 1.6 Comfortline - R$ 41.490
Fox 1.6 Comfortline I-Motion - R$ 44.590
Fox 1.6 Highline - R$ 48.490
Fox 1.6 Highline I-Motion - R$ 51.790
Acessórios
Aquecimento - R$ 500
Ar-condicionado - R$ 3.040
Bancos em couro - R$ 670 (somente para Comfortline e Highline)
Assistente de estacionamento - R$ 720 (exceto para versão Trendline)
Faróis e lanterna de neblina - R$ 460 (somente para Bluemotion)
Teto solar - R$ 2.470 (somente para Comfortline e Highline)
Sistema de som com GPS - R$ 3.050 (somente para Highline)
Roda de liga leve de 15 polegadas - R$ 1.210 (somente para Comfortline)
Roda de liga leve de 16 polegadas - R$ 1.400 (somente para Highline)
Volante multifuncional, computador de bordo e sistema de som - R$ 1.520 (somente para Bluemotion)
Volante multifuncional em couro com troca de marchas no volante - R$ 5.170 (somente para Comfortline)
  """},
    # txt5
    {'titulo': """Os lançamentos que chegam às lojas em setembro""",
             'corpo': """
Texto original: Nas próximas semanas, não faltará assunto à imprensa especializada de automóveis. Setembro será um mês bastante movimento com a estreia de, pelo menos, sete modelos de peso no mercado nacional. Ao menos três deles, novo Ford Ka, Fiat Uno reestilizado e Volkswagen Fox,  prometem balançar a categoria mais acirrada do mercado, o de hatches compactos. Mas ainda há novidades no segmento de picapes pequenas, sedãs compactos e até de utilitários esportivos. Confira:

A nova geração do Ka já começou à chegar as revendas da marca nesta semana. Mas não espere por um subcompacto mirradinho e basicão. O hatch encorpou e subiu na vida. Equipado com o novo motor 1.0 três cilidros da Ford, que rende até 85 cv com etanol, ele sai de fábrica equipado com direção hidráulica, ar-condicionado, rádio, além de vidros e travas elétricas. As versões mais completas e com motor 1.5 trazem até controle de tração e estabilidade e assistente de partida em rampa. Mas o upgrade elevou o custo do modelo, que tem preços entre R$ 35.390 e R$ 44.990.


Lançado oficialmente pela Fiat nesta semana, o Uno 2015 chega com pequenos retoques no visual externo, algumas novidades no interior e vários equipamentos, alguns até inéditos no segmento de compactos, como é o caso da tecnologia start-stop, que equipa e nova versão intermediária Evolution. Há também mudanças no câmbio automatizado Dualogic. Os motores 1.0 e 1.4 foram mantidos, sem alterações, e os preços agora variam de R$ 26.370 a R$ 36.650.


O Fox também recebeu um tapinha no visual e ganhou um "quê" de Golf para a linha 2015. Para assumir o lugar do Polo, ele também agregou novos equipamentos e na versão topo de linha até adotou o novo motor 1.6 MSI de 120 cv e estreou o inédito câmbio de seis marchas da marca. Controle de tração é item de série no Fox mais completo, enquanto o de estabilidade é opcional. A tabela do modelo ficou entre R$ 35.900 a R$ 51.790.

Além do novo Fox, estreia nas revendas da Volkswagen a inédita Saveiro Cabine Dupla, versão que veio incrementar a linha da picape. A versão mais cara, a Cross, também é oferecida com o motor 1.6 de 120 cv e equipada com controle de tração e estabilidade.

Novo Honda City
Com retoques visuais que o deixaram mais próximo do irmão maior Civic e com um ar mais sofisticado, o novo City chega ao mercado com preço inicial de R$ 53.900. Entre as novidades, está o interior reformulado e a adoção do câmbio CVT que simula sete marchas nas versões EX e EXL. É a mesma caixa presente no monovolume Fit.

O novo Cherokee já está à venda no Brasil. Ele é oferecido em versão única, de R$ 174.900, que é equipada com motor 3.2 V6 de 275 cv de potência e 32,3 kgfm. O câmbio é automático de nove velocidades e a tração 4x4.

Suzuki Swift
Com motor 1.6 aspirado de 142 cv, o hot hatch japonês chega ao país em duas versão: a normal de R$ 74.990 e a R, mais esportiva, de R$ 81.990.
SAIBA MAIS
ACELERAMOS O SUZUKI SWIFT SPORT, QUE CHEGA POR R$ 74.990
             """},
    #txt6
    {'titulo': """Top 5 do segundo dia da New York Fashion Week""",
             'corpo': """
MELHOR MÚSICA: a trilha sonora mais marcante do dia foi a do desfile de Diane Von Furstenberg, no fim da tarde, no Spring Studio, em Tribeca. Inspirada numa numa espécie de Brigitte Bardot contemporânea, DVF encheu a passarela de tons florais e xadrez vichy ao som de música animada. Ao terminar o desfile a estilista levou a platéia ao delírio com Parole, Parole, canção italiana que ficou famosa na voz de Alain Delon. Enquanto cantava a música, Diane ia cumprimentando os amigos e vips da primeira fila.
MELHOR SAPATO: As mules com cara de bota de couro branca, cor tendência da temporada, foram o ponto do desfile da Custo Barcelona pois quebraram os looks supercoloridos e transparentes da marca.

MELHOR GIFT: Durante entrevista com estilista Tommy Hilfiger no Hudson Studios, vimos em primeira mão as peças do verão 2015 que ele apresentará nesta segunda-feira (08), e conversamos sobre a moda atual. Os festivais de música dos anos 60 e 70 foram fonte de inspiração para a coleção e para um livro super bacana, com fotos históricas de artistas como Rolling Stones, que será lançado em breve.

MELHOR ESTREIA: colocar um estilista, como Anthony Vaccarello, que ama comprimentos micro, decotes e fendas no comando da Versus Versace, que é sexy por natureza só podia dar certo! E a apresentação da marca foi exatamente isso. Basicamente todo em preto, com pitadas de branco e dourado, o desfile foi perfeito para mulheres que amam sensualidade. No final, Donatella Versace entrou ao lado do designer e muitos levantaram para aplaudir.

        MELHOR PAUSA: aproveitar aqueles 20 minutos raros entre um desfile e outro para ir até o hotel trocar o salto e se deleitar com o pôr do sol maravilhoso da janela do 26 andar do Row Hotel em NYC"""},
    #txt7
    {'titulo': """NYFW: aprenda a fazer o look girly da maquiagem da grife Giulietta""",
             'corpo': """
A grife Giulietta fez sua apresentação no New York Fashion Week neste domingo (7) no Hotel Highline, um lugar supercharmoso no bairro do Chelsea, em Manhattan. A Marie Claire foi até o backstage para conversar com o maquiador Dominique Samuel e conferir os detalhes de como foi feito o make.

"Eu quis criar uma maquiagem girly e feminina", comentou Dominique. Para fazer a pele, ele aplicou a base (“Dream Wonder”, da Maybeline) com um pincel. "Depois segui para o contorno do rosto. Minha intenção foi criar uma silhueta à la Sophia Lauren", emenda. Dominique utilizou uma sombra clara no centro da testa, têmporas, queixo e nariz. Esse é o truque do contorno, uma técnica de luz e sombra aplicada à maquiagem.

Para o ar girly, Dominique utilizou um blush rosa (“Master Blaze”, também da Maybelline, na cor pink fever) nas maçãs do rosto. O mesmo blush foi usado nos lábios. "Apliquei em seguida uma lápis de boca, assim consegui o tom que queria e o efeito do batom", explicou. Aprovaram o resultado?
        """},
    #txt8
    {'titulo': """Adriana Sant'Anna malha com barriga de fora e reclama de pernas finas""",
             'corpo': """
Adriana Sant'Anna mostrou animação na manhã de segunda-feira (8) e correu para a academia. A ex-BBB mostrou o look do dia para os seguidores: top com um pouco de transparência e uma sainha, que a deixou com barriga e pernas à mostra.

Na legenda, a loira reclamou das pernas, que ela considera finas, e promete investir pesado em um novo treino para os membros inferior. "Ja viram as coxas de um franguinho?  Pois é... Estao ai! HAHAHAHAHAHA!  A partir de hoje eu e Fernanda vamos levar a sério os treinos. Prometemos nao faltar um dia de academia por pelo menos 2 semanas!  Vamos ver se consigo! ( mais tarde eu vou)  Beijooos  #truquesdaadriana", escreveu ela.

        """},
    #txt9
    {'titulo': """Joan Rivers baniu Michelle Obama e Adele de seu próprio funeral""",
             'corpo': """Mesmo após a sua morte, Joan Rivers continua sendo notícia por suas declarações bizarras. A comediante, que morreu depois de ter uma parada respiratória e entrar em coma durante uma operação nas cordas vocais na quinta-feira (8), havia concedido - semanas antes de morrer - uma entrevista ao The Sunday Times Magazine falando de nomes que não gostaria de ter no seu funeral.

Durante a conversa - que foi publicada na segunda-feira (8) pela agência WENN -, Joan discutiu o fato de saber que a sua morte estava próxima e sobre o quanto tentava ajudar a sua filha, Melissa Rivers, a aprender a lidar com isso.

"Eu não quero ouvir e nem falar sobre isso. Mas eu digo que está chegando. É inevitável. Não é mais algo abstrato. Eu já estou nos meus 80 anos. Quando eu morrer, ninguém vai dizer: 'Como ela era jovem'. Eles vão dizer: 'Ela teve uma ótima jornada".

Irreverente, ela contou à publicação sobre nomes que gostaria de ver banidos de seu enterro, incluindo o de Michelle Obama, a quem chamou de "travesti", Adele "por ser acima do peso" e a apresentadora Chelsea Handler, que, segundo Joan, "é uma bebada e uma v****a".
        """},
    #txt10
    {'titulo': """Dilma diz que Mantega não fica como ministro em um segundo mandato""",
              'corpo': """
A presidente Dilma Rousseff afirmou nesta segunda-feira, durante entrevista ao jornal "O Estado de S. Paulo" transmitida pela internet, que o ministro Guido Mantega, da Fazenda, não permanecerá no governo na hipótese de ela vir a ser reeleita.
"O Guido já me comunicou que ele não tem como ficar no governo em um segundo mandato por razões eminentemente pessoais que peço que vocês respeitem", declarou.
Na semana passada, a presidente já havia afirmado que fará mudanças em sua equipe de governo se vencer as eleições de outubro. Na ocasião, ela disse, sem detalhar quais pastas seriam alvo de alterações, que "governo novo, equipe nova".

Questionada se, com a substituição de Mantega, alterará a política econômica na hipótese de reeleição, Dilma voltou a mencionar, sem citar nomes, o episódio da eleição municipal de 1985, em São Paulo, e não respondeu. Na ocasião, o ex-presidente Fernando Henrique Cardoso (PSDB), à época candidato do PMDB à Prefeitura de São Paulo, posou para a imprensa sentado na cadeira do prefeito um dia antes da eleição. FHC perdeu para o ex-presidente Jânio Quadros.
Confrontada com a informação de que o adversário Aécio Neves (PSDB) já anunciou o nome do ministro da Fazenda caso seja eleito (Armínio Fraga), Dilma não quis falar sobre o eventual novo ocupante da pasta.
"Não vou nunca dizer quem vai ser ministro no meu segundo mandato, se porventura eu tiver. Quem indica ministro antes da hora, eu acho que dá azar. Da ultima vez que fizeram isso – você lembra – dá azar", declarou.
Dilma afirmou que poderá haver mudanças na economia porque o país "se preparou para essas mudanças". "Temos condições de diminuir alguns incentivos e outros não. Vamos continuar a garantir emprego e valorização do salário mínimo", disse.
Delação premiada
A presidente afirmou que recorrerá até ao Supremo Tribunal Federal (STF) para saber se há integrantes do governo envolvidos com as denúncias resultantes de delação premiada do ex-diretor da Petrobras Paulo Roberto Costa, preso no Paraná.
Costa vem dando depoimentos diários a policiais federais e procuradores da República em troca da possibilidade de redução da pena. Nos depoimentos, Costa delatou dezenas de senadores e deputados federais de três partidos (PT, PMDB e PP), governadores e um ministro como supostos beneficiários de um esquema de pagamento de propinas com dinheiro de contratos da Petrobras.
Dilma afirmou que, após a divulgação das denúncias pela revista "Veja", determinou ao ministro da Justiça, José Eduardo Cardozo, que fizesse um ofício à Polícia Federal pedindo acesso ao conteúdo dos depoimentos para saber se há algum funcionário ou agente do governo envolvido.
"Quando a PF respondeu que não pode, fiz ofício ao procurador-geral da República que me informe, para proceder às providências cabíveis [...] Se o Ministério Público não for capaz de responder, vou pedir ao Supremo Tribunal Federal", declarou.
Segundo a presidente. ela necessita das informações oficiais porque não sabe se as informações divulgadas pela imprensa são verdadeiras. "Primeiro é preciso saber se isso é verdade. Então, eu quero essas informações. [...] Quem vai falar isso para mim é quem investiga [...] e não diz-que-diz-que. Quero a informação a mais aprofundada possível", afirmou.
A candidata disse que tem a "competência" para dar informações sobre o assunto é Polícia Federal, o Ministério Público ou o Supremo Tribunal Federal. "Baseado na informação da imprensa, eu não posso condenar ou perdoar ninguém", disse.
              """},
    #txt11
    {'titulo': """Antônio de Souza: Mídia e direita se “esquecem” de propineiros nas chapas de Aécio, Alckmin e Serra""",
           'corpo': """Pelo menos desde 2006, sistematicamente, a mídia e grupos de direita têm lembrado aos eleitores que havia na chapa do PT os chamados “mensaleiros”, denominação equivocada, visto que não se provou pagamentos mensais e sequer a Justiça anulou a votação da Previdência, uma das que teriam sido influenciadas pelo suposto “mensalão”.

O “mensalão” já foi julgado, mas serviu como instrumento da direita para caracterizar o PT ligado à corrupção e, com isto, se esqueceu dos maiores escândalos do Brasil: o caso Banestado e a privataria tucana. Agora, com as novas denúncias da Petrobrás, a mídia dá a impressão que a simples citação já é prova suficiente para a pessoa ser condenada, tanto que exige providências sem saber se existem provas reais e concretas.

A ética seletiva da direita e da turma do ódio acaba se esquecendo, propositalmente, do “mensalão tucano mineiro” e dos pagamentos para o então candidato Aécio Neves.

Agora, enquanto a Siemens entrega provas do propinoduto tucano para o Ministério Público Paulista, sobre o escândalo do Metrô, CPTM e setor elétrico, encontramos uma situação muito interessante, para não dizermos cínica: a turma da propina vai disputar a eleição e a mídia se cala frente a esta participação.

Rodrigo Garcia (DEM) é candidato a deputado federal na chapa de Geraldo Alckmin, o candidato do PSDB ao governo do Estado de São Paulo. O DEM é o mesmo partido citado em outro mensalão, o de Brasília, do governo Arruda. E o pior: é o partido de um dos coordenadores da campanha de Aécio, o senador Agripino Maia.

Outro citado no caso é José Aníbal, que agora é candidato a suplente do ex-governador  José Serra na disputa por uma vaga no Senado.

Agravante: segundo o plano tucano, se Aécio for eleito e Serra  se tornar ministro, o propineiro Aníbal vai ser senador.

Isso sem falar que o candidato Serra vai ter de depor, em 7 de outubro, na Polícia Federal para explicar a sua participação no propinoduto.

Agora o governador Alckmin, que diz que nada sabia disto tudo, afirma que a Petrobrás foi assaltada, mas  se esquece de dizer que  o metrô também foi. E o pior: nada faz para acabar com o esquema no metrô e ainda é conivente em que esses candidatos que disputam a eleição na sua chapa.

Além do mais, o candidato a vice-presidente da chapa de Aécio, o senador Aloysio Nunes, é citado pelos executivos da Siemens como um dos envolvidos neste esquema.

O coordenador da Casa Civil de Alckmin, Edson Aparecido, é tricampeão em citações: casos Castelo de Areia, Demop e agora o propinoduto tucano (Alston /Siemens) (aqui e aqui).

Isso sem falar de Andrea Matarazzo, investigado  pela citação de propina ao partido no setor elétrico (aqui e  aqui) e que agora responde como coordenador da campanha de Aécio na capital paulista.

Desta forma, é cínico o discurso tucano em defesa da ética e contra a corrupção, visto que esta turma fala uma coisa, mas faz outra.

Ante todas as acusações contra petistas como André Vargas e Luiz Moura, o PT fez a limpeza e mandou estes senhores para fora, mesmo antes de serem condenados. Agora se esperava o mesmo gesto dos tucanos e do DEM, mas, ao contrário, se deu legenda e os trata como se nada houvesse ocorrido. Lembrem-se de todos esses fatos na hora de votar.

Por último, este texto visa lembrar a todos estes fatos e fazer com que as pessoas se lembrem disso na hora de votar.

A propósito. O programa de rádio de Alckmin afirma que o governo   Luiz Antônio Fleury Filho foi um desastre  e quebrou são Paulo. O governador “esquece” que senador Aloysio Nunes era o  vice de Fleury e foi aeu secretário de Transportes Metropolitanos. Muitos dos citados no propinoduto já trabalhavam nesta gestão como Zaniboni, entre outros.

           Assim, o desespero de Alckmin com a possibilidade cada vez mais concreta de segundo turno  faz com  que ele dar esses tiros no pé. """},
           #txt12
    {'titulo': """ O Porvir do Brasil I: A Economia, Banco Central e o Estatuto do Comum""",
     'corpo': """Marina Silva propôs conceder, caso eleita, "autonomia formal em lei" ou "independência" para o Banco Central. Ainda que isso dependa do Congresso Nacional e que, ainda por cima, Marina não tenha explicado muito bem como seria essa mudança, o fato é que a questão acirrou o debate político no que toca à economia:  Dilma saiu da defensiva e se colocou contra no debate do SBT. O que nos interessa, no entanto, para além da polêmica eleitoral, é justamente o que há por trás da proposta desregulamentadora de Marina e, também, da regulamentação do desenvolvimentismo: para além, inclusive, da dialética público x privado, temos de debater as implicações dessa polêmica -- de fundo e forma estatal -- sobre a vida, criação e riqueza comum.

O Lulismo forçou uma virada nunca antes imaginada. Mesmo mantendo a indexação econômica para o capital -- a correção monetária --, ele criou a correção monetária inversa, no campo de política de valorização do salário mínimo, forçou a formalização do emprego, buscou estimular a criação de mais empregos com melhores salários. Criou programas sociais que geravam renda em contraste com o rentismo do capital. A economia passou a se orientar desde baixo como diria Bruno Cava ao analisar a potência do estímulo ao consumo do proletariado, fato desapercebido pelas esquerdas que viam no consumo algo necessariamente ruim.

O estímulo ao consumo [da multidão] reorientou a produção: não havia mais escravos trabalhando para produzir um café cujo sabor -- e rendimentos -- nem passaria perto de seus corpos; havia, agora, o trabalhador -- ou qualquer um -- com poder de consumo demandando produtos que significavam algo para si: passamos a ver, por exemplo, cosméticos para a população negra brasileira surgir, o que há dez anos era raro.

Os negros só produziam coisas para outrem, não tinham salário suficiente, não havia motivo para o mercado produzir qualquer coisa para eles. O aumento da renda lhes permitiu não apenas o consumo do que era produzido, mas passou a demandar a produção de bens e serviços na quantidade e qualidade que eles precisavam.

O Lulismo pôs o capitalismo em curto-circuito porque mexeu no seu dispositivo básico: a economia capitalista não é super-consumo, mas consumo aquém, em quantidade e qualidade, de uma superprodução estéril -- voltada para a estocagem, o que permite o cacife para o controle dos preços; o capitalismo produz muito, mas produz em tom de exclusividade; o produzido é exclusivo e excludente.

Se a produção capitalista se dá partir da exploração do comum -- das riquezas, conhecimentos e ações comuns, para ser mais exato --, ela se aperfeiçoa em um último instante, no mercado, enquanto bem incomum, exclusivo e pronto -- o fetiche da mercadoria é uma hipnose bem própria, é o transe entre a vontade de consumo e, na outra ponta, a superabundância inacessível. Do outro lado, a imanência entre consumo e criação seria o comunismo.

A diminuição do déficit entre consumo e produção, nem é preciso dizer, gerou mudanças consideráveis. Não apenas se produziu e se consumiu mais como, também, os libertos, em certa medida, passaram a se sentir autorizados a desejar. Desejar direitos. Passaram a se sentir livres para desejar o que lhes era lícito só até o momento em que resolvessem deseja-lo. O confronto desse novo brasileiro, assujeitado a qualquer coisa, com as velhas estruturas políticas e sociais levaram a um conflito iminente em plena era da crise econômica -- e da economia de crise 2.0.

Dilma, em sentido contrário, se preocupava em resolver esse enigma social por uma subjetividade nova, a síntese dessa multiplicidade incontrolável na forma de classe média. No entanto, o modo de vida médio-classista -- sua disciplina do trabalho, sua ansiosa insatisfação face à sua condição insustentável de ser (social) -- anteviam uma explosão. E de fato ela aconteceu, mas não apenas por isso.

É preciso ver o jogo de tensões e pretensões: a reação aos ganhos sociais já chegava ali, por mais que parecesse irracional, uma vez que o capital estaria ganhando com esse jogo. Mas as corporações, antes de pensarem nos ganhos presentes, pensam no futuro: e o futuro depende da manutenção do controle do trabalho. Não são raros os momentos na história que o capital, apesar dos riscos de ganhar menos ou perder agora, opta pelo mando.

No nosso caso, era preciso responder nos preços o que os trabalhadores ganhavam nos salários, o que os pobres ganhavam no bolsa família. Na medida em que a escassez à dignidade diminuía, a exclusividade da cidadania mínima rareava junto com seu preço; era preciso pôs as coisas de volta no lugar. Mas junto disso, a economia encontrava a soma da velha inflação das indexações mil, dos muitos monopólios, gerando uma inflação em cascata.

Nos últimos anos, as tentativas de segurar essa inflação pela valorização do câmbio, por subsídios brancos ao preço dos derivados de petróleo e à energia elétrica, no protelamento dos reajustes de tarifas, desonerações tributárias etc se mostraram um erro. A resposta social à ascensão da classe sem nome era possível pelo grau de concentração de mercado.

Mas não só, a existência, desde os primórdios do plano Real, de uma correção monetária, cria uma inequívoca inflação inercial, que incidirá, aconteça o que acontecer: quando a lei autoriza a reajuste para mais o preço de algo com fundamento, vejamos nós, na perda de valor em abstrato do poder de compra na moeda, o fato é que o reajuste é, na verdade, inflação em concreto. Os reajustes anuais de aluguéis, de tarifas de luz, água geram um fluxo de carestia interminável. É um problema objetivo da arquitetura do sistema.

Enquanto isso, se a política de protagonismo dos bancos públicos no crédito para o consumo funcionavam, por outro lado, o protagonismo do BNDES no financiamento do capital é um fracasso: no sentido em que o empreendimento favorecido já era a grande corporação oligopolista, a qual por seu gigantismo não inova ou produz com mais eficiência -- seja ocupando concessões públicas, se agenciando com as grandes obras e serviços públicos (como nos casos de empreiteiras) ou pelo simples domínio de mercado elevado (no caso das atividades replicáveis como, p.ex., a produção de automóveis ou alimentos).

Novamente ele, o nosso velho conhecido grau de monopólio. A forma de financiamento absolutamente molar da produção, calcada no grande empreendimento, se une à parafernália burocrática. Esse processo perverso serve, lembrando Eduardo Pimenta de Mello, para fazer com que existam apenas empreendedores pequenos -- e débeis -- e grandes corporações [ou, no campo, a fazenda high-tech do agronegócio e a pequena, e paupérrima, propriedade do agronegócio]. Isso não foi superado. Só se cresce com escala porque o sistema não permite um aumento racional do empreendimento.

Se em uma ponta o novo mercado interno demanda qualidade e quantidade -- e até obtém isso -- o "sistema produtivo" atola (1) no aspecto [veladamente] político da sua gestão, (2) na concentração de mercado que permite não uma improdutividade, mas o controle sobre a produção e sua estocagem mediante a política de preços; (3) na perda de valor monetário por meio dos indexadores que, a rigor, favorecem o capital rentista (e a possível financeirização até de serviços públicos).

Esses enormes brontossauros incapazes de dar conta da demanda nova. Apenas acumular, não investir, fazer aquisições, acomodar, de qualquer forma, força de trabalho empregada só para se dispôr com o governo -- aumentando salários que são "resolvidos" no preço final.Eis aí o nó górdio. Medidas voluntaristas como a baixa forçada da taxa Selic, sem o fim da correção monetária ou o combate ao monopólio, em Dilma esbarraram nesse cenário de insuficiência.

A taxa de juros alta se deve à inflação alta, a qual expressa a oferta anômica para uma demanda que, felizmente, continuou a ser irrigada desde baixo. Essa a contradição que o dilmismo tem pela frente: avançar nas redes produtivas, o que demandaria pensar o micro-empreendimento, inclusive cooperativo. Mas não é nada simples.  Por outro lado, os ataques que ora se insurgem, com força, contra sua política econômica tenham, também, outra conotação: o oligopólio financeiro nacional, pressionado pela concorrência dos bancos públicos, deseja recuperar o protagonismo que perdeu porque quis.

Quando Marina fala em dar "independência" ao Banco Central, sua medida esotérica sinaliza para, sob os auspícios da técnica, dar poder político efetivo ao oligopólio financeiro na formulação da política monetária. Uma vez tomado o BC, estaria tomado  o COPOM e o governo teria de escolher entre concordar com o BC ou, quem sabe, discordar de um órgão emancipados deus sabe como e ver a briga da mão com o pé.

E, naturalmente, impor uma política de juros altos, para substituir  "um voluntarismo" ou "ativismo" monetário, não resolve o problema -- para além do óbvio favorecimento do oligopólio financeiro. Ou se, por um lado, mobilizar os bancos estatais para financiar as grandes corporações é um erro -- não no sentido moral, mas no sentido prático --, por outro lado, retirar-lhes esse protagonismo sem pôr nada no lugar, sem os bancos privados terem essa disposição, vai gerar, em outro sentido, a falta de financiamento para produção e consumo.

Mas o problema do revival (neo)liberal de Marina não para por aí: num certo neo-malthusianismo que recorre à crítica do consumo -- e não raro a temática ambientalista surge nesse sentido como -- e não identifica na superprodução -- e na gestão que permite essa desvinculação -- o problema chave. O culpado é o trabalhador que passou a consumir.

É o consumo que geraria mais produção -- uma hipótese absurda, uma vez que, p.ex., o Brasil produz há tempos os alimentos suficientes para que não haja fome, mas nem por isso deixou de haver fome, fosse assim, o consumo baixo, até poucos anos, levaria a uma hipo-produção agrícola!

O paradoxo é que não há vinculação entre produção e consumo nesse sentido, mais consumo, quando socialmente espraiado, requalifica a produção, não necessariamente exige que se produza mais. Não é um argumento de fundo tão melhor quanto a retórica do "ajuste" de Aécio, o qual, em prol de ganhos imediatos, iria arrochar o mercado interno -- as tais "medidas impopulares" que ele não teria medo de implementar: isso é ruim não porque é imoral (embora até seja), mas porque não funcionaria, levando ao mesmo círculo vicioso dos anos 1990.

O debate econômico, em tempos difíceis, nunca foi tão urgente. A crise do modelo atual e as novas saídas, que mais parecem aporias, impõem uma crítica radical.
"""},
    #txt13
    {'titulo': """Cardozo ou a PF: quem explicará o vazamento? - por Ricardo Kotscho""",
     'corpo': """Passaram-se já mais de 48 horas desde que veio a público o "vazamento" da delação premiada feita à Polícia Federal pelo ex-diretor da Petrobras Paulo Roberto Costa, em nova tentativa de alterar os rumos da campanha presidencial, com base em denúncias sobre um esquema de corrupção montado na empresa por políticos e partidos da base aliada do governo Dilma Rousseff, atingindo também o PSB da candidata Marina Silva.

É bastante estranho o silêncio mantido até o momento, manhã de segunda-feira, pelo ministro da Justiça, José Eduardo Cardoso, e a Polícia Federal a ele subordinada, como se fosse a coisa mais normal do mundo um "vazamento" destas proporções, sem qualquer prova, baseado em fontes anônimas, a quatro semanas da abertura das urnas, sabendo-se que este processo está submetido a segredo de Justiça e os áudios e vídeos da delação devem ser criptografados e mantidos num cofre forte.

Ao terminar de escrever a coluna de domingo, sobre as consequências nas campanhas presidenciais destas graves denúncias lançadas no ar, ficou martelando na minha cabeça uma dúvida: ninguém vai explicar ou pelo menos procurar saber em que condições e a serviço de quem se deu mais este explosivo "vazamento", mais conhecido por "bala de prata", que já se tornou comum em vésperas de eleição em nosso país? Ninguém se dignou a questionar este ponto entre as dezenas de análises que li sobre o assunto e seus desdobramentos ou a indagar quem pode ter vazado estas "informações".

Dá-se de barato que estas denúncias realmente existem, que o delator só falou a verdade e tem provas do que disse, mas o que temos por enquanto é apenas mais um factoide midiático explorado à exaustão pelo esquema Veja-Globo-Folha-Estadão, e seus respectivos portais, com os concorrentes retroalimentando o noticiário entre si.

Nem é preciso ser muito esperto para descobrir a quem interessa tumultuar o processo eleitoral nesta reta final da campanha. Quando parecia que este esquema já tinha abandonado o  tucano Aécio Neves à sua própria sorte, e aderido ao Furacão Marina, mesmo com algumas restrições, eis que o candidato atolado em terceiro lugar ressurge das cinzas no noticiário, com tanto ímpeto para atacar as duas adversárias que lideram com folga todas as pesquisas, como se já soubesse de alguma coisa antes da anunciada "bomba" explodir no último final de semana.

Como sabemos, as investigações sigilosas estão sob a responsabilidade da Polícia Federal, quer dizer, de um órgão do Ministério da Justiça, que vêm mantendo um obsequioso silêncio sobre o assunto. De que forma nós, simples cidadãos eleitores, poderemos saber o que existe de verdade no que vem sendo noticiado a respeito do "mar de lama da Petrobras"? Ou o objetivo é mesmo só fazer a divulgação oficial depois de 5 de outubro, mantendo as suspeitas no ar para a devida exploração eleitoral?

Cabe registrar aqui a previsão feita por Aécio dias atrás, quando sua campanha definhava, baseado na experiência do seu avô Tancredo Neves, de que as definições eleitorais só começam para valer depois do feriado de 7 de setembro.

Tem alguma coisa estranha no ar e não são os aviões de carreira."""},
    {'titulo': "Existe facismo em São Paulo",
     "corpo": """Se existe amor em São Paulo eu não sei. Mas fascismo, esse existe. E a elite paulistana não faz nenhuma questão de escondê-lo.



Sabemos que não é de hoje. A história da segregação territorial em São Paulo vem dos anos 1940, quando se inicia de forma mais sistemática a demolição dos cortiços e das residências operárias nas regiões centrais. Pobre tem que vir ao centro para trabalhar e servir, mas morar ali? Não, aí já é vandalismo!



Foi assim que surgiram e se expandiram as periferias da cidade. Numa jogada de mestre e sempre com o apoio do Estado, os agentes imobiliários conseguiram, ao mesmo tempo, tirar os pobres do convívio nos bairros centrais, ganhar um bom dinheiro com loteamentos clandestinos na periferia e reservar áreas intermediárias para a especulação. Nessas áreas estabeleceram-se depois os verdadeiros nichos da elite paulistana.



O que estava em jogo era materializar no território a segregação social entre ricos e pobres.



Até hoje a dinâmica do mercado imobiliário reproduz esse fenômeno. Quando um bairro recebe investimentos ou passa a hospedar grandes empreendimentos privados –condomínios de alto padrão, shoppings, etc– sofre um processo intenso de valorização. Expulsa assim os moradores mais pobres, por vezes através de despejos coletivos e mais frequentemente pela hipervalorização dos aluguéis.



Esta dinâmica econômica sedimentou uma mentalidade higienista na elite e nas camadas médias. Veio junto com uma fobia, um nojo, uma recusa da convivência. Seu ideal seria que os pobres trabalhassem para servi-los, mas ao fim do expediente evaporassem, para retornar apenas no dia seguinte. Pobres podem até existir, desde que longe de seus olhos.



Há casos emblemáticos e recentes. Em 2010, os seletos moradores de Higienópolis iniciaram um movimento contra uma estação de metrô nas redondezas. Motivo: traria ao bairro "gente diferenciada". Em 2011 foi a vez de uma turma de comerciantes e moradores de Pinheiros, que se organizaram contra um albergue para moradores de rua no bairro. "Ficaremos acuados em casa", alegaram na ocasião.



As rampas antimendigo, iniciadas na gestão de José Serra para impedir moradores de rua em certas partes da cidade, deram a chancela do poder público.



Mas o pior ainda estava por vir. A face mais perversa deste fenômeno foram os incêndios em favelas. O mercado imobiliário é mesmo muito criativo. Quando, por alguma eventualidade, o judiciário barra o despejo de uma favela localizada em zona de expansão imobiliária eles fazem a seu modo. Incendiar favelas tornou-se um recurso habitual para afastar pobres dos condomínios de alto padrão.



Em muitos casos é difícil provar, o que permitiu aos interessados atribuir os incêndios à baixa umidade do ar. Mas os indícios são avassaladores. O site http://fogonobarraco.laboratorio.us/ reuniu o mapa de todos os incêndios em favelas paulistanas de 2005 a 2014 e comparou as regiões incendiadas com o índice de valorização imobiliária. O mapa mostra como a enorme maioria dos incêndios ocorreu nas zonas de valorização. Mais inflamável que o clima seco é a especulação.



Os dados dizem ainda que metade dos incêndios dos últimos 20 anos ocorreram entre 2008 e 2012, isto é, durante a gestão de Gilberto Kassab (PSD) como prefeito, que foi marcada pela promiscuidade com o mercado imobiliário. A conivência do poder público também é inflamável.



No último domingo (7), o tema voltou com o incêndio em uma favela na região do Campo Belo. O bairro valorizou-se 130% nos últimos 5 anos, de acordo com o Índice Fipe/Zap. Tinha uma pedra no sapato do mercado imobiliário e da mentalidade fascista que foi novamente varrida com fogo. Os bombeiros que foram até o local afirmaram em reportagem que o incêndio foi mais uma vez criminoso.



Também nos últimos dias, a Folha noticiou que condomínios do Morumbi estão se mobilizando contra a ocupação Chico Mendes, organizada pelo MTST num terreno municipal com destinação prevista para habitação popular. Uma das ilustres moradoras disse tudo: "Atrapalhando eles estão, é desconfortável". Atrapalhando a vista de sua sacada, pobres, ali, ao lado.



A realidade é mesmo desconfortável. A mentalidade fascista atua para negar, queimar, expulsar este desconforto para bem longe. Que os pobres existam, mas em algum lugar de Ferraz de Vasconcelos, bem longe da minha sacada.



     São Paulo é uma das cidades mais desiguais do mundo. A cidade dos muros. Dos muros, incêndios criminosos, despejos, rampas antimendigo e dos condomínios exclusivos. O fascismo da elite só coloca mais combustível neste barril de pólvora.""",
 },
    {'titulo': """ Em novo vídeo, PT diz que Marina vai tirar R$ 1,3 tri da educação""",
     'corpo': """Depois de simular reunião de banqueiros, propaganda de Dilma explora posicionamento de adversária sobre o pré-sal para dizer que eventual governo prejudicaria investimentos federais

                        São Paulo - A campanha da presidente Dilma Rousseff publicou novo vídeo nesta quinta-feira, 11, em que procura associar um eventual governo da candidata do PSB, Marina Silva, a cortes de investimentos e a interesses do mercado. Depois de atacar a proposta de autonomia do Banco Central, a propaganda explora o posicionamento da adversária sobre o uso do pré-sal e o suposto impacto para os investimentos em educação e saúde.
Rands critica 'discurso Regina Duarte' do PT"Marina tem dito que, se eleita, vai reduzir a prioridade do pré-sal. Parece algo distante da vida da gente, né? Parece, mas não é", diz um locutor. O vídeo de 30 segundos, publicado no canal oficial da campanha no YouTube, repete o formato da propaganda veiculada na terça-feira. Um grupo de homens simula uma reunião de negócios. Na sequência, aparece uma mulher estudando com crianças. À medida que o locutor narra as propostas de Marina, os conteúdos dos livros desaparecem e os empresários parecem comemorar um acordo.
No vídeo, o locutor diz que, ao reduzir a prioridade do pré-sal, a educação e a saúde poderiam perder R$ 1,3 trilhão, além de ameaçar "melhores de empregos". "É isso que você quer para o futuro do Brasil?", conclui o narrador.
A veiculação da propaganda com os ataques à proposta de dar autonomia ao Banco Central desagradou a campanha de Marina, que chegou a acionar o Tribunal Superior Eleitoral (TSE), acusando o PT de " estelionato eleitoral". A corte, no entanto, decidiu manter o vídeo no ar por entender que não houve ofensa à candidata.
Na avaliação do comando da campanha petista, a repercussão do material foi positiva e a estratégia de associar a adversária ao rótulo de "candidata da elite" seria mantida. As propagandas vão manter a abordagem de afirmar que as ideias de Marina podem ameaçar a continuidade de programas sociais, causar desemprego e a volta da inflação.                                            "

"""},
    {'titulo': """Autonomia do BC é para acabar com inteferência política, diz Marina""",
     'corpo': """Candidata do PSB usa horário eleitoral da TV para responder aos ataques da campanha de Dilma a propostas da adversária sobre Banco Central e exploração do pré-sal

PT reforça tática antieliteAécio critica ataques pessoais de Dilma a Marina São Paulo - Após a campanha da presidente Dilma Rousseff intensificar os ataques à candidata Marina Silva (PSB), a ex-ministra usou o horário eleitoral desta quinta-feira, 11, para responder às principais críticas dirigidas ao seu programa de governo. Na TV, Marina prometeu continuar com o Bolsa Família, usar os recursos da exploração do pré-sal para investir em saúde e educação e explicou o que é a autonomia do Banco Central.
"A autonomia do Banco Central será para nomear técnicos competentes que irão trabalhar sem a interferência de políticos preocupados com a próxima eleição", afirmou a candidata. A política de comando do Banco Central tem sido um dos alvos da campanha petista, que procura explorar a ideia de que a medida vai ameaçar, por exemplo, a manutenção de programas sociais e o controle da inflação. "O Bolsa Família vai continuar como um direito garantido para as famílias necessitadas terem acesso mesmo quando mudarem os governos", disse Marina no horário eleitoral.
A campanha do PSB simulou um "povo fala" para rebater outros ataques, entre eles sobre a exploração do uso do pré-sal, temática que também começou a ser abordada com mais frequência nas propagandas do PT. "Os recursos do pré-sal são para saúde de educação. Não vamos permitir o desvio para a corrupção", disse a candidata.
Cerca de três horas antes da exibição do horário eleitoral, o canal oficial da campanha de Dilma noYoutube publicou um novo vídeo que procura associar um eventual governo de Marina a cortes de investimentos e à submissão a interesses do mercado. A primeira inserção falou sobre o Banco Central e a publicada nesta manhã, sobre o pré-sal. Na avaliação do comando da campanha petista, a repercussão do material foi positiva e a estratégia de associar a adversária ao rótulo de "candidata da elite" deveria ser mantida.
Dilma e Aécio. A exemplo dos últimos programas, a propaganda petista dedicou quase metade dos seus 11 minutos para enumerar as ações dos governos Dilma e Lula no combate à corrupção. "A verdade é que hoje se combate muito mais a corrupção do que no passado", diz um locutor. Ao final, a propaganda repetiu as críticas à proposta de autonomia do Banco Central defendida por Marina, além de números de programas sociais do governo federal.
O horário do candidato do PSDB, Aécio Neves, começou relembrando as denúncias de corrupção na Petrobrás e as críticas comumente feitas à política econômica atual. Como parte da estratégia de associar Aécio aos correligionários mais bem pontuados nas pesquisas de intenção de voto nos Estados, o programa do tucano trouxe o governador de São Paulo, Geraldo Alckmin, candidato à reeleição. """},
    {'titulo': """Aécio critica ataques pessoais de Dilma e Marina e diz que não vai entrar no 'vale tudo'""",
     'corpo': """Montes Claros - O candidato do PSDB à Presidência, Aécio Neves, criticou nesta quinta-feira, 11, os ataques feitos pela campanha da presidente Dilma Rousseff contra Marina Silva (PSB). A ex-ministra está à frente do tucano nas pesquisas de intenção de voto, mas Aécio afirmou que não entrará "no vale tudo para ganhar eleição". Durante sua agenda em Montes Claros (MG), o tucano manteve a estratégia de associar Marina ao PT, lembrando o fato de a candidata já ter sido filiada ao partido e ministra do governo Lula.
RelacionadasMinistro de Minas e Energia causa 'vergonha alheia', diz MarinaTSE mantém propaganda do PT sobre autonomia do BC"Não faço a ela (Marina) qualquer acusação do ponto de vista pessoal. Acho absolutamente inaceitável o tipo de acusação que ela recebe hoje da presidente Dilma. Não entro nesse campo, entro no campo político", declarou, referindo-se a programas da campanha do PT como o que comparou a adversária do PSB aos ex-presidentes Jânio Quadros e Fernando Collor.
Aécio fez questão de pontuar o que considera incoerências do discurso adotado por Marina na atual campanha eleitoral em relação a sua postura diante de uma série de temas. "É preciso que saibamos, por exemplo, que a Marina candidata é a que abraça o agronegócio ou a aquela que propunha a proibição do cultivo de transgênicos no País. É a Marina que hoje defende a política econômica do PSDB, até indo além do que imaginamos adequado como a autonomia do Banco Central, ou é a Marina que no PT lá atrás combateu o Plano Real e votou contra a Lei de Responsabilidade Fiscal", disse.
O senador também voltou a associar Marina ao PT, em uma tentativa de se posicionar como "a mudança verdadeira em relação a tudo isso que está aí". "Vejo muitas semelhanças no discurso da candidata Marina, que respeito pessoalmente, com o discurso da candidata Dilma quatro anos atrás. Até porque conviveram muito tempo juntas no próprio PT", salientou.
Arrecadação. O fato de aparecer em terceiro lugar nas pesquisas de intenção de voto, segundo Aécio, não representou queda na arrecadação de sua campanha. Ele disse que, caso isso tenha ocorrido, não foi "avisado". O candidato destacou que sua campanha "não é rica", mas "vai bem" e será feita "dentro do planejado". E, apesar de ter arrecadado mais que o dobro da candidatura de Marina Silva, afirmou que "as doações estão indo de forma muito vigorosa para as outras candidaturas".
De acordo com a segunda parcial da prestação de contas à Justiça Eleitoral, a campanha tucana arrecadou R$ 42 milhões, contra R$ 19 milhões arrecadados pelo PSB. Mesmo somados, os valores não chegam à metade dos R$ 123 milhões arrecadados até o momento pela campanha pela reeleição da presidente Dilma Rousseff.                                            """},
    {'titulo': """A estratégia de Obama contra o Estado Islâmico em 5 pontos""",
     'corpo': """Em pronunciamento na noite de quarta-feira (10) o presidente dos Estados Unidos, Barack Obama, anunciou que o país vai se opor aos terroristas do grupo Estado Islâmico através de ofensivas aéreas. Sua condução não exige autorização do Congresso. As ações, segundo o presidente, se estenderão para a Síria pela primeira vez. Obama comparou o plano àquele que o governo já pratica em países como o Iêmen e Somália, em que drones e missões de tropas especiais são usados para combater militantes da Al Qaeda e grupos afiliados.  Apesar de elogiada pelo presidente, a estratégia adotada nesses países já foi apontada como ineficaz por críticos, além de provocar mortes de grande número de civis.

O pronunciamento de Obama ocorreu às vésperas do 13º aniversário dos atentados às Torres Gêmeas, no dia 11 de setembro de 2001. Segundo Obama, as ofensivas aéreas são necessárias para evitar que o Estado Islâmico se torne uma ameaça real para os Estados Unidos. Por ora, seus militantes foram os responsáveis pela decapitação de dois jornalistas americanos. Como as investidas são classificadas como medidas de contra-terrorismo, elas não precisam passar pelo Congresso americano para ser aprovadas. “Não hesitarei em agir contra o Estado Islâmico na Síria, tampouco no Iraque”, afirmou Obama.

Apesar de não precisar de autorização do Congresso para lançar essas ofensivas, a administração Obama espera encontrar apoio para autorizar o envio de munição e e organizar treinamento aos rebeldes sírios que se opõem ao regime de Bashar Al-Assad. O pronunciamento do presidente ocorre cerca de um ano depois de Obama ter afirmado que não usaria bombardeios aéreos contra Assad.

No pronunciamento de 15 minutos, Obama ressaltou que as operações militares na Síria e no Iraque, para deter o Estado Islâmico, serão diferentes das guerras anteriores travadas pelo país na região: “Eu trabalhei quatro anos e meio para acabar com as guerras, não para começá-las”. A nova estratégia não envolvera tropas americanas combatendo em solo:“Não seremos arrastados para mais uma guerra no Iraque”.

Obama disse que a estratégia americana está amparada em quatro pilares: ataques aéreos, apoio a aliados locais, esforços de contraterrorismo para prevenir ataques e minar o Estado Islâmico, e assistência humanitária contínua a civis. Seus planos podem ser organizados em 5 principais pontos:

1 – Ataques aéreos
Eles já ocorrem no norte do Iraque. Com seu auxílio, o exército iraquiano conseguiu conter o avanço do EI na região em meados de agosto. Obama se ampara no aparente sucesso já experimentado pela estratégia para aumentar a frequência dos ataques.  Como a ideia deu certo no norte do Iraque, é possível que funcione também no resto do país e na Síria. Por ora, os ataques americanos são limitados: são direcionados a ações do Estado Islâmico que possam trazer riscos humanitários óbvios ou ameaçar militares americanos atuando na região. Essas limitações deixam de existir.

A maior diferença entre a estratégia adotada até aqui e o plano anunciado na noite de quarta está na expansão desses ataques para a Síria. A estratégia foi desenhada para tirar proveito do maior ponto fraco do Estado Islâmico - suas rotas de abastecimento. A intenção dos americanos é interromper a movimentação dos terroristas entre o Iraque e a Síria, onde eles obtêm munição, combustível e suprimentos.

2 – Evitar a ação de tropas americanas em solo
Um dos pontos mais destacados no pronunciamento de Obama. Segundo ele, não serão enviados soldados americanos para o campo de batalha.

3 – Conquistar o apoio de atores regionais, como a Arábia Saudita, e aliados europeus.
Obama quer buscar o apoio de governos sunitas na região e de aliados europeus. Na quarta-feira, a administração Obama entrou em contato com monarquias sunitas, em busca de seu auxílio para armar, treinar e abastecer rebeldes sírios contra o Estado Islâmico.
Obama também tem planos para deter o financiamento internacional para o EI. Para isso, pretende contar com o apoio dos países europeus. Outra preocupação é que os cerca de 1500 europeus que viajaram para lutar com o Estado Islâmico retornem aos seus países de origem para organizar atentados. Os EUA vão cooperar com esses países para deter possíveis atentados

4 – Treinar e armar as tropas iraquianas e curdas
Cerca de 470 soldados americanos serão enviados para a região para treinar o exército iraquiano e as tropas curdas. Espera-se que, com mais treinamento e equipamento melhor, esses aliados serão capazes de deter o avanço do Estado Islâmico e retomar os territórios já conquistados.

5 – Treinar e armar rebeldes sírios
     Para que os ataques aéreos surtam resultado, o governo espera contar com aliados em solo que retomem o controle de territórios ocupados pelo Estado Islâmico na Síria. Obama já destinou US$500 milhões para armar os rebelde sírios. Espera aprovação do Congresso para destinar uma verba maior para este fim."""},
     {'titulo': """Joaquim Barbosa telefonou para Francisco Falcão""",
      'corpo': """Ex-presidente do Supremo Tribunal Federal, o jurista Joaquim Barbosa telefonou da Argentina para cumprimentar o ministro Francisco Falcão por ter assumido a presidência do Superior Tribunal de Justiça. Barbosa estava na Argentina em férias quando Falcão assumiu o cargo. Os dois se aproximaram no período em que Barbosa se dividiu entre o Supremo e a presidência do Conselho Nacional de Justiça, do qual Francisco Falcão era o corregedor. Barbosa de seu sucessor na presidência do Supremo, Ricardo Lewandowski, que assumiu o cargo ontem."""},
      {'titulo': """Ela organiza uma manifestação pelo clima em São Paulo""",
       'corpo': """Marchas pelo clima agitarão várias grandes cidades do mundo no dia 21 de setembro. Estão previstas passeatas em Nova York, Paris, Londres, Nova Délhi, Berlim, Bogotá, Melbourne, Vancouver e no Rio de Janeiro. E por que não em São Paulo? Empolgada com a mobilização pelo clima dos cariocas e simpatizantes, a paulistana Graziela Massonetto decidiu tomar a iniciativa de organizar uma manifestação no mesmo dia em São Paulo. “Realizaremos um ato às 13h no vão livre do Masp”, diz Grazi. Ela é facilitadora em São Paulo da Avaaz, uma das entidades que promove o dia de marchas pelo mundo. Quem estiver interessado pode se inscrever como voluntário na Avaaz ou curtir no Facebook. Para descrever o evento, Grazi conversou conosco.
ÉPOCA: Por que marchar pelo clima no dia 21 de setembro?
Grazi Massonetto: A ideia de sair às ruas começou porque quase todos os presidentes e primeiros ministros do mundo irão para Nova York no dia 23/09 participar de uma conferência sobre o clima que promete entrar para história, tanto pelo número de participantes quanto pelo tamanho, além de ser organizada pelo líder da ONU, o Ban Ki-Moon. Aí membros de diversas ONGs, como a Avaaz, começaram a espalhar as vozes pelo mundo todo. O projeto é que no dia 21 de setembro, pessoas do mundo inteiro saiam às ruas pra pressionar atitudes concretas desses líderes e não apenas palavras. Queremos mostrar que nos importamos e que não queremos apenas promessas, queremos ver atitudes em relação às mudanças climáticas.
ÉPOCA: O que vocês farão em São Paulo?
Grazi: Aqui em SP, realizaremos um ato no dia 21/09, domingo, às 13h, no vão livre do MASP. Iremos nos reunir com cartazes, apitos e muita disposição para chamar a atenção dos governantes e da mídia. Nós sabemos que durante décadas, esforços internacionais para combater as mudanças climáticas tem encontrado obstáculos em diversos países e dificilmente são colocados em prática. Mas esta Cúpula do Clima da ONU é diferente. Ela tem como objetivo destravar esse processo, reunindo líderes mundiais para focar no que é necessário para assegurar um novo tratado climático global em dezembro de 2015, quando o acordo existente vence.
ÉPOCA: Por que mobilizar as pessoas neste momento?
Grazi: Até hoje, nenhum movimento que pedia mudanças sociais venceu sem estar presente em massa nos momentos chave na história, sem ir às ruas, sem ocupar espaço e mostrar as caras. A mobilização global pelo clima é o nosso momento de ocupar as ruas e recuperar o mundo que sabemos ser possível. Pesquisas de opinião no mundo inteiro mostram que as pessoas se importam com o assunto, mas, onde estão os protestos? Esta mobilização é a nossa resposta! Vai depender de cada um de nós levar às ruas o empurrão que os líderes mundiais precisam para salvar o planeta. Todos nós queremos ar limpo, comunidades saudáveis,  uma economia que beneficia as pessoas e o planeta, um mundo protegido de desastres causados pelas mudanças climáticas.
ÉPOCA: Grazi, como mobilizar as pessoas pelo clima justo agora que as temperaturas estão temporariamente num platô?
Grazi: A gente não precisa ir longe pra ver como as mudanças climáticas afetam nossa vida. No ano passado, por exemplo, tivemos o verão mais quente da história de São Paulo. A nossa umidade relativa do ar também anda baixa. Ou seja, sentimos no dia-a-dia o peso das mudanças climáticas. Isso sem contar os estragos para a fauna e flora local. As pessoas têm que entender que falar sobre mudanças climáticas é falar sobre um todo: é sobre a vida humana também e não apenas sobre plantas e animais.
ÉPOCA: Segundo os cientistas, as mudanças climáticas existem. Mas a falta de chuvas e a secura que atingem São Paulo nos últimos meses fazem parte de um fenomeno separado. As mudanças climáticas devem ter efeito contrário: elas trazem mais chuvas. Como explicar à população que deve se preocupar com o aquecimento global se a seca atual não tem nada a ver com ele?
Grazi: Fenômenos separados existem, claro. Mas fazem parte de um todo. O importante é ter essa consciência de que não vai ser do dia pra noite que sentiremos tão forte as mudanças climáticas. Seus efeitos se mostram em um prazo maior. Por isso é importante ações que parem o aquecimento. É uma medida aqui e agora que será vista daqui há alguns anos. Se quisermos continuar vivendo aqui, precisamos tomá-las. O que devemos explicar à população é que as mudanças climáticas existem e podem interferir em nossas vidas mais rápido do que imaginamos. Elas não fazem apenas o mundo ficar um pouco mais quente, ou criam algumas tempestades a mais. Os cientistas estão nos alertando há tempos que o clima do mundo é resultado de um equilíbrio muito, mas muito, delicado.
ÉPOCA: Diante de outros problemas cotidianos, como o trânsito ou a segurança, por que priorizar a questão do clima?
Grazi: O aquecimento global está prejudicando esse equilíbrio e, se não agirmos, provavelmente alcançará resultados nos quais mudanças climáticas aceleradas escapam do nosso controle e devastam a vida humana como a conhecemos. Exatamente por isso: porque se quisermos manter um equilíbrio, devemos pensar em como retardar ou mesmo parar o aquecimento global. Se pensarmos apenas no trânsito estaremos esquecendo que para continuarmos existindo, para continuarmos tendo a vida que temos hoje na Terra, é preciso pensar além. Se a Terra continuar esquentando, não sabemos ao certo as consequências desastrosas que teremos para a vida humana. Ou seja: de que adianta dar mais prioridade ao trânsito se daqui há alguns anos poderemos nem mais estar aqui pra contar história? Qual é o mundo que queremos deixar pras próximas gerações? Devemos nos questionar: o que é mais importante? pensar apenas no trânsito de São Paulo ou levar em conta que eu vivo num ecossistema gigante que está sufocando? E mais. Se pensarmos em medidas efetivas no trânsito, como redução de gás carbônico, também estaremos ajudando o clima. Uma coisa está interligada à outra. por isso devemos pensar como um todo e não apenas no trânsito ou na mudança climática. Até porque, são assuntos que andam de mãos dadas.
"""},
    {'titulo': """VENDAS DO VAREJO RECUAM 1,1% EM JULHO""",
     'corpo': """As vendas do comércio varejista restrito caíram 1,1% em julho ante junho, na série com ajuste sazonal, informou o Instituto Brasileiro de Geografia e Estatística (IBGE). A queda é a mais intensa desde outubro de 2008, quando também registrou recuo de 1,1% na margem.

Já na comparação com julho do ano passado, sem ajuste sazonal, as vendas do varejo tiveram queda de 0,9% em julho deste ano. Até julho, as vendas do varejo restrito acumulam altas de 3,5% no ano e de 4,3% nos últimos 12 meses.

Quanto ao varejo ampliado, que inclui as atividades de material de construção e de veículos, as vendas subiram 0,8% em julho ante junho, na série com ajuste sazonal.

Na comparação com julho do ano passado, sem ajuste, as vendas do varejo ampliado tiveram queda de 4,9% em julho deste ano. Até julho, as vendas do comércio varejista ampliado acumulam queda de 0,6% no ano, mas alta de 1,1% nos últimos 12 meses.

+ O varejo brasileiro desacelera. E agora?

O índice de média móvel trimestral do varejo restrito caiu 0,6% no trimestre encerrado em julho. No varejo ampliado, que inclui as atividades de veículos e material de construção, o índice de média móvel trimestral das vendas caiu 1,1% no trimestre encerrado em julho.

O IBGE revisou as vendas do varejo restrito em maio ante abril, de +0,3% para estabilidade (0,0%). Em abril ante março, o resultado também foi revisado, de -0,3% para -0,4%. No varejo ampliado, foi revisado o resultado de junho ante maio, de -3,6% para -3,4%. Segundo o IBGE, a queda nas vendas de veículos neste período, apurada inicialmente em -12,9%, passou a uma taxa de -7,0%.

Segmentos
A queda de 1,1% nas vendas do varejo em julho ante junho foi puxada por resultados ruins em quatro dos oito segmentos pesquisados, de acordo com o IBGE. O recuo mais intenso foi observado em móveis e eletrodomésticos, que tiveram vendas 4,1% menores. Trata-se da segunda taxa negativa consecutiva, já que o setor havia registrado retração de 4,0% nas vendas em junho ante maio.

Além disso, o setor de hiper, supermercados, produtos alimentícios, bebidas e fumo teve retração de 1,3% nas vendas em julho ante julho. O setor vinha de uma alta de 0,5% na comparação de junho com maio.

Também tiveram retração nas vendas em julho ante junho os setores de tecidos, vestuário e calçados (-0,1%) e outros artigos de uso pessoal e doméstico (-0,4%), ambos a segunda taxa negativa seguida.

No sentido contrário, registraram alta nas vendas em julho ante junho combustíveis e lubrificantes (+0,8%), equipamentos e materiais para escritório, informática e comunicação (+0,9%) e livros, jornais, revistas e papelaria (+2,1%), todos se recuperando de um recuo no mês anterior.

O segmento de artigos farmacêuticos, médicos, ortopédicos e de perfumaria apresentou estabilidade nas vendas na passagem de junho para julho.

Veículos
As vendas de veículos, motos, partes e peças subiram 4,3% em julho ante junho. O resultado recupera parte das perdas observadas em junho ante maio, quando as vendas recuaram 7,0%. Apesar da alta mensal, a venda de veículos recuou 12,5% na comparação com julho do ano passado. O setor acumula quedas de 8,6% no ano e de 5,2% em 12 meses.

"Com dois meses de retração nas vendas, as concessionárias colocaram os estoques em promoção na tentativa de esvaziar o pátio", explicou Juliana Vasconcellos, gerente da Coordenação de Serviços e Comércio do IBGE. O mesmo ocorreu com os materiais de construção, que por sua vez vinham de uma sequência de quatro quedas na margem. Em julho, o volume das vendas aumentaram 3,8%.

Apesar disso, ambos os segmentos seguem com desempenho abaixo de 2013, de acordo com o IBGE. No caso de veículos, houve recuo de 12,5% na comparação com julho do ano passado, enquanto para materiais de construção, as vendas diminuíram 3,2% nesta base."""},
    {'titulo': """MODELO DE OUTLET ATRAI O MERCADO DE LUXO""",
     'corpo': """Sucesso no mercado americano, os outlets já foram sinônimo de mico no Brasil. Na década de 1990, quem tentou desenvolver esse tipo de empreendimento por aqui teve de voltar atrás e abrir um shopping convencional para não quebrar: nem lojistas, nem consumidores estavam preparados para o novo modelo.

Mas o vento virou para quem quer investir em outlets e uma das provas de que os tempos parecem mais promissores neste segmento está na lista de interessados em ter um shopping de descontos: até quem fez sua reputação no mercado de luxo, passando longe das pechinchas e de grandes liquidações, decidiu entrar no negócio de outlets.

Primeiro, em 2011, a JHSF, incorporadora que desenvolveu o complexo Cidade Jardim em São Paulo, anunciou um projeto na rodovia Castelo Branco, próximo à cidade de São Roque. Com previsão de inauguração em julho deste ano, o Catarina Fashion Outlet Shopping integra um projeto maior que reúne no mesmo lugar um centro comercial, hotel, salas de escritórios, residências e até um aeroporto. A incorporadora não tem outros empreendimentos em desenvolvimento.

Agora, quem está investindo pesado em outlets é seu arquirrival Iguatemi. O grupo, da família Jereissati, chegou depois, mas com apetite para crescer rápido e demarcar território. Em abril do ano passado, a empresa, dona do primeiro shopping do país - o Iguatemi São Paulo - anunciou sua primeira investida no segmento ao comprar uma participação de 41% no Shopping Platinum Outlet, em Novo Hamburgo (RS), por R$ 46,2 milhões. O empreendimento, da construtora São José, foi inaugurado em setembro do ano passado.

O Iguatemi já está desenvolvendo outros dois outlets, um na região metropolitana de Belo Horizonte, em Nova Lima, e outro na cidade de Tijucas, perto de Florianópolis. "Trabalhamos com a ideia de ter entre cinco e sete empreendimentos. A ideia é ficar dentro desse universo", diz Carlos Jereissati, presidente do Grupo Iguatemi. Os outlets serão feitos, sempre, em sociedade e terão uma marca própria, a partir do ano que vem, com a inauguração do projeto de Tijucas. Seguindo a mesma linha dos shoppings do Iguatemi, os centros de desconto serão batizados de iFashion Outlet.

Nos próximos três anos, é provável que o grupo inaugure mais shoppings de descontos do que convencionais. Mas Jereissati sabe que esse é um mercado limitado. "Esse segmento deve representar, ao fim dos investimentos, cerca de 10% da nossa receita", afirma o empresário. "Não é um mercado grande, mas tem muita oportunidade. Percebemos que nossos lojistas, que são premium, querem um operador com o mesmo profissionalismo em outlet."

No mercado, os principais competidores estão acompanhando de perto o movimento do Iguatemi. O valor do investimento, por exemplo, já foi colocado em xeque. Em média, o grupo vai desembolsar cerca de R$ 140 milhões para fazer um outlet - metade do que costuma gastar num shopping convencional. "Ainda assim, o valor parece muito alto para um projeto que se baseia em custos mais baixos para sustentar os descontos", diz André Costa, sócio da consultoria About, que assessora desenvolvedores de outlets. Um dos projetos que está sob sua gestão é o de um shopping de descontos em Santa Catarina, numa cidade vizinha a Tijucas, que vai custar R$ 80 milhões. O Iguatemi diz que seus outlets custam mais porque se enquadram na categoria premium.

Mercado
Se todos os outlets já anunciados saírem do papel na data prevista, o país terá ao menos 14 empreendimentos desse tipo até o fim do ano que vem, segundo levantamento da About. O avanço do varejo brasileiro é o pano de fundo desse crescimento. A abertura de novas lojas fez com que os fabricantes precisassem de outro meio para vender as sobras, já que os bazares temporários e as liquidações nas lojas próprias tornaram-se insuficientes em alguns casos.

Como esse ainda é um negócio novo e a receita de sucesso americana não pode ser aplicada na íntegra por aqui, algumas perguntas sobre o mercado de outlets seguem sem respostas. Quantos empreendimentos desse tipo o País é capaz de suportar é uma delas. Executivos de grandes empresas de shoppings divergem sobre esse número, que varia de 15 a 30.

"O outlet é um canal de escoamento de mercadoria. Não posso abrir outlet por vontade própria se não houver demanda do lojista", diz Alexandre Dias, diretor de relações com o varejo da General Shopping - pioneira dos outlets no Brasil. Ela foi a primeira a inaugurar um empreendimento do tipo, em 2009, às margens da Rodovia dos Bandeirantes. A meta da General Shopping é ter sete outlets até 2015."""},
    {'titulo': """Comandante da UPP Nova Brasília morre após tiroteio no Rio""",
     'corpo': """O comandante da UPP Nova Brasília, capitão Uanderson Manoel da Silva, 34 anos, morreu no início da noite desta quinta-feira (11), após ser alvejado com um tiro no peito. Segundo a polícia, o oficial participou de uma troca de tiros com suspeitos de pertencer ao tráfico de drogas no Conjunto de Favelas do Alemão, Zona Norte do Rio.
O comandante chegou a passar por uma cirurgia no tórax, mas morreu por volta das 19h30. De acordo com a Coordenadoria de Polícia Pacificadora, ele chegou a ser levado para a Unidade de Pronto Atendimento do Alemão e encaminhado para o Hospital Getúlio Vargas. O tiroteio ocorreu por volta das 17h30.
Segundo a PM, o capitão Uanderson estava há 11 anos na Polícia Militar. Ele trabalhou no 14º BPM (Bangu), 15º BPM (Caxias) e 41º BPM (Irajá). O oficial comandava a UPP Nova Brasília há 3 meses. O PM era casado e tinha uma filha. Ainda não há informações sobre o enterro do policial.
Às 19h42, segundo a polícia, homens do Batalhão de Operações Especiais da PM (Bope), permaneciam na região buscando os criminosos que mataram o oficial.
Mais cedo, um tiroteiro assustou os moradores da comunidade. Por volta das 14h30, policiais do Grupamento Tático de Polícia de Proximidade (GTPP) da Unidade de Polícia Pacificadora (UPP) Nova Brasília, no Complexo do Alemão, estavam em patrulhamento pela localidade conhecida como Campo do Seu Zé quando encontraram com homens armados que atiraram contra os agentes, segundo a polícia. Os PMs revidaram e os bandidos fugiram deixando para trás 58 papelotes de cocaína, 10 pedras de crack e duas motos.
Pouco depois, ainda segundo a polícia, militares da UPP Fazendinha encontraram Raian Dias da Rocha, de 20 anos, que foi atendido na Unidade de Pronto Atendimento com ferimentos causados por disparos de arma de fogo. Raian foi transferido para o Hospital Municipal Souza Aguiar, no Centro. O caso foi registrado na 45ª DP (Alemão).

'Tentativas como essa não nos intimidam'
Em nota, o governador Luiz Fernando Pezão lamentou a morte do comandante “que morreu trabalhando em defesa da paz dos moradores do Alemão” e destacou “não vamos permitir que bandidos tirem covardemente a vida dos nossos policiais”.
O governador reiterou que o estado não vai retroceder na política de pacificação e enfatizou o processo de instalação de UPPs no Conjunto de Favelas da Maré. “Amanhã (sexta-feira), vamos formalizar a permanência do Exército na Maré para darmos continuidade ao processo de pacificação na comunidade com a futura instalação de UPPs. As unidades têm sofrido ataques de marginais que tentam a todo custo desmoralizar o programa. Tentativas como essa não nos intimidam”, afirmou.
Inauguração
A UPP Nova Brasília, que fica no Conjunto de Favelas do Alemão, foi inaugurada em abril de 2012 juntamente com a UPP Fazendinha. Quando foi aberta a  UPP Nova Brasília contava com 340 policiais para atender 25 mil pessoas a partir da estação Itararé do teleférico. A unidade é responsável pela segurança das localidades Ipê Itararé, Mourão Filho, Largo Gamboa, Cabão, Joaquim de Queiroz, Loteamento, Prédios, Aterro I e Aterro II.
Entregas restritas
Como mostrou o G1 no dia 31, o medo da violência está levando varejistas a restringir entregas no Complexo do Alemão, apesar da região abrigar bases de uma Unidade de Polícia Pacificadora. Na ocasião, duas das maiores redes varejistas confirmaram ao G1 que as entregas em determinados locais da região estão restritas em função da criminalidade e destacaram que a lista de endereços considerados perigosos é flutuante, ou seja, varia conforme o registro de atos violentos."""},
    {'titulo': """PM é baleado durante ataque à UPP da Cidade de Deus, no Rio""",
     'corpo': """Um policial militar foi baleado por volta das 20h40 de domingo (25), durante um ataque à base da Unidade de Polícia Pacificadora (UPP) da Cidade de Deus, na Zona Oeste do Rio de Janeiro, informou a Polícia Militar (PM).
Segundo a PM, dois homens em uma moto passaram atirando contra a entrada principal do prédio onde fica a UPP. Uma porta de vidro ficou quebrada, e o muro foi atingido por pelo menos um disparo. Os dois suspeitos conseguiram fugir.
O policial ferido foi socorrido por outros PMs e levado a uma Unidade de Pronto Atendimento (UPA) da região. Posteriormente, o soldado foi transferido para o Hospital Lourenço Jorge, na Barra da Tijuca, também na Zona Oeste.
     De acordo com a PM, o policiamento foi reforçado na Cidade de Deus. A polícia informou ainda que tem pistas de um dos agressores e faz buscas para prender o suspeito."""},
    {'titulo': """PM faz operação na Cidade de Deus para achar suspeitos de atacar UPP """,
     'corpo': """A Polícia Militar fazia uma operação na Cidade de Deus, Zona Oeste do Rio, na manhã desta segunda-feira (26). Como mostrou o Bom Dia Rio, imagens do Globocop registraram a ação dos PMs, que revistavam pedestres e faziam buscas na comunidade, por volta das 7h, na tentativa de localizar os criminosos que passaram atirando contra a entrada principal da Unidade de Polícia Pacificadora (UPP), na noite de domingo (25), e deixaram um PM ferido. Vinícius Salles Oliveira de Sá, 28 anos, foi baleado por volta das 20h40 e socorrido na Unidade de Pronto Atendimento (UPA) da região.
PMs abordam pedestres nesta manhã  (Foto: Reprodução / TV Globo)
PMs abordam pedestres nesta manhã
(Foto: Reprodução / TV Globo)
De acordo com a PM, o policial baleado foi transferido, posteriormente, para o Hospital Lourenço Jorge, na Barra da Tijuca. Até as 7h desta segunda, Vinícius passava bem e não corria risco de morrer, segundo informações da Secretaria Municipal de Saúde.
A Polícia Militar identificou um dos suspeitos de participar do ataque. De acordo com o 18º BPM (Jacarepaguá), Luciano da Silva Teixeira, 31 anos, conhecido como "Sardinha", teria ligação com o tráfico de drogas na Cidade de Deus. Segundo o site de procurados da Polícia Civil, ele seria o homem de confiança do traficante Sam que chefiava o tráfico na comunidade e cumpre pena em um presídio federal.
saiba mais
PM é baleado durante ataque à UPP da Cidade de Deus
Policial é baleado em confronto na favela Nova Brasília, no Rio, diz CPP
Contêiner de UPP é incendiado no Lins, Zona Norte do Rio
Homem armado em UPA
Um homem armado entrou na Unidade de Pronto Atendimento (UPA) da Cidade de Deus à procura do PM ferido no ataque à UPP. Segundo o 18º BPM (Jacarepaguá), funcionários da UPA pediram ajuda à polícia depois que o suspeito entrou na unidade com um fuzil, por volta de 0h30.
Ainda de acordo com a PM, quatro homens em um carro teriam parado em frente à unidade hospitalar e um deles entrou no local para checar se o PM estava sendo socorrido. Funcionários do hospital disseram que o PM não estava lá e os suspeitos fugiram.
Ataque à UPP
      Segundo a PM, dois homens em uma moto passaram atirando contra a entrada principal do prédio onde fica a unidade na Estrada Marechal Miguel Salazar de Moraes. Uma das portas de vidro foi quebrada e um muro foi atingido por pelo menos um disparo."""},
    {'titulo': """Campos afirma que é preciso 'dar um salto de qualidade na política' e critica governo Dilma""",
     'corpo' : """O governador de Pernambuco e presidente nacional do PSB, Eduardo Campos, afirmou na madrugada desta terça (15) que é preciso “dar um salto na qualidade da política”, seguindo o mesmo tom crítico adotado pela ex-senadora Marina Silva em coletiva na tarde de segunda (14), onde ela afirmou que o retrocesso marca o governo Dilma Rousseff. “Se você não melhora o padrão da economia, não contém inflação, não melhora fundamentos macroeconômicos, não faz a sinergia entre as políticas fiscal e monetária, nós vamos fragilizar a conquista da estabilidade, e qualquer problema econômico que perdure, a gente sabe que aqueles que subiram degraus na ascensão social, eles podem descer de uma vez só”, disse o governador.

Campos falou com a imprensa após encontro que teve com a ex-senadora na casa dele, situada na Zona Norte do Recife. Durante à tarde, Marina Silva participou de seminário sobre sustentabilidade ambiental na Universidade de Pernambuco (UPE). Foi a primeira vez que ela veio a capital pernambucana após o anúncio da aliança Rede-PSB. O encontro com o governador teve início por volta das 22h de segunda e terminou na madrugada desta terça-feira (15).
saiba mais
•	Se Marina desistir, Dilma fica com maior parte de votos, diz Datafolha
•	Dilma soma 42%, Aécio, 21%, e Campos, 15%, informa Datafolha
O presidente nacional do PSB acrescentou que a aliança com a Rede discute um conjunto de ideias para superar o que eles chamam de “velha lógica da política”. “Sabemos que foi importante ter a democracia para construir as bases dos fundamentos da estabilização econômica que nos permitiu o ciclo de inclusão social. Agora, um ciclo para melhorar os serviços públicos, a qualidade de vida e a produtividade vai exigir um outro padrão de serviço público, e esse outro padrão só vai vir com novo padrão da política. Nós temos agora que ter coragem de dar um salto na qualidade da política e depois disso uma visão estratégica para as próximas décadas", comentou.

Campos ainda afirmou ser "acertada" e "coerente" a decisão da Rede de não aceitar cargos na executiva socialista. "Cabia a nós fazer convite, mas no momento que evoluímos no diálogo, que somos uma aliança em torno de um programa de dois partidos, decidimos, de forma coerente, ter a direção da Rede e do PSB, que vão tratar juntas e tirar os consensos necessários".

'Encontro de família'
Líder do PSB na Câmara, o deputado Beto Albuquerque participou do jantar que classificou como "encontro de família" e destacou a série de seminários que a coligação vai fazer a partir do próximo dia 29, começando por São Paulo.

"Falamos mais no final sobre os seminários que vamos fazer para ouvir as pessoas que não estão engajadas formalmente na Rede e no PSB. Cada um [partido] vai expor o que pensa e depois abrir canais para juntar, tem uma massa muito grande da sociedade querendo ser ouvida. Temos que aprender a coexistir [Rede e PSB], conviver com eventuais divergências, isso é a virtude da nossa união, ninguém impor nada a ninguém, e só vamos conversar com os outros depois que a orquestra estiver afinada", argumentou. """},

]
