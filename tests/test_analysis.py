# -*- coding: utf-8 -*-

import unittest
from resumeai import analysis
from resumeai import extraction


class BaseAnalysedTextTest(unittest.TestCase):
    def test_update_entities_dict(self):
        entities = {extraction.NamedEntity('Entidade Coisada'): 1}

        new_entities = {extraction.NamedEntity('Entidade Coisada'): 1,
                        extraction.NamedEntity('Outra Coisada'): 1}

        expected = {extraction.NamedEntity('Entidade Coisada'): 2,
                    extraction.NamedEntity('Outra Coisada'): 1}

        returned = analysis.BaseAnalysedText()._update_entities_dict(
            entities, new_entities)

        self.assertEqual(returned, expected)


class AnalysedTextTest(unittest.TestCase):
    def setUp(self):
        raw_text = """
O ministro da Secretaria de Assuntos Estratégicos, Marcelo Neri,virou o queridinho da presidente Dilma Rousseff. Ganhou a chefe pela capacidade de analisar estatísticas e ter sempre prontas respostas sobre o país. Não há dúvida que ficará num eventual governo Dilma. Tem quem aposte até na Fazenda.
"""
        language = 'portuguese'
        self.analysed_text = analysis.AnalysedText(raw_text, language)
        self.expected_entities = [
            extraction.NamedEntity('Secretaria de Assuntos Estratégicos'),
            extraction.NamedEntity('Marcelo Neri'),
            extraction.NamedEntity('Dilma Rousseff')]

    def test_analysed_text_entities(self):
        for e in self.expected_entities:
            self.assertIn(e, self.analysed_text.entities, e)


class AnalysedParagraphTest(unittest.TestCase):
    def setUp(self):
        raw_text = """
Estou na Provença, na França, com minha família. Todos  à mesa de um restaurante que, segundo soube, tem duas estrelas no prestigiado Guia Michelin, a bíblia dos gourmets de todo o mundo. O nome, chiquérrimo: Oustau de Baumamière, em Les Beaux de Provence. A comida é servida em um pátio, sob uma árvore gigantesca. Diante do cenário, penso: “Como sou chique!”. É brega a gente pensar que é chique. Mas, naquele momento, me senti no topo do mundo, pronto a desfrutar um dos melhores jantares da Terra. Pedimos o menu degustação, composto de vários pratos que, em conjunto, dariam uma noção da culinária do restaurante a nós, ignorantes turistas em busca da alta gastronomia. Para começar, veio uma espuma de vegetal. Espuma é um item que saiu das banheiras para as mesas elegantes algum tempo atrás. Degustam-se pratos com espumas de todo tipo e, segundo os entendidos, é preciso apreciar a textura, não só o sabor do alimento. Minha sobrinha assumiu um ar sofisticado, eu também. Declaramos que a espuma era maravilhosa, embora não soubéssemos bem do que se tratava. Passou-se meia hora enquanto esperávamos pelo segundo prato. No intervalo, nos fartamos de pãezinhos com manteiga, esses sim muito bons. Falamos sobre o preço do cardápio, altíssimo. Mas tudo era válido pela comida boa. Chegou o segundo prato. Eram três tirinhas de salmão, mais finas que meu dedo mindinho. Degustamos lentamente, qual a alternativa? Durante mais 45 minutos, comemos pão com manteiga. Veio o terceiro prato, frango. Quer dizer, um quadradinho de carne de uns 2 centímetros, no máximo. Senti um sabor familiar. Todos nos olhamos, com a mesma sensação surpresa. Finalmente, o marido de minha sobrinha desabafou:

– É frango de panela! Igualzinho o que vovó fazia.

– Gastamos essa fortuna para comer frango de panela? Ainda mais essa miséria? – perguntou minha prima.


A partir do ano que vem, os restaurantes do eixo Rio-São Paulo também serão classificados pelo Michelin. Muita gente se deslumbrará pela chance de comer num estrelado. Exatamente como eu, antes de começar a viagem.

"""

        self.text = analysis.AnalysedText(raw_text, 'portuguese')
        self.paragraph = self.text.paragraphs[3]

    def test_paragraph_score(self):
        self.assertIsNotNone(self.paragraph.similarity_score)
        self.assertIsNotNone(self.paragraph.entities_score)
        self.assertEqual(self.paragraph.score,
                         self.paragraph.similarity_score +
                         self.paragraph.entities_score)


class AnalysedSentenceTest(unittest.TestCase):
    def setUp(self):
        txt = """
1 - Vamos fazer algo.
2) Algo bem legal."""

        self.text = analysis.AnalysedText(txt, 'portuguese')

    def test_sentenca_numerada(self):
        self.assertEqual(self.text.paragraphs[0].sentences[0].raw_text,
                         'Vamos fazer algo.')

    def test_sentenca_pequena(self):
        self.assertFalse(self.text.paragraphs[0].sentences[0].entities_score)
