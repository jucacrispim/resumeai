# -*- coding: utf-8 -*-

import unittest
import nltk
from resumeai import decomposition


class BaseDecomposedTextTest(unittest.TestCase):

    def setUp(self):
        raw_text = 'Um texto curto!'
        self.decomposed_text = decomposition.BaseDecomposedText(raw_text)

    def test_len(self):
        self.assertEqual(len(self.decomposed_text), 15)

    def test_eq(self):
        text = 'Um texto curto!'
        other = decomposition.BaseDecomposedText('Outro texto')
        self.assertEqual(text, self.decomposed_text)
        self.assertNotEqual(self.decomposed_text, other)

    def test_qt(self):
        text = 'Zulmira foi pra lá'
        other = decomposition.BaseDecomposedText(text)
        self.assertGreater(text, self.decomposed_text)
        self.assertGreater(other, self.decomposed_text)

    def test_lt(self):
        text = 'A coisa tá feia'
        other = decomposition.BaseDecomposedText(text)
        self.assertLess(text, self.decomposed_text)
        self.assertLess(other, self.decomposed_text)


class DecomposedTextTest(unittest.TestCase):
    def setUp(self):
        raw_text = "Um texto curto.\nÉ só pra testar a parada. Rapidinho, viu?"
        raw_text += '\nNem tenho saco pra isso!'

        language = 'portuguese'
        self.decomposed_text = decomposition.DecomposedText(raw_text, language)

    def test_paragraphs(self):
        self.assertEqual(len(self.decomposed_text.paragraphs), 3)


class DecomposedParagraphTest(unittest.TestCase):
    def setUp(self):
        raw_text = 'Sou um parágrafo. '
        raw_text += 'Não faço sentido, mas quem se importa? Eu não.'
        stopwords = []

        self.decomposed_paragraph = decomposition.DecomposedParagraph(
            raw_text, stopwords)

    def test_sentences(self):
        self.assertEqual(len(self.decomposed_paragraph.sentences), 3)


class DecomposedSentenceTest(unittest.TestCase):
    def setUp(self):
        raw_text = 'Sou uma frase legal cheia de palavras bonitas.'
        stopwords = nltk.corpus.stopwords.words('portuguese')
        self.decomposed_sentence = decomposition.DecomposedSentence(
            raw_text, stopwords)

    def test_important_words(self):
        important_words = set(['fras', 'legal', 'chei', 'palavr', 'bonit'])
        self.assertEqual(self.decomposed_sentence.important_words,
                         important_words)
