# -*- coding: utf-8 -*-

import unittest
import nltk
from resumeai import extraction
from resumeai import decomposition


class NamedEntityTest(unittest.TestCase):
    def setUp(self):
        self.entity = extraction.NamedEntity('Dilma Rousseff')
        self.extense_entity = extraction.NamedEntity('Paulo Roberto Costa')
        self.more_extense_entity = extraction.NamedEntity(
            'Pedro Alcântara de Orleans e Bragança')

    def test_variations(self):
        variations = ['Dilma', 'Rousseff']
        self.assertEqual(self.entity.variations, variations)

    def test_extense_variations(self):
        variations = ['Paulo', 'Roberto', 'Costa', 'Paulo Roberto',
                      'Roberto Costa']
        self.assertEqual(self.extense_entity.variations, variations)

    def test_equal_string(self):
        self.assertEqual(self.entity, 'Dilma')
        self.assertEqual(self.entity, 'Dilma Rousseff')
        self.assertEqual(self.entity, 'Rousseff')
        self.assertNotEqual(self.entity, 'Lula')

    def test_equal_entity(self):
        entity = extraction.NamedEntity('Dilma Rousseff')
        other_entity = extraction.NamedEntity('Barack Obama')

        self.assertEqual(self.entity, entity)
        self.assertNotEqual(self.entity, other_entity)

    def test_equal_other_thing(self):
        self.assertNotEqual(self.entity, 2)

    def test_lower(self):
        self.assertEqual('dilma rousseff', self.entity.lower())

    def test_isupper(self):
        self.assertTrue(extraction.NamedEntity('AGORA SIM').isupper())
        self.assertFalse(extraction.NamedEntity('Agora Não').isupper())

    def test_lt(self):
        lesser = extraction.NamedEntity('Aécio Never')
        notlesser = 'Marina Silva'
        self.assertTrue(lesser < self.entity)
        self.assertTrue(self.entity < notlesser)
        with self.assertRaises(TypeError):
            self.entity < 0

    def test_gt(self):
        greater = extraction.NamedEntity('Marina Silva')
        notgreater = 'Aécio Never'

        self.assertTrue(greater > self.entity)
        self.assertTrue(self.entity > notgreater)
        with self.assertRaises(TypeError):
            self.entity > {}

    def test_last_word(self):
        entity = extraction.NamedEntity('Marina Silva')
        self.assertEqual(entity.last_word, 'Silva')

    def test_first_word(self):
        entity = extraction.NamedEntity('Marina Silva')
        self.assertEqual(entity.first_word, 'Marina')


class NamedEntityExtractorTest(unittest.TestCase):
    def setUp(self):
        stopwords = nltk.corpus.stopwords.words('portuguese')
        self.extractor = extraction.NamedEntityExtractor(stopwords)

    def test_extract_sentence_entities(self):
        raw_text = 'Hoje a presidente Dilma se reuniu em Brasília '
        raw_text += 'com ministros para tratar das denúncias envolvendo '
        raw_text += 'a Petrobrás'
        entities = [extraction.NamedEntity('Dilma Rousseff'),
                    extraction.NamedEntity('Brasília'),
                    extraction.NamedEntity('Petrobrás')]

        sentence = decomposition.DecomposedSentence(
            raw_text, stopwords=self.extractor.stopwords)

        rentities = self.extractor.extract_sentence_entities(sentence.words)
        for e in entities:
            self.assertIn(e, rentities)

    def test_extract_sentence_entities_extended(self):
        # testa se acha coisas compostas por
        # subst + prep + subst

        raw_text = 'Candidato ao governo de São Paulo, Geraldo Alckmin '
        raw_text += 'comparece à ato em '
        raw_text += 'Ferraz de Vasconcelos, na zona leste de São Paulo. '
        raw_text += 'próximo à Cidade de Deus.'
        entities = [extraction.NamedEntity('Geraldo Alckmin'),
                    extraction.NamedEntity('Ferraz de Vasconcelos'),
                    extraction.NamedEntity('São Paulo'),
                    extraction.NamedEntity('Cidade de Deus')]
        sentence = decomposition.DecomposedSentence(
            raw_text, stopwords=self.extractor.stopwords)

        rentities = self.extractor.extract_sentence_entities(sentence.words)
        rentities = [e.entity for e in rentities]

        for e in entities:
            self.assertTrue(e.entity in rentities, e)

    def test_extract_entities(self):
        txt = """
Com duras críticas à política econômica do governo Dilma Rousseff (PT), a presidenciável Marina Silva (PSB) criticou a petista neste domingo (14) ao dizer que não será necessário à concorrente mudar o ministro porque o resultado das eleições cuidará disso.



"O atual governo tem responsabilidade pela contabilidade criativa e pela administração dos preços para controlar a inflação. A Dilma disse que está resolvendo isso e até já se comprometeu a demitir seu atual ministro da Fazenda. Só que obviamente agora é tarde, porque ambos serão demitidos pelo povo brasileiro", disse a candidata do PSB.
"""
        entities = [extraction.NamedEntity("Dilma Rousseff"),
                    extraction.NamedEntity("Marina Silva"),
                    extraction.NamedEntity('PT'),
                    extraction.NamedEntity('PSB')]
        text = decomposition.DecomposedText(txt, language='portuguese')
        rentities = self.extractor.extract_entities(text)
        for e in entities:
            self.assertTrue(e in rentities)

    def test_correct_entities_mac_morpho(self):
        entity = extraction.NamedEntity('Carlos Castelo Branco à sua')
        scored = {entity: 1}
        returned = self.extractor._correct_entities_mac_morpho(scored)
        entity = [e for e in returned.keys()][0]

        self.assertEqual(entity, 'Carlos Castelo Branco')

    def test_extract_entities_from_chunked_text(self):
        t = nltk.tree.Tree('S', ['Uma', 'frase', 'qualquer', 'falando'
                                 'da', 'Marina' 'Silva'])
        entity = self.extractor._extract_entities_from_chunked_text(t)
        self.assertFalse(entity)
