# -*- coding: utf-8 -*-

import unittest
from resumeai import summarization, analysis


class SummarizedTextTest(unittest.TestCase):
    def setUp(self):
        # longo1 é um texto maior que o tamanho máximo mas com um
        # só parágrafo
        self.texto_longo1 = """
Depois de 10 anos longe do Brasil, a atriz portuguesa Maria João Bastos fez as malas e desembarcou no Rio de Janeiro para se envolver num triângulo amoroso na novela Boogie Oogie. Como Diana, ela vai bagunçar a vida de Paulo (Caco Ciocler) e de seu ex-namorado Angelo (o também lusitano Marco Delgado). Vale lembrar que a atriz fez sua última participação na Rede Globo no Sítio do Picapau Amarelo, em 2004 e, nos anos anteriores trabalhou em Sabor da Paixão e O Clone. "Fiz três projetos com o Rui (Vilhena, o autor da trama) em Portugal e ele me chamou. Vim sem pensar duas vezes. Fico muito feliz em voltar para a TV Globo e mais feliz ainda de participar da primeira novela do Rui. Me sinto em casa e digo que tenho uma família por aqui", diz ela, que nunca cortou os laços com o país. "Sempre me paravam nas ruas perguntando quando eu iria voltar. Ficava surpresa com isso e sinto um carinho muito grande. Sabia que voltaria, mas não pensei que fosse tão rápido", afirma Maria, que acabou de participar do festival de San Sebastian, na Espanha, esta semana, com filme The Casanova Variations (Variações de Casanova), ao lado do ator John Malcovich. "Ele é brilhante, parceiro de cena e quase que você se esquece que está com John", diz Maria, confessando que adoraria ser dirigida no teatro por Aderbal Freire Filho. Fica a dica"""

        # longo2 é um texto maior que o tamanho máximo e com
        # mais de um parágrafo.
        self.texto_longo2 = """
        "Teoria da Branca de Neve: por que só ter um se eu posso ter sete?". Foi com esse refrão que Mc Mayara conquistou a internet no último ano. Seus clipes no Youtube têm, em média, 1 milhão de visualizações cada um. Mais do que pela musicalidade, a jovem de 23 anos prendeu atenção do público por características que a tornam única no funk. "Não tenho nada de corpão. Fujo de todos os estereótipos", diz Mayara. Nascida em Curitiba, capital do Paraná, Mayara começou a carreira nas baladas do sul, até fazer a fama com vídeos caseiros na internet. É feminista, ativista do movimento LGBT, faixa preta em Taekwondo e mãe de uma menina de 3 meses. Mantém uma página divertida no Facebook, com mais de 300 mil seguidores. Entre as pérolas, estão suas doses de sabedoria quase diárias, as #teoriasdamcmayara: "A ilusão da vida começa quando seu notebook diz que você tem duas horas restantes de bateria e ele só fica ligado mais 30 segundos". A garota adora desafiar paradigmas para enfrentar o preconceito. No clipe "Ela sabe rebolar", Mayara dança grávida. As letras de duplo sentido e conteúdo sexual já lhe renderam até uma passagem pelo Ministério Público. Suspeitava-se de exploração sexual porque Mayara parecia menor de idade. Não era. Na entrevista abaixo diz que seu objetivo é aumentar a liberdade sexual das mulheres.
ÉPOCA - Como foi a ascensão de vídeos caseiros no YouTube à MC com um contrato?
Mayara - Comecei a frequentar baladas de eletrofunk quando tinha 17 anos. Foi lá que conheci outros MCs. Procurei sobre o gênero e produtores na internet e comecei a fazer minha própria música. Atrás de um CD distribuído num show de eletrofunk, tinha o contato de um empresário. Mandei vídeos para ele até que decidiu me contratar. O que chamou atenção em mim foi a estética: paranaense, baixinha, magrinha e sem corpão. Ele falou pra mim: "Você é uma típica menina do Paraná. Foge dos estereótipos do funk. Acho que pode dar certo". Tudo mudou. Eu fazia vídeos do meu celular, uma coisa bem caseira. Era tudo do meu jeito. A primeira vez que eu fui fazer um vídeo profissional fiquei paralisada: e agora, o que eu faço? Surgiu uma timidez que nunca tive.

ÉPOCA -  Uma de suas bandeiras é usar o funk para acabar com o preconceito, contra mulheres e homossexuais. Como pretende fazer isso?
Mayara - Quero mostrar para as pessoas que elas não precisam se esconder. A gente tem que fazer o que gosta. As pessoas deixam de expressar suas vontades porque levam a sério o que o outro pensa. Você acaba com o preconceito quando não liga para o que os outros falam.

ÉPOCA - Como começou o engajamento no movimento LGBT?
Mayara - Com a carreira musical. Vários grupos vieram me procurar por conta do conteúdo das letras e o tipo de mensagem que passo nas redes sociais. O público gay é extrovertido e muito querido. Mas minha luta é contra o preconceito de modo geral. O movimento LGBT faz parte. Formei uma união com alguns grupos ativistas. O que me importa é direitos iguais para todos. O mesmo para o feminismo.

"""

        # um texto menor que o tamanho mínimo
        self.texto_curto = """
A Estácio de Sá será a universidade oficial do Rock in Rio em 2015. Durante os sete dias de festival, 600 alunos trabalharão na captação de áudio e imagens, na transmissão de shows pelos telões do evento e na gravação de entrevistas com os músicos."""

        self.language = 'portuguese'

        self.decomposto_longo1 = analysis.AnalysedText(self.texto_longo1,
                                                       self.language)

        self.decomposto_longo2 = analysis.AnalysedText(self.texto_longo2,
                                                       self.language)

        self.decomposto_curto = analysis.AnalysedText(self.texto_curto,
                                                      self.language)

    def test_summarizedtext_com_texto_curto(self):
        summarized = summarization.SummarizedText(self.decomposto_curto,
                                                  4, 650, 450)
        self.assertEqual(summarized.summary,
                         summarized.original_text)

    def test_summarizedtext_com_texto_longo1(self):
        summarized = summarization.SummarizedText(self.decomposto_longo1,
                                                  6, 650, 450)
        self.assertNotEqual(summarized.summary,
                            summarized.original_text)
        self.assertTrue(summarized.entities)

    def test_summarizedtext_com_texto_longo2(self):
        summarized = summarization.SummarizedText(self.decomposto_longo2,
                                                  5, 650, 450)

        self.assertNotEqual(summarized.summary,
                            summarized.original_text)
        self.assertTrue(summarized.entities)


class SummarizerTest(unittest.TestCase):
    def test_summarizer(self):
        self.texto = """
A Estácio de Sá será a universidade oficial do Rock in Rio em 2015. Durante os sete dias de festival, 600 alunos trabalharão na captação de áudio e imagens, na transmissão de shows pelos telões do evento e na gravação de entrevistas com os músicos."""

        summarizer = summarization.Summarizer(self.texto)
        summary = summarizer.summarize()
        self.assertTrue(summary.entities)
        self.assertTrue(summary.summary)
