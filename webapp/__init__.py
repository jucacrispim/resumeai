# -*- coding: utf-8 -*-

import os

BASE_DIR = os.path.dirname(__file__)
DATA_DIR = os.path.join(BASE_DIR, 'data')

from mongoengine.connection import connect
from webapp import config

dbconf = config.DATABASE
conn = connect(**dbconf)
