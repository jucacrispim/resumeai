# -*- coding: utf-8 -*-

from datetime import datetime
from webapp.models import Conteudo, EntidadeNomeada


class ConteudoController(object):
    def __init__(self):
        self.model = Conteudo
        self.tamanho_lista = 20

    def get_lista(self, tipo, pagina=1):
        ini = (pagina - 1) * self.tamanho_lista
        end = pagina * self.tamanho_lista
        if tipo == 'ultimas':
            lista = self.model.objects.all()
        else:
            lista = self.model.objects.filter(entidades=tipo)

        lista = lista.order_by('-publicacao')
        return lista[ini:end]

    def get_lista_site(self, site, pagina=1):
        ini = (pagina - 1) * self.tamanho_lista
        end = pagina * self.tamanho_lista

        lista = self.model.objects.filter(site=site).order_by('-publicacao')
        return lista[ini:end]

    def cadastrar_conteudo(self, site, url, titulo, corpo, publicacao, imagem):
        conteudo = self.model(url=url, titulo=titulo, corpo=corpo,
                              publicacao=publicacao, site=site, imagem=imagem)
        conteudo.save()
        return conteudo

    def get_conteudo(self, url):
        conteudo = self.model.objects.get(url=url)
        return conteudo

    def get_trend_topics(self, hours=12, site=None, quantidade=10, smart=False):

        if smart:
            now = datetime.now()
            if now.hour >= 23 or now.hour < 6:
                base = 7
            elif now.hour == 22:
                base = 6
            else:
                base = 5

            if base < 7 and base > 5:
                base += ((10 / 60) * now.minute) / 600
            hours = base

        return self.model.trend_topics(hours, site, quantidade)


class EntidadeNomeadaController(object):
    def __init__(self):
        self.model = EntidadeNomeada

    def cadastrar_entidade_nomeada(self, nome_principal, *sinonimos):
        try:
            entidade = self.model.objects.get(nome_principal=nome_principal)
            entidade.sinonimos = sinonimos
        except self.model.DoesNotExist:
            entidade = self.model(nome_principal=nome_principal, sinonimos=sinonimos)

        entidade.save()
        return entidade

    def get_sinonimo(self, nome):
        return self.model.sinonimo(nome)
