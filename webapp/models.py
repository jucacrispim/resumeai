# -*- coding: utf-8 -*-

import os
import json
import pickle
import datetime
from mongoengine import Document
from mongoengine.fields import StringField, URLField, ListField, DateTimeField
from resumeai import summarize_text
import webapp


class Conteudo(Document):
    site = StringField(required=True)
    url = URLField(required=True, unique=True)
    titulo = StringField(required=True)
    imagem = URLField()
    corpo = StringField()
    resumo = StringField()
    publicacao = DateTimeField(required=True)
    entidades = ListField(StringField())

    def save(self, *args, **kwargs):
        if not self.resumo:
            self.resumir()
        super(Conteudo, self).save(*args, **kwargs)

    def resumir(self):
        resumo = summarize_text(self.corpo)
        self.resumo = resumo.summary
        self.entidades = set([EntidadeNomeada.sinonimo(e.entity)
                              for e in resumo.entities])
        self.entidades = [e for e in self.entidades if e]

    @property
    def permalink(self):
        return '/resumo/%s' % self.url

    def to_dict(self):
        cjson = self.to_json()
        return json.loads(cjson)

    @classmethod
    def trend_topics(cls, hours=12, site=None, quantidade=10):
        # db.conteudo.aggregate([
        #  {$match: {"publicacao": {"$gte": ISODate("2014-10-31T23:59:59Z")}}},
        #  {$unwind: "$entidades"},
        #  {$group: {"_id": "$entidades", count: {$sum: 1}}},
        #  {$sort: {"count": -1 }}
        # ])

        delta = datetime.timedelta(hours=hours)
        data = datetime.datetime.now() - delta
        conteudos = cls.objects.filter(publicacao__gte=data)
        conteudos = conteudos.order_by('-publicacao')
        if site:
            conteudos = conteudos.filter(site=site)
        entidades = conteudos.item_frequencies('entidades')

        notentities_file = os.path.join(webapp.DATA_DIR, 'nottrends.pickle')
        with open(notentities_file, 'rb') as f:
            blacklist = pickle.loads(f.read())

        new_entities = entidades.copy()
        for entidade in entidades:
            if entidade in blacklist:
                del new_entities[entidade]

        new_entities = [[v, k] for k, v in new_entities.items() if k]
        return sorted(new_entities, reverse=True)[:quantidade]


class EntidadeNomeada(Document):
    nome_principal = StringField(unique=True, required=True)
    sinonimos = ListField(StringField())


    @classmethod
    def sinonimo(cls, nome):
        """
        Retorna uma string com o sinônimo preferido
        para este nome. Se não houver sinônimo, retorna
        o próprio nome.
        """
        try:
            sinonimo = cls.objects.get(nome_principal=nome)
        except EntidadeNomeada.DoesNotExist:
            try:
                sinonimo = cls.objects.get(sinonimos=nome)
            except EntidadeNomeada.DoesNotExist:
                sinonimo = None

        if not sinonimo:
            return nome
        else:
            return sinonimo.nome_principal
