function postText(){
  var txt = jQuery('.txtarea').val();
  popin.showPleaseWait();
  jQuery.ajax({
    url: '/resumeai/api/',
    method: 'POST',
    data: {'txt': txt},
    success: function(res){
      popin.hidePleaseWait();
      var ftxt = '<p class="lead">Resumão</p>';
      ftxt += res.resumo + '<br/><br/>';
      ftxt += '<h3>Principais entidades</h3>';
      for (i=0; i < res.entidades.length; i++){
	ftxt += res.entidades[i] + '<br/>';
      }
      ftxt += '<br/><strong>Tempo gasto para resumir:</strong> ' + res.tempo + ' segundos.';
      jQuery('.resumeai-direita').html(ftxt);

    },
    error: function(res){
      popin.hidePleaseWait();
    }
  })
}

function add2notentities(word){
  jQuery.ajax({
    url: '/add2notentities',
    method: 'POST',
    data: {'word': word},
    success: function(res){
      console.log(res);
    }
  });
}

function rmnotentities(word){
  jQuery.ajax({
    url: '/rmnotentities',
    method: 'POST',
    data: {'word': word},
    success: function(res){
      console.log(res);
    }
  });
}

function innotentities(word){
  jQuery.ajax({
    url: '/innotentities',
    method: 'GET',
    data: {'word': word},
    success: function(res){
      console.log(res);
    }
  });
}

var popin = (function () {
  var pleaseWaitDiv = $('#pleaseWaitDialog');
  return {
    showPleaseWait: function() {
      pleaseWaitDiv.modal();
      jQuery('#pleaseWaitDiv').show()
    },
    hidePleaseWait: function () {
      jQuery('#pleaseWaitDiv').hide()
      pleaseWaitDiv.modal('hide');
    },

  };
})();

jQuery('#btn-resume').click(postText)
