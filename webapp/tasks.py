# -*- coding: utf-8 -*-

import os
from celery import Celery
from webapp import utils

celery = Celery('webapp.tasks')
celery.config_from_object('webapp.celeryconfig')


@celery.task
def atualizar():
    if os.path.exists('atualizar.lock'):
        with open('atualizar.lock', 'r') as f:
            pid = int(f.read().strip().strip('\n'))
            # os.kill(pid, 9)

    with open('atualizar.lock', 'w') as f:
        f.write(str(os.getpid()))

    try:
        utils.resumir_conteudos()
    except Exception as e:
        print('Erro atualizando conteúdos %s' % str(e))

    os.remove('atualizar.lock')

# @celery.task():
# def atualizar_outros
#     utils.get_content_viomundo()
