# -*- coding: utf-8 -*-

import os
import bs4
import requests
from resumeai import summarize_text
from datetime import datetime, timedelta
import webapp
from webapp.controllers import ConteudoController, EntidadeNomeadaController



URLS = [
    ['Marie Claire', 'http://revistamarieclaire.globo.com/dinamico/plantao/marie-claire/marie-claire/qtd/%s/1/'],
    ['Auto Esporte', 'http://revistaautoesporte.globo.com/dinamico/plantao/auto-esporte/auto-esporte/qtd/%s/1/'],
    ['Globo Rural', 'http://revistagloborural.globo.com/dinamico/plantao/globo-rural/globo-rural/qtd/%s/1/'],
    ['Quem Acontece', 'http://revistaquem.globo.com/dinamico/plantao/quem-acontece/quem-acontece/qtd/%s/1/'],
    ['Crescer', 'http://revistacrescer.globo.com/dinamico/plantao/crescer/crescer/qtd/%s/1/'],
    ['Época Negócios', 'http://epocanegocios.globo.com/dinamico/plantao/epoca-negocios/epoca-negocios/qtd/%s/1/'],
    ['Pequenas Empresas Grandes Negócios', 'http://revistapegn.globo.com/dinamico/plantao/pegn/pegn/qtd/%s/1/'],
    ['Casa Vogue', 'http://casavogue.globo.com/dinamico/plantao/casa-vogue/casa-vogue/qtd/%s/1/'],
    ['Glamour', 'http://revistaglamour.globo.com/dinamico/plantao/glamour/glamour/qtd/%s/1/'],
    ['GQ', 'http://gq.globo.com/dinamico/plantao/gq/gq/qtd/%s/1/'],
    ['Vogue', 'http://vogue.globo.com/dinamico/plantao/vogue/vogue/qtd/%s/1/'],
    ['Monet', 'http://revistamonet.globo.com/dinamico/plantao/monet/monet/qtd/%s/1/'],
    ['Casa e Jardim', 'http://revistacasaejardim.globo.com/dinamico/plantao/casa-e-jardim/casa-e-jardim/qtd/%s/1/'],
    ['Galileu', "http://revistagalileu.globo.com/dinamico/plantao/galileu/galileu/qtd/%s/1/"],
    ["Época", 'http://epoca.globo.com/dinamico/plantao/epoca/epoca/qtd/%s/1/',]]
#    'Meus 5 Minutos': 'http://meus5minutos.globo.com/dinamico/plantao/meus-5-minutos/meus-5-minutos/qtd/20/1/'}

# URLS = [
#     ['Vi o Mundo', 'http://www.viomundo.com.br/feed']
# ]


def get_content_viomundo():
    url = 'http://www.viomundo.com.br/feed'
    response = requests.get(url)
    response.connection.close()
    xml = bs4.BeautifulSoup(response.text)
    for item in xml.findAll('item'):
        d = {}
        d['revista'] = 'Vi o Mundo'
        d['titulo'] = item.find('title').get_text()
        d['url'] = item.find('link').get_text()
        if toscodb.get_resumo(d['url']):
            continue
        print('Cadastrando conteudo %s' % d['titulo'])
        publicacao = item.find('pubdate').get_text()
        d['publicacao'] = datetime.strptime(publicacao.split('+')[0].strip(),
                                            '%a, %d %b %Y %H:%M:%S')
        corpo = bs4.BeautifulSoup(item.find('content:encoded').get_text())
        corpo = corpo.get_text()

        sumario = summarize_text(corpo)
        d['resumo'], entidades = sumario.summary, sumario.entities
        entidades = [e.entity for e in entidades]
        d['entidades'] = entidades
        toscodb.save2mongo(d)

def resumir_conteudos(qtd_por_site=20):
    for revista, json in download_json(qtd_por_site):
        if not json:
            continue

        conteudos = json['conteudos']

        cadastrar_conteudos(revista, conteudos)
    return 'ok'


def download_json(qtd_por_site):
    for revista, url in URLS:
        print('** Atualizando dados do site %s **' % revista)

        url = url % qtd_por_site
        try:
            response = requests.get(url, timeout=10)
            print(' - Plantão downloaded')
        except:
            response = None

        if not response:
            print(' - Not response')
            yield None, None

        else:
            response.connection.close()
            try:
                myjson = response.json()
                print(' - Json ok')
            except:
                print(' - Json not ok')
                myjson = None

            yield revista, myjson


def cadastrar_conteudos(revista, conteudos):
    for conteudo in conteudos:
        url = conteudo['permalink']
        try:
            if ConteudoController().get_conteudo(url):
                continue
        except webapp.models.Conteudo.DoesNotExist:
            pass

        site = revista
        titulo = _limpar_corpo(conteudo['titulo'])
        url = url
        publicacao = datetime.strptime(conteudo['primeira_publicacao'],
                                       '%B %d, %Y %H:%M:%S')
        if 'thumb_gigigo' in conteudo.keys():
            imagem = conteudo.get('thumb_gigigo') or None
        else:
            imagem = conteudo.get('thumbnail') or None

        corpo = conteudo['corpo']
        if not corpo:
            continue
        corpo = bs4.BeautifulSoup(_limpar_corpo(corpo))
        corpo = corpo.get_text()
        if not corpo:
            continue

        print(' - Cadastrando conteudo "%s"' % titulo)

        conteudo = ConteudoController().cadastrar_conteudo(site, url, titulo,
                                                           corpo, publicacao, imagem)
        print(' - Conteúdo "%s" cadastrado com sucesso!' % titulo)

def _limpar_corpo(corpo_sujo):
    corpo = bs4.BeautifulSoup(corpo_sujo)
    corpo_raw = corpo.decode()

    for el in corpo.findAll('div', {'class': 'componente_materia'}):
        if str(el) == '\n':
            continue
        corpo_raw = corpo_raw.replace(str(el), '')

    if corpo.findAll('div', {'class': 'saibamais'}):
        for el in corpo.findAll('div', {'class': 'saibamais'}):
            if str(el) == '\n':
                continue
            corpo_raw = corpo_raw.replace(str(el), '')

    for el in corpo.findAll('label', {'class': 'foto-legenda'}):
        if str(el) == '\n':
            continue

        corpo_raw = corpo_raw.replace(str(el), '')

    corpo_raw = corpo_raw.replace('\r', '').replace('\t', '')
    return corpo_raw


def cadastrar_sinonimos():
    sinonimos_file = os.path.join(webapp.DATA_DIR, 'sinonimos.txt')
    with open(sinonimos_file, 'r') as f:
        sinonimos = f.readlines()

    sinonimos = [s.split(',') for s in sinonimos]
    for entidade in sinonimos:
        nome_principal = entidade[0]
        sins = entidade[1:]
        sins = [s.strip('\n').strip() for s in sins]
        EntidadeNomeadaController().cadastrar_entidade_nomeada(
            nome_principal, *sins)

def atualizar_resumos():
    controller = ConteudoController()
    agora = datetime.now()
    delta = timedelta(hours=12)
    data = agora - delta
    for conteudo in controller.model.objects.filter(publicacao__gte=data):
        print('atualizando conteúdo %s' % conteudo.titulo)
        conteudo.resumir()
        conteudo.save()

if __name__ == '__main__':
    import sys

    qtd_por_site = 20
    resumir = False
    # cadastrar sinonimos
    cadsin = False
    atualizar = False
    for a in sys.argv:
        if '--qtd-por-site' in a:
            qtd_por_site = int(a.split('--qtd-por-site=')[1])
            resumir = True

        if '--cadsin' in a:
            cadsin = True

        if '--atualizar' in a:
            atualizar = True

    if resumir:
        resumir_conteudos(qtd_por_site)
    elif cadsin:
        cadastrar_sinonimos()
    elif atualizar:
        atualizar_resumos()
#    get_content_viomundo()
