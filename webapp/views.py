# -*- coding: utf-8 -*-

import os
import time
import json
from flask import Flask, request, render_template, Response
from flask_cors import CORS
from resumeai import summarize_text
from test_data import runner
from webapp import config
from webapp import utils
from webapp.controllers import ConteudoController


BASE_DIR = os.path.dirname(os.path.abspath(__file__))
TEMPLATES_DIR = os.path.join(BASE_DIR, 'templates')
STATIC_DIR = os.path.join(BASE_DIR, 'static')

toscoapp = Flask('webapp', template_folder=TEMPLATES_DIR,
                 static_folder=STATIC_DIR)

cors = CORS(toscoapp)


# json
@toscoapp.route('/json/lista/<tipo>')
def listar_resumos_tipo(tipo):
    pagina = int(request.args.get('pagina', 1))
    conteudos = ConteudoController().get_lista(tipo, pagina)
    cjson = [c.to_dict() for c in conteudos]
    return Response(json.dumps(cjson), mimetype='application/json')


@toscoapp.route('/json/site/<nome>')
def listar_resumos_site(nome):
    pagina = int(request.args.get('pagina', 1))
    conteudos = ConteudoController().get_lista_site(nome, pagina)
    cjson = [c.to_dict() for c in conteudos]
    return Response(json.dumps(cjson), mimetype='application/json')


@toscoapp.route('/json/conteudo/<path:url>')
def mostar_conteudo(url):
    conteudo = ConteudoController().get_conteudo(url)
    return Response(json.dumps(conteudo), mimetype='application/json')


@toscoapp.route('/json/trendtopics')
def get_trend_topics():
    horas = float(request.args.get('horas', 12))
    site = request.args.get('site')
    quantidade = int(request.args.get('quantidade', 10))
    smart = bool(int(request.args.get('smart', 0)))
    smart = smart or request.args.get('horas', True) == True
    trends = ConteudoController().get_trend_topics(horas, site, quantidade,
                                                   smart=smart)
    return Response(json.dumps(trends), mimetype='application/json')


if config.AMBIENTE.lower() != 'prod':
    # site
    @toscoapp.route('/')
    def index():
        return render_template('index.html')

    @toscoapp.route('/resumeai/api/', methods=["POST"])
    def resumeaí_api():
        txt = request.form['txt']
        t0 = time.time()
        sumario = summarize_text(txt)
        t1 = time.time()
        ttotal = t1 - t0
        resumo, entidades = sumario.summary, sumario.entities
        entidades = [e.entity for e in entidades]
        resumo_json = json.dumps({'resumo': resumo, 'entidades': entidades,
                                  'tempo': ttotal})
        return Response(resumo_json, mimetype="application/json")

    @toscoapp.route('/lista/<tipo>')
    def listar_resumos(tipo):
        controller = ConteudoController()
        mostrar_anterior = False
        mostrar_próximo = False
        pagina = int(request.args.get('pagina', 1))
        mostrar_proximo = True
        mostrar_anterior = False
        if pagina > 1:
            mostrar_anterior = True

        conteudos = controller.get_lista(tipo, pagina)
        trends = controller.get_trend_topics(smart=True)

        contexto = {'conteudos': conteudos,
                    'proximo': mostrar_proximo,
                    'anterior': mostrar_anterior,
                    'proxima_pagina': pagina + 1,
                    'pagina_anterior': pagina - 1,
                    'trends': trends}
        return render_template('lista.html', **contexto)

    @toscoapp.route('/site/<nome>')
    def listar_resumos_por_site(nome):
        mostrar_anterior = False
        mostrar_próximo = False
        pagina = int(request.args.get('pagina', 1))
        mostrar_proximo = True
        mostrar_anterior = False
        if pagina > 1:
            mostrar_anterior = True

        conteudos = ConteudoController().get_lista_site(nome, pagina)
        contexto = {'conteudos': conteudos,
                    'proximo': mostrar_proximo,
                    'anterior': mostrar_anterior,
                    'proxima_pagina': pagina + 1,
                    'pagina_anterior': pagina - 1}
        return render_template('lista.html', **contexto)

    @toscoapp.route('/resumo/<path:url>')
    def mostrar_resumo(url):
        resumo = ConteudoController().get_conteudo(url)

        return render_template('resumo.html', **{'c': resumo})

    @toscoapp.route('/add2notentities', methods=["POST"])
    def add2notentities():
        word = request.form['word'].lower()
        runner.add2notentities(word)
        return 'ok'

    @toscoapp.route('/rmnotentities', methods=["POST"])
    def rmnotentities():
        word = request.form['word'].lower()
        runner.rmnotentities(word)
        return 'ok'

    @toscoapp.route('/innotentities', methods=["GET"])
    def innotentities():
        word = request.args.get('word').lower()
        ret = runner.innotentities(word)
        return str(ret)


if __name__ == '__main__':

    debug = True

    toscoapp.run(debug=debug, host='0.0.0.0')
